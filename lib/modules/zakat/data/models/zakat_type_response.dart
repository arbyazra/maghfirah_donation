// To parse this JSON data, do
//
//     final zakatTypeResponse = zakatTypeResponseFromJson(jsonString);

import 'dart:convert';

ZakatTypeResponse zakatTypeResponseFromJson(String str) => ZakatTypeResponse.fromJson(json.decode(str));

String zakatTypeResponseToJson(ZakatTypeResponse data) => json.encode(data.toJson());

class ZakatTypeResponse {
    ZakatTypeResponse({
        this.success,
        this.message,
        this.data,
    });

    bool success;
    String message;
    List<ZakatType> data;

    ZakatTypeResponse copyWith({
        bool success,
        String message,
        List<ZakatType> data,
    }) => 
        ZakatTypeResponse(
            success: success ?? this.success,
            message: message ?? this.message,
            data: data ?? this.data,
        );

    factory ZakatTypeResponse.fromJson(Map<String, dynamic> json) => ZakatTypeResponse(
        success: json["success"] == null ? null : json["success"],
        message: json["message"] == null ? null : json["message"],
        data: json["data"] == null ? null : List<ZakatType>.from(json["data"].map((x) => ZakatType.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "success": success == null ? null : success,
        "message": message == null ? null : message,
        "data": data == null ? null : List<dynamic>.from(data.map((x) => x.toJson())),
    };
}

class ZakatType {
    ZakatType({
        this.type,
        this.description,
    });

    String type;
    String description;

    ZakatType copyWith({
        String type,
        String description,
    }) => 
        ZakatType(
            type: type ?? this.type,
            description: description ?? this.description,
        );

    factory ZakatType.fromJson(Map<String, dynamic> json) => ZakatType(
        type: json["type"] == null ? null : json["type"],
        description: json["description"] == null ? null : json["description"],
    );

    Map<String, dynamic> toJson() => {
        "type": type == null ? null : type,
        "description": description == null ? null : description,
    };
}
