// To parse this JSON data, do
//
//     final kursResponse = kursResponseFromJson(jsonString);

import 'dart:convert';

KursResponse kursResponseFromJson(String str) => KursResponse.fromJson(json.decode(str));

String kursResponseToJson(KursResponse data) => json.encode(data.toJson());

class KursResponse {
    KursResponse({
        this.success,
        this.message,
        this.data,
    });

    bool success;
    String message;
    Data data;

    KursResponse copyWith({
        bool success,
        String message,
        Data data,
    }) => 
        KursResponse(
            success: success ?? this.success,
            message: message ?? this.message,
            data: data ?? this.data,
        );

    factory KursResponse.fromJson(Map<String, dynamic> json) => KursResponse(
        success: json["success"] == null ? null : json["success"],
        message: json["message"] == null ? null : json["message"],
        data: json["data"] == null ? null : Data.fromJson(json["data"]),
    );

    Map<String, dynamic> toJson() => {
        "success": success == null ? null : success,
        "message": message == null ? null : message,
        "data": data == null ? null : data.toJson(),
    };
}

class Data {
    Data({
        this.goldPrice,
        this.silverPrice,
    });

    String goldPrice;
    String silverPrice;

    Data copyWith({
        String goldPrice,
        String silverPrice,
    }) => 
        Data(
            goldPrice: goldPrice ?? this.goldPrice,
            silverPrice: silverPrice ?? this.silverPrice,
        );

    factory Data.fromJson(Map<String, dynamic> json) => Data(
        goldPrice: json["gold_price"] == null ? null : json["gold_price"],
        silverPrice: json["silver_price"] == null ? null : json["silver_price"],
    );

    Map<String, dynamic> toJson() => {
        "gold_price": goldPrice == null ? null : goldPrice,
        "silver_price": silverPrice == null ? null : silverPrice,
    };
}
