
import 'package:maghfirah_donation/shared/shared.dart';

import '../../../modules.dart';

class ZakatRepository{
  final Network network;

  ZakatRepository({this.network});
  Future<ZakatTypeResponse> getZakatType() async {
    var url = parseUrl("/util/zakat-type");
    final response = await network.getRequest(url,);
    if (response.statusCode >= 200 && response.statusCode < 300) {
      return zakatTypeResponseFromJson(response.body);
    } else {
      print(response.body);
      return zakatTypeResponseFromJson(response.body);
    }
  }
  Future<KursResponse> getZakatKurs() async {
    var url = parseUrl("/util/nishab-price");
    final response = await network.getRequest(url,);
    if (response.statusCode >= 200 && response.statusCode < 300) {
      return kursResponseFromJson(response.body);
    } else {
      print(response.body);
      return kursResponseFromJson(response.body);
    }
  }
}