import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

part 'zakat_calculator_state.dart';

class ZakatCalculatorCubit extends Cubit<ZakatCalculatorState> {
  ZakatCalculatorCubit() : super(ZakatCalculatorState());

  void reset()=>emit(ZakatCalculatorState());

  void onDepositChanged(double value){
    emit(state.copyWith(
      deposit: value
    ));
  }
  void onGoldNishabChanged(double value){
    emit(state.copyWith(
      goldNishab: value
    ));
  }
  void onPerakNishabChanged(double value){
    emit(state.copyWith(
      perakNishab: value
    ));
  }
  void onGoldChanged(double value){
    emit(state.copyWith(
      gold: value
    ));
  }
  void onAssetChanged(double value){
    emit(state.copyWith(
      asset: value
    ));
  }
  void onStockChanged(double value){
    emit(state.copyWith(
      stock: value
    ));
  }
  void onDebtChanged(double value){
    emit(state.copyWith(
      debt :value
    ));
  }
  void onPerakChanged(double value){
    emit(state.copyWith(
      perak :value
    ));
  }
  void onHasilPanenChanged(int value){
    print(value);
    emit(state.copyWith(
      hasilPanen :value
    ));
  }
  void onStokDagangChanged(double value){
    emit(state.copyWith(
      stokDagang :value
    ));
  }
  void onPricePerKgChanged(double value){
    emit(state.copyWith(
      pricePerKg :value
    ));
  }
  void onIsUsingIrigation(bool value){
    emit(state.copyWith(
      isUsingIrigation:value
    ));
  }
  void onOtherPriceChanged(double value){
    emit(state.copyWith(
      other:value
    ));
  }
}
