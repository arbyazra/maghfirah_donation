part of 'zakat_calculator_cubit.dart';

class ZakatCalculatorState extends Equatable {
  final double deposit;
  final double gold;
  final double perak;
  final double asset;
  final double stock;
  final int hasilPanen;
  final double stokDagang;
  final double pricePerKg;
  final double debt;
  final double other;
  final bool isUsingIrigation;
  final double goldNishab;
  final double perakNishab;
  ZakatCalculatorState({
    this.other = 0,
    this.goldNishab = 0,
    this.perakNishab = 0,
    this.isUsingIrigation = false,
    this.stokDagang = 0,
    this.hasilPanen = 0,
    this.pricePerKg = 0,
    this.deposit = 0,
    this.gold = 0,
    this.asset = 0,
    this.perak = 0,
    this.stock = 0,
    this.debt = 0,
  });

  ZakatCalculatorState copyWith(
          {deposit,
          gold,
          perak,
          asset,
          stock,
          hasilPanen,
          stokDagang,
          pricePerKg,
          debt,
          isUsingIrigation,
          goldNishab,
          perakNishab,
          other}) =>
      ZakatCalculatorState(
        deposit: deposit ?? this.deposit,
        gold: gold ?? this.gold,
        perak: perak ?? this.perak,
        asset: asset ?? this.asset,
        stock: stock ?? this.stock,
        hasilPanen: hasilPanen ?? this.hasilPanen,
        stokDagang: stokDagang ?? this.stokDagang,
        pricePerKg: pricePerKg ?? this.pricePerKg,
        debt: debt ?? this.debt,
        isUsingIrigation: isUsingIrigation ?? this.isUsingIrigation,
        other: other ?? this.other,
        goldNishab: goldNishab ?? this.goldNishab,
        perakNishab: perakNishab ?? this.perakNishab,
      );

  double get networth => (deposit + gold + asset + stock);
  double get totalZakatHarta =>
      ((deposit + gold + asset + stock) - debt) * 0.025;
  double get totalZakatEmasPerak => ((gold*goldNishab) + (perak*perakNishab)) * 0.025 * 0.025;
  double get totalZakatHartaKarun => (gold) * 0.2;
  double get totalZakatPerdagangan => ((gold + asset) - (debt + other)) * 0.025;
  double get totalZakatPertanian =>
      (hasilPanen * pricePerKg) / (isUsingIrigation ? 2 : 1) *0.1;
  @override
  List<Object> get props => [
        deposit,
        gold,
        perak,
        other,
        asset,
        stock,
        hasilPanen,
        stokDagang,
        pricePerKg,
        debt,
        isUsingIrigation,
        perakNishab,
        goldNishab,
      ];
}
