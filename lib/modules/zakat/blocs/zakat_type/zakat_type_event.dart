part of 'zakat_type_bloc.dart';

abstract class ZakatTypeEvent extends Equatable {
  const ZakatTypeEvent();

  @override
  List<Object> get props => [];
}

class GetZakatType extends ZakatTypeEvent{}
class ResetZakatType extends ZakatTypeEvent{}