part of 'zakat_type_bloc.dart';

abstract class ZakatTypeState extends Equatable {
  const ZakatTypeState();
  
  @override
  List<Object> get props => [];
}

class ZakatTypeInitial extends ZakatTypeState {}
class ZakatTypeLoading extends ZakatTypeState{}
class ZakatTypeLoaded extends ZakatTypeState{
  final ZakatTypeResponse result;

  ZakatTypeLoaded(this.result);
  @override
  List<Object> get props => [result];
}
class ZakatTypeError extends ZakatTypeState{}