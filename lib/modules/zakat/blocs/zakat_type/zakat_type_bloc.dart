import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:maghfirah_donation/modules/modules.dart';

part 'zakat_type_event.dart';
part 'zakat_type_state.dart';

class ZakatTypeBloc extends Bloc<ZakatTypeEvent, ZakatTypeState> {
  ZakatTypeBloc({this.repository}) : super(ZakatTypeInitial());
  final ZakatRepository repository;
  @override
  Stream<ZakatTypeState> mapEventToState(
    ZakatTypeEvent event,
  ) async* {
    if(event is ResetZakatType){
      yield ZakatTypeInitial();
    }
    if (event is GetZakatType) {
      yield ZakatTypeLoading();
      try {
        var result = await repository.getZakatType();
        if (result.success) {
          yield ZakatTypeLoaded(result);
        } else {
          yield ZakatTypeError();
        }
      } catch (e) {
        print(e);
        yield ZakatTypeError();
      }
    }
  }
}
