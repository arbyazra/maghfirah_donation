part of 'zakat_kurs_bloc.dart';

abstract class ZakatKursState extends Equatable {
  const ZakatKursState();
  
  @override
  List<Object> get props => [];
}

class ZakatKursInitial extends ZakatKursState {}
class ZakatKursLoading extends ZakatKursState {}
class ZakatKursLoaded extends ZakatKursState {
  final KursResponse result;

  ZakatKursLoaded({this.result});
}
class ZakatKursError extends ZakatKursState {}
