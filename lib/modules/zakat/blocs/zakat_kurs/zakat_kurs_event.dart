part of 'zakat_kurs_bloc.dart';

abstract class ZakatKursEvent extends Equatable {
  const ZakatKursEvent();

  @override
  List<Object> get props => [];
}

class GetZakatKurs extends ZakatKursEvent{}