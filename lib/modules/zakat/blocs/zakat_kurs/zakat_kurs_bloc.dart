import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:maghfirah_donation/modules/modules.dart';

part 'zakat_kurs_event.dart';
part 'zakat_kurs_state.dart';

class ZakatKursBloc extends Bloc<ZakatKursEvent, ZakatKursState> {
  ZakatKursBloc({this.repository}) : super(ZakatKursInitial());
  final ZakatRepository repository;
  @override
  Stream<ZakatKursState> mapEventToState(
    ZakatKursEvent event,
  ) async* {
    if (event is GetZakatKurs) {
      yield ZakatKursLoading();
      try {
        var result = await repository.getZakatKurs();
        if (result.success) {
          yield ZakatKursLoaded(result: result);
        } else {
          yield ZakatKursError();
        }
      } catch (e) {
        print(e);
        yield ZakatKursError();
      }
    }
  }
}
