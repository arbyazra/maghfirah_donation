import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:maghfirah_donation/components/components.dart';
import 'package:maghfirah_donation/shared/shared.dart';

import '../../../modules.dart';

class ZakatHartaPertanianCalculatorForm extends StatefulWidget {
  final BuildContext context;

  const ZakatHartaPertanianCalculatorForm({Key key, @required this.context})
      : super(key: key);

  @override
  _ZakatHartaPertanianCalculatorFormState createState() =>
      _ZakatHartaPertanianCalculatorFormState();
}

class _ZakatHartaPertanianCalculatorFormState
    extends State<ZakatHartaPertanianCalculatorForm> {
  int typeOfIrigation = 0;
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ZakatCalculatorCubit, ZakatCalculatorState>(
      builder: (context, state) => Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          CalculatorTextField(
            title: "Hasil Panen",
            inputUnit: "Kg",
            onChanged: (value) => context
                .read<ZakatCalculatorCubit>()
                .onHasilPanenChanged(value.length == 0
                    ? 0
                    : int.parse(value.split(".").join())),
          ),
          SizedBox(height: Dimens.dp10),
          H2Atm(
            "Jenis Pengairan",
            style: TextStylesTheme.contentText,
          ),
          SizedBox(
            height: Dimens.dp8,
          ),
          RadioListTile(
            value: false,
            groupValue: state.isUsingIrigation,
            onChanged: (v) {
              context.read<ZakatCalculatorCubit>().onIsUsingIrigation(false);
            },
            title: H3Atm("Tanpa Pengairan"),
          ),
          RadioListTile(
            value: true,
            groupValue: state.isUsingIrigation,
            onChanged: (v) {
              context.read<ZakatCalculatorCubit>().onIsUsingIrigation(true);
            },
            title: H3Atm("Dengan Pengairan"),
          ),
          SizedBox(height: Dimens.dp10),
          CalculatorTextField(
            title: "Harga per Kg",
            onChanged: (value) => context
                .read<ZakatCalculatorCubit>()
                .onPricePerKgChanged(value.length == 0
                    ? 0
                    : double.parse(value.split(".").join())),
          ),
        ],
      ),
    );
  }
}
