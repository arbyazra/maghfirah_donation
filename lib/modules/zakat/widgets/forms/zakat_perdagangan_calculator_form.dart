import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:maghfirah_donation/components/components.dart';
import 'package:maghfirah_donation/shared/shared.dart';

import '../../../modules.dart';

class ZakatPerdaganganCalculatorForm extends StatelessWidget {
  final BuildContext context;

  const ZakatPerdaganganCalculatorForm({Key key, @required this.context})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ZakatCalculatorCubit, ZakatCalculatorState>(
      builder: (context, state) => Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          CalculatorTextField(
            title: "Uang (cash, tabungan, dkk)",
            onChanged: (value) => context
              .read<ZakatCalculatorCubit>()
              .onGoldChanged(value.length == 0
                  ? 0
                  : double.parse(value.split(".").join())),
          ),
          SizedBox(height: Dimens.dp10),
          CalculatorTextField(
            title: "Stok Barang Dagangan",
            onChanged: (value) => context
              .read<ZakatCalculatorCubit>()
              .onStokDagangChanged(value.length == 0
                  ? 0
                  : double.parse(value.split(".").join())),
          ),
          SizedBox(height: Dimens.dp10),
          CalculatorTextField(
            title: "Piutang (1)",
            onChanged: (value) => context
              .read<ZakatCalculatorCubit>()
              .onAssetChanged(value.length == 0
                  ? 0
                  : double.parse(value.split(".").join())),
          ),
          SizedBox(height: Dimens.dp10),
          CalculatorTextField(
            title: "Hutang (2)",
            onChanged: (value) => context
              .read<ZakatCalculatorCubit>()
              .onDebtChanged(value.length == 0
                  ? 0
                  : double.parse(value.split(".").join())),
          ),
          SizedBox(height: Dimens.dp10),
          CalculatorTextField(
            title: "Biaya Lainya (3)",
            onChanged: (value) => context
              .read<ZakatCalculatorCubit>()
              .onOtherPriceChanged(value.length == 0
                  ? 0
                  : double.parse(value.split(".").join())),
          ),
        ],
      ),
    );
  }

  
}
