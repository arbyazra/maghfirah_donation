import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:maghfirah_donation/components/components.dart';
import 'package:maghfirah_donation/shared/shared.dart';

import '../../../modules.dart';

class ZakatHartaCalculatorForm extends StatelessWidget {
  final BuildContext context;

  const ZakatHartaCalculatorForm({Key key, @required this.context})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ZakatCalculatorCubit, ZakatCalculatorState>(
      builder: (context, state) => Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          CalculatorTextField(
            title: "Uang tunai dan tabungan",
            onChanged: (value) => context
              .read<ZakatCalculatorCubit>()
              .onDepositChanged(value.length == 0
                  ? 0
                  : double.parse(value.split(".").join())),
          ),
          SizedBox(height: Dimens.dp10),
          CalculatorTextField(
            title: "Saham dan surat berharga lainnya (1)",
            onChanged: (value) => context
              .read<ZakatCalculatorCubit>()
              .onGoldChanged(value.length == 0
                  ? 0
                  : double.parse(value.split(".").join())),
          ),
          SizedBox(height: Dimens.dp10),
          CalculatorTextField(
            title: "Piutang (2)",
            onChanged: (value) => context
              .read<ZakatCalculatorCubit>()
              .onAssetChanged(value.length == 0
                  ? 0
                  : double.parse(value.split(".").join())),
          ),
          SizedBox(height: Dimens.dp10),
          CalculatorTextField(
            title: "Hutang (3)",
            onChanged: (value) => context
              .read<ZakatCalculatorCubit>()
              .onDebtChanged(value.length == 0
                  ? 0
                  : double.parse(value.split(".").join())),
          ),
          SizedBox(height: Dimens.dp10),
        ],
      ),
    );
  }

  
}
