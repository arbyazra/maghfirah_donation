import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:maghfirah_donation/components/components.dart';
import 'package:maghfirah_donation/shared/shared.dart';

import '../../../modules.dart';

class ZakatEmasPerakCalculatorForm extends StatelessWidget {
  final BuildContext context;

  const ZakatEmasPerakCalculatorForm({Key key, @required this.context})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ZakatCalculatorCubit, ZakatCalculatorState>(
      builder: (context, state) => Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          CalculatorTextField(
            title: "Emas yang dimiliki",
            inputUnit: "gram",
            onChanged: (value) => context
              .read<ZakatCalculatorCubit>()
              .onGoldChanged(value.length == 0
                  ? 0
                  : double.parse(value.split(".").join())),
          ),
          SizedBox(height: Dimens.dp10),
          CalculatorTextField(
            title: "Perak yang dimiliki",
            inputUnit: "gram",
            onChanged: (value) => context
              .read<ZakatCalculatorCubit>()
              .onPerakChanged(value.length == 0
                  ? 0
                  : double.parse(value.split(".").join())),
          ),
          SizedBox(height: Dimens.dp10),
        ],
      ),
    );
  }

  
}
