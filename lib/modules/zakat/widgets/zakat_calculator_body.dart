import 'package:flutter/material.dart';
import 'package:maghfirah_donation/components/components.dart';
import 'package:maghfirah_donation/shared/shared.dart';

import '../../modules.dart';

class ZakatCalculatorBody extends StatefulWidget {
  final ZakatType zakatType;

  const ZakatCalculatorBody({Key key, this.zakatType}) : super(key: key);

  @override
  _ZakatCalculatorBodyState createState() => _ZakatCalculatorBodyState();
}

class _ZakatCalculatorBodyState extends State<ZakatCalculatorBody> {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.only(
            left: Dimens.dp14,
            top: Dimens.dp16,
            right: Dimens.dp14,
            bottom: 200),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            H2Atm(
              Strings.zakatCalculatorWarning,
              style: TextStylesTheme.contentText,
            ),
            SizedBox(
              height: Dimens.dp18,
            ),
            H1Atm(
              "Komponen Zakat",
              // activity.title,
              maxLine: 4,
              overflow: TextOverflow.ellipsis,
              style: TextStylesTheme.titleText,
            ),
            SizedBox(
              height: Dimens.dp16,
            ),
            getCalculators(),
            getSummary()
          ],
        ),
      ),
    );
  }

  Widget getSummary() {
    switch (widget.zakatType.type) {
      case "Zakat Harta":
        return ZakatHartaSummary();
        break;
      case "Zakat Emas & Perak":
        return ZakatEmasPerakSummary();
        break;
      case "Zakat Perdagangan":
        return ZakatPerdaganganSummary();
        break;
      case "Zakat Pertanian":
        return ZakatPertanianSummary();
        break;
      case "Zakat Harta Karun":
        return SizedBox();
        break;
      default:
        return ZakatHartaSummary();
    }
  }

  Widget getCalculators() {
    switch (widget.zakatType.type) {
      case "Zakat Harta":
        return ZakatHartaCalculatorForm(context: context,);
        break;
      case "Zakat Emas & Perak":
        return ZakatEmasPerakCalculatorForm(context: context);
        break;
      case "Zakat Perdagangan":
        return ZakatPerdaganganCalculatorForm(context: context);
        break;
      case "Zakat Pertanian":
        return ZakatHartaPertanianCalculatorForm(context: context);
        break;
      case "Zakat Harta Karun":
        return ZakatHartaKarunCalculatorForm(context: context);
        break;
      default:
        return ZakatHartaCalculatorForm(context: context,);
    }
  }

  
}
