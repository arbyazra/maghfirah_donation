import 'package:flutter/material.dart';
import 'package:maghfirah_donation/components/components.dart';
import 'package:maghfirah_donation/shared/shared.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import '../../modules.dart';

class ZakatBody extends StatefulWidget {
  final bool isFromDonation;

  const ZakatBody({Key key, this.isFromDonation}) : super(key: key);
  @override
  _ZakatBodyState createState() => _ZakatBodyState();
}

class _ZakatBodyState extends State<ZakatBody> {
  final RefreshController _refreshController =
      RefreshController(initialRefresh: false);
  @override
  void initState() {
    super.initState();
    context.read<ZakatTypeBloc>().add(GetZakatType());
    
  }

  @override
  Widget build(BuildContext context) {
    return SmartRefresher(
      controller: _refreshController,
      onRefresh: () {
        context.read<ZakatTypeBloc>().add(ResetZakatType());
        _refreshController.refreshCompleted();
      },
      child: BlocListener<ZakatTypeBloc, ZakatTypeState>(
        listener: (context, state) {
          if (state is ZakatTypeInitial) {
            context.read<ZakatTypeBloc>().add(GetZakatType());
          }
        },
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.symmetric(
                horizontal: Dimens.dp14, vertical: Dimens.dp18),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                H1Atm(
                  "Jenis Zakat",
                  // activity.title,
                  maxLine: 4,
                  overflow: TextOverflow.ellipsis,
                  style: TextStylesTheme.titleText,
                ),
                SizedBox(height: Dimens.dp10),
                _buildZakats()
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildZakats() {
    return BlocBuilder<ZakatTypeBloc, ZakatTypeState>(
      builder: (context, state) {
        if (state is ZakatTypeError) {
          return _buildEmptyState();
        }
        if (state is ZakatTypeLoaded) {
          return ListView.separated(
            shrinkWrap: true,
            physics: NeverScrollableScrollPhysics(),
            itemCount: state.result.data.length,
            separatorBuilder: (context, index) => DefaultDividerAtm(),
            itemBuilder: (context, index) {
              var data = state.result.data[index];
              return Column(
                children: [
                  ListTile(
                    leading: Icon(MaghfirahIcon.zakat,
                        color: AppColors.primaryColor),
                    title: H3Atm(
                      data.type,
                      style: TextStyle(fontWeight: FontWeight.w500),
                    ),
                    onTap: () {
                      context.read<ZakatCalculatorCubit>().reset();
                      Navigator.push(
                          context,
                          ZakatCalculatorPage(
                            isFromDonation: widget.isFromDonation,
                             zakatType: data,
                          ).route());
                    },
                  ),
                  DefaultDividerAtm(
                    color: AppColors.grey2,
                  ),
                ],
              );
            },
          );
        }
        return Center(child: CircularProgressIndicator());
      },
    );
  }

  Widget _buildEmptyState() {
    return Container(
      child: Center(
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                height: 100,
                child: Image.asset(AssetPaths.delivered),
              ),
              SizedBox(
                height: 12,
              ),
              H2Atm(
                "Ups, Ada kesalahan",
                style: TextStyle(fontWeight: FontWeight.w500),
              )
            ],
          ),
        ),
      ),
    );
  }
}
