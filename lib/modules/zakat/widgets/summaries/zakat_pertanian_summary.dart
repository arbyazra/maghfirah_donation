import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:maghfirah_donation/components/components.dart';
import 'package:maghfirah_donation/shared/shared.dart';

import '../../../modules.dart';

class ZakatPertanianSummary extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ZakatKursBloc, ZakatKursState>(
      builder: (context, state) {
        if (state is ZakatKursLoaded) {
          return _buildSummary(state);
        }
        return _buildSummary(null);
      },
    );
  }

  BlocBuilder<ZakatCalculatorCubit, ZakatCalculatorState> _buildSummary(
      ZakatKursLoaded kurs) {
    return BlocBuilder<ZakatCalculatorCubit, ZakatCalculatorState>(
      builder: (context, state) {
        print(state.pricePerKg);
        print(state.hasilPanen);
        return Padding(
          padding: const EdgeInsets.symmetric(vertical: Dimens.dp24),
          child:
              Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
            H1Atm(
              "Rincian",
              // activity.title,
              maxLine: 4,
              overflow: TextOverflow.ellipsis,
              style: TextStylesTheme.titleText,
            ),
            SizedBox(
              height: Dimens.dp18,
            ),
            Utils.buildSummaryRow(
                "Total Penjualan",
                "Rp " +
                    NumberFormat.decimalPattern("id")
                        .format(state.hasilPanen * state.pricePerKg))
          ]),
        );
      },
    );
  }
}
