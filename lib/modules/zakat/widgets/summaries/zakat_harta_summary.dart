import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_linkify/flutter_linkify.dart';
import 'package:intl/intl.dart';
import 'package:maghfirah_donation/components/components.dart';
import 'package:maghfirah_donation/shared/shared.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../../modules.dart';

class ZakatHartaSummary extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ZakatKursBloc, ZakatKursState>(
      builder: (context, state) {
        if (state is ZakatKursLoaded) {
          return _buildSummary(state);
        }
        return _buildSummary(null);
      },
    );
  }

  Widget _buildSummary(ZakatKursLoaded kurs) {
    return BlocBuilder<ZakatCalculatorCubit, ZakatCalculatorState>(
      builder: (context, state) {
        return Padding(
          padding: const EdgeInsets.symmetric(vertical: Dimens.dp24),
          child:
              Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
            H1Atm(
              "Rincian",
              // activity.title,
              maxLine: 4,
              overflow: TextOverflow.ellipsis,
              style: TextStylesTheme.titleText,
            ),
            SizedBox(
              height: Dimens.dp18,
            ),
            Utils.buildSummaryRow(
                "Harga 1 gram Emas",
                kurs == null
                    ? "-"
                    : "Rp. " +
                        NumberFormat.decimalPattern("id")
                            .format(int.parse(kurs.result.data.goldPrice))),
            SizedBox(
              height: Dimens.dp10,
            ),
            Utils.buildSummaryRow(
                "Harga 1 gram Perak",
                kurs == null
                    ? "-"
                    : "Rp. " +
                        NumberFormat.decimalPattern("id")
                            .format(int.parse(kurs.result.data.silverPrice))),
            SizedBox(
              height: Dimens.dp10,
            ),
            Utils.buildSummaryRow(
                "Nishab Emas",
                kurs == null
                    ? "-"
                    : "Rp" +
                        NumberFormat.decimalPattern("id")
                            .format(double.parse(kurs.result.data.goldPrice)*85)),
            SizedBox(
              height: Dimens.dp10,
            ),
            Utils.buildSummaryRow(
                "Nishab Perak",
                kurs == null
                    ? "-"
                    : "Rp" +
                        NumberFormat.decimalPattern("id")
                            .format(double.parse(kurs.result.data.silverPrice)*595)),
            SizedBox(
              height: Dimens.dp20,
            ),
            Utils.buildSummaryRow(
              "Total Harta",
              "Rp. ${state.networth > 0 ? NumberFormat("###,###").format(state.networth) : 0}",
            ),
            SizedBox(
              height: Dimens.dp10,
            ),
            Utils.buildSummaryRow(
              "Total Kewajiban",
              "Rp " + NumberFormat.decimalPattern("id").format(state.debt),
            ),
            SizedBox(
              height: Dimens.dp10,
            ),
            Utils.buildSummaryRow(
              "Selisih harta dan kewajiban",
              "Rp " +
                  NumberFormat.decimalPattern("id").format((state.gold +
                      state.asset +
                      state.deposit -
                      (state.debt))),
            ),
            SizedBox(
              height: Dimens.dp10,
            ),
            H1Atm(
              "Keterangan",
              // activity.title,
              maxLine: 4,
              overflow: TextOverflow.ellipsis,
              style: TextStylesTheme.titleText,
            ),
            SizedBox(
              height: Dimens.dp14,
            ),
            Linkify(
              linkStyle: TextStyle(color: Colors.blue),
              onOpen: (link) async {
                if (await canLaunch(link.url)) {
                  await launch(link.url);
                } else {
                  throw 'Could not launch $link';
                }
              },
              text:
                  '''1. Termasuk di dalamnya adalah investasi seperti reksadana dkk. Khusus untuk saham, kami sarankan untuk membaca tulisan di https://KonsultasiSyariah.com.\n2. Piutang yang diharapkan dapat kembali / ditagih.\n3. Cicilan hutang yang harus dibayar (jatuh tempo) dalam waktu dekat.
''',
            )
          ]),
        );
      },
    );
  }
}
