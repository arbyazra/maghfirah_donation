import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:maghfirah_donation/components/components.dart';
import 'package:maghfirah_donation/shared/shared.dart';

import '../../../modules.dart';

class ZakatEmasPerakSummary extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ZakatKursBloc, ZakatKursState>(
      builder: (context, state) {
        if (state is ZakatKursLoaded) {
          return _buildSummary(state);
        }
        return _buildSummary(null);
      },
    );
  }

  Widget _buildSummary(ZakatKursLoaded kurs) {
    return BlocBuilder<ZakatCalculatorCubit, ZakatCalculatorState>(
      builder: (context, state) {
        print(state.perak);
        return Padding(
          padding: const EdgeInsets.symmetric(vertical: Dimens.dp24),
          child:
              Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
            H1Atm(
              "Rincian",
              // activity.title,
              maxLine: 4,
              overflow: TextOverflow.ellipsis,
              style: TextStylesTheme.titleText,
            ),
            SizedBox(
              height: Dimens.dp18,
            ),
            Utils.buildSummaryRow(
                "Harga 1 gram Emas",
                kurs == null
                    ? "-"
                    :"Rp"+NumberFormat.decimalPattern("id")
                        .format(int.parse(kurs.result.data.goldPrice))),
            SizedBox(
              height: Dimens.dp10,
            ),
            Utils.buildSummaryRow(
                "Harga 1 gram Perak",
                kurs == null
                    ? "-"
                    :"Rp. "+NumberFormat.decimalPattern("id")
                        .format(int.parse(kurs.result.data.silverPrice))),
            SizedBox(
              height: Dimens.dp10,
            ),
            Utils.buildSummaryRow(
              "Zakat Emas",
              "Rp. "+NumberFormat.decimalPattern("id")
                        .format(state.gold*0.025),
            ),
            SizedBox(
              height: Dimens.dp10,
            ),
            Utils.buildSummaryRow(
              "Zakat Perak",
              "Rp. "+NumberFormat.decimalPattern("id")
                        .format(state.perak*0.025),
            ),
          ]),
        );
      },
    );
  }
}
