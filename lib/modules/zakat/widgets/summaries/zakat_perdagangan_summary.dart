import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:maghfirah_donation/components/components.dart';
import 'package:maghfirah_donation/shared/shared.dart';

import '../../../modules.dart';

class ZakatPerdaganganSummary extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ZakatKursBloc, ZakatKursState>(
      builder: (context, state) {
        if (state is ZakatKursLoaded) {
          return _buildSummary(state);
        }
        return _buildSummary(null);
      },
    );
  }

  BlocBuilder<ZakatCalculatorCubit, ZakatCalculatorState> _buildSummary(
      ZakatKursLoaded kurs) {
    return BlocBuilder<ZakatCalculatorCubit, ZakatCalculatorState>(
      builder: (context, state) {
        return Padding(
          padding: const EdgeInsets.symmetric(vertical: Dimens.dp24),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              H1Atm(
                "Rincian",
                // activity.title,
                maxLine: 4,
                overflow: TextOverflow.ellipsis,
                style: TextStylesTheme.titleText,
              ),
              SizedBox(
                height: Dimens.dp18,
              ),
              Utils.buildSummaryRow(
                  "Harga 1 gram Emas",
                  kurs == null
                      ? "-"
                      : "Rp "+NumberFormat.decimalPattern("id")
                          .format(int.parse(kurs.result.data.goldPrice))),
              SizedBox(
                height: Dimens.dp10,
              ),
              Utils.buildSummaryRow(
                  "Harga 1 gram Emas",
                  kurs == null
                      ? "-"
                      : "Rp "+NumberFormat.decimalPattern("id").format(
                          double.parse(kurs.result.data.goldPrice) * 85)),
              SizedBox(
                height: Dimens.dp10,
              ),
              Utils.buildSummaryRow(
                "Total Harta Kena Zakat",
                "Rp " +
                    NumberFormat.decimalPattern("id")
                        .format(state.gold + state.asset + state.stokDagang),
              ),
              SizedBox(
                height: Dimens.dp10,
              ),
              Utils.buildSummaryRow(
                "Total Kewajiban",
                "Rp " +
                    NumberFormat.decimalPattern("id")
                        .format(state.debt + state.other),
              ),
              SizedBox(
                height: Dimens.dp10,
              ),
              Utils.buildSummaryRow(
                "Selisih harta dan kewajiban",
                "Rp " +
                    NumberFormat.decimalPattern("id")
                        .format((state.gold + state.asset + state.stokDagang)-(state.debt + state.other)),
              ),
              SizedBox(
                height: Dimens.dp18,
              ),
              H1Atm(
                "Keterangan",
                // activity.title,
                maxLine: 4,
                overflow: TextOverflow.ellipsis,
                style: TextStylesTheme.titleText,
              ),
              SizedBox(
                height: Dimens.dp14,
              ),
              H3Atm(
                '''1. Piutang yang diharapkan dapat kembali atau ditagih.\n2. Hutang dagang (jangka pendek).\n3. Biaya lain yang masih harus dibayar sebelum sampai waktu pembayaran zakat.\n''',
              )
            ],
          ),
        );
      },
    );
  }
}
