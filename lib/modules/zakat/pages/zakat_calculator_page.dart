import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:maghfirah_donation/components/components.dart';
import 'package:maghfirah_donation/modules/modules.dart';
import 'package:maghfirah_donation/shared/shared.dart';

class ZakatCalculatorPage extends StatefulWidget {
  final bool isFromDonation;
  final ZakatType zakatType;
  const ZakatCalculatorPage({Key key, this.isFromDonation, this.zakatType})
      : super(key: key);

  @override
  _ZakatCalculatorPageState createState() => _ZakatCalculatorPageState();
}

class _ZakatCalculatorPageState extends State<ZakatCalculatorPage> {
  @override
  void initState() {
    super.initState();
    context.read<ZakatKursBloc>().add(GetZakatKurs());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: DefaultAppBarAtm(
        title: widget.zakatType.type,
      ),
      bottomSheet: _buildBottomButton(),
      body: BlocListener<ZakatKursBloc, ZakatKursState>(
        listener: (context, state) {
          if(state is ZakatKursLoaded){
            context.read<ZakatCalculatorCubit>().onGoldNishabChanged(double.parse(state.result.data.goldPrice));
            context.read<ZakatCalculatorCubit>().onPerakNishabChanged(double.parse(state.result.data.silverPrice));
          }
        },
        child: ZakatCalculatorBody(
          zakatType: widget.zakatType,
        ),
      ),
    );
  }

  double getCalculatedZakat(ZakatCalculatorState state) {
    switch (widget.zakatType.type) {
      case "Zakat Harta":
        return state.totalZakatHarta;
        break;
      case "Zakat Emas & Perak":
        return state.totalZakatEmasPerak;
        break;
      case "Zakat Perdagangan":
        return state.totalZakatPerdagangan;
        break;
      case "Zakat Pertanian":
        return state.totalZakatPertanian;
        break;
      case "Zakat Harta Karun":
        return state.totalZakatHartaKarun;
        break;
      default:
        return state.totalZakatHarta;
    }
  }

  Widget _buildBottomButton() {
    return BlocBuilder<ZakatCalculatorCubit, ZakatCalculatorState>(
      builder: (context, state) {
        // print(state.totalX);
        return Container(
          height: 110,
          padding: const EdgeInsets.all(14),
          decoration: BoxDecoration(boxShadow: [
            BoxShadow(
              blurRadius: 2,
              offset: Offset(0, -2),
              color: Colors.black.withOpacity(0.02),
              spreadRadius: 1,
            ),
          ], color: Colors.white),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  H2Atm(
                    "Nominal Zakat",
                    style: TextStyle(
                      fontWeight: FontWeight.w300,
                    ),
                  ),
                  H1Atm(
                    "Rp ${getCalculatedZakat(state) > 0 ? NumberFormat("###,###").format(getCalculatedZakat(state)) : 0}",
                    style: TextStyle(
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: Dimens.dp18,
              ),
              DefaultButtonMol(
                text: "Tunaikan Zakat Sekarang",
                whiteMode: false,
                onClick: () {
                  if (widget.isFromDonation) {
                    Navigator.pop(context);
                    Navigator.pop(context);
                    return;
                  }
                  Navigator.push(context, DonationPage().route());
                },
              ),
            ],
          ),
        );
      },
    );
  }
}
