import 'package:flutter/material.dart';
import 'package:maghfirah_donation/components/components.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:maghfirah_donation/shared/shared.dart';
import '../../modules.dart';

class ZakatPage extends StatelessWidget {
  final bool isFromDonation;

  const ZakatPage({Key key, this.isFromDonation=false}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    final newtork = AppConfigProvider.of(context).network;
    return BlocProvider(
      create: (context) => ZakatTypeBloc(repository:ZakatRepository(network: newtork)),
      child: Scaffold(
        appBar: DefaultAppBarAtm(
          title: "Kalkulator Zakat",
        ),
        body: ZakatBody(isFromDonation: isFromDonation,),
      ),
    );
  }

  
}
