import 'package:flutter/material.dart';
import 'package:maghfirah_donation/components/components.dart';
import 'package:maghfirah_donation/shared/shared.dart';

class DonationNewsDetailPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: DefaultAppBarAtm(
        title: "Kabar Terbaru",
      ),
      body: ListView.builder(
        padding: EdgeInsets.fromLTRB(Dimens.dp14,Dimens.dp16,Dimens.dp14,100),
        itemCount: 20,
        itemBuilder: (context, index) {
          return DonationNewsItemCard();
        },
      ),
    );
  }
}
