import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:maghfirah_donation/components/components.dart';
import 'package:maghfirah_donation/shared/shared.dart';
import '../../modules.dart';
import 'package:formz/formz.dart';

class DonatingPage extends StatefulWidget {
  final Donation donation;
  final String price;
  const DonatingPage({Key key, this.donation, this.price}) : super(key: key);
  @override
  _DonatingPageState createState() => _DonatingPageState();
}

class _DonatingPageState extends State<DonatingPage> {
  
  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
  @override
  void initState() {
    super.initState();
    context.read<DonatingCubit>().reset();
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<AuthBloc, AuthState>(
      listener: (context, state) {
        if (state is Unauthenticated) {
          _navigateToLoginPage(context);
        }
      },
      builder: (context, state) {
        if (state is Unauthenticated) {
          Future.microtask(() => _navigateToLoginPage(context));
          return Scaffold(
              body: Center(
            child: CircularProgressIndicator(),
          ));
        }
        return Scaffold(
          key: scaffoldKey,
          appBar: DefaultAppBarAtm(
            title: "Sedekah Sekarang",
          ),
          body: BlocListener<DonatingCubit, DonatingState>(
            listener: (context, state) {
              switch (state.status) {
                case FormzStatus.submissionFailure:
                  var msg = context.read<DonatingCubit>().msg;
                  scaffoldKey.currentState
                    ..hideCurrentSnackBar()
                    ..showSnackBar(SnackBar(
                      duration: Duration(seconds: 2),
                      content: H3Atm(
                        msg ?? "Suatu kesalahan telah terjadi, maaf",
                      ),
                    ));
                  break;
                case FormzStatus.submissionSuccess:
                  scaffoldKey.currentState
                    ..hideCurrentSnackBar()
                    ..showSnackBar(
                      SnackBar(
                        duration: Duration(seconds: 2),
                        content: H3Atm(
                          "Berhasil Memproses Transaksi",
                        ),
                      ),
                    ).closed.then((value) {
                      var response =
                          context.read<DonatingCubit>().response.data;
                      Navigator.pushReplacement(
                          context,
                          PaymentTutorialPage(
                            paymentId: response.id.toString(),
                            isBank:
                                response.paymentType.toLowerCase() == 'bank',
                          ).route());
                    });
                  break;
                case FormzStatus.submissionInProgress:
                  scaffoldKey.currentState
                      .showSnackBar(Utils.showLoadingSnackBar(
                        duration: Duration(minutes: 1)
                      ));
                  break;
                default:
              }
            },
            child: DonatingBody(
              scaffoldKey: scaffoldKey,
              donation: widget.donation,
              price: widget.price,
            ),
          ),
        );
      },
    );
  }

  void _navigateToLoginPage(BuildContext context) {
    Navigator.of(context).push(LoginPage(
      isFromDonation: true,
    ).route());
  }
}
