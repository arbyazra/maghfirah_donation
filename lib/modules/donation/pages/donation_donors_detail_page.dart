import 'package:flutter/material.dart';
import 'package:maghfirah_donation/components/components.dart';
import 'package:maghfirah_donation/modules/donation/donation.dart';

class DonationDonorsDetailPage extends StatelessWidget {
  final DonationDonatursResponse donaturs;

  const DonationDonorsDetailPage({Key key, this.donaturs}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: DefaultAppBarAtm(
        title: "Donatur",
      ),
      body: SingleChildScrollView(
              child: Padding(
          padding: const EdgeInsets.only(bottom: 100),
          child: DonationDetailDonors(
            hasMuchDonors: true,
            donaturs: donaturs,
          ),
        ),
      ),
    );
  }
}
