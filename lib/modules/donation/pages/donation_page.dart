import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:maghfirah_donation/components/components.dart';
import 'package:maghfirah_donation/modules/donation/blocs/donation_type/donation_type_bloc.dart';
import 'package:maghfirah_donation/shared/providers/providers.dart';

import '../../modules.dart';

class DonationPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final network = AppConfigProvider.of(context).network;
    return MultiBlocProvider(
      providers: [
        BlocProvider(
          create: (context) => DonationTabCubit(
              repository: DonationRepository(network: network)),
        ),
        BlocProvider(
          create: (context) => DonationTypeBloc(
              repository: DonationRepository(network: network)),
        ),
      ],
      child: WillPopScope(
        onWillPop: () async {
          context.read<DonationBloc>().add(ResetDonation());
          return true;
        },
        child: Scaffold(
          appBar: DefaultAppBarAtm(
            onForceBack: () {
              context.read<DonationBloc>().add(ResetDonation());
              Navigator.pop(context);
            },
            title: "12 Amal Kampung Maghfirah",
          ),
          body: DonationBody(),
        ),
      ),
    );
  }
}
