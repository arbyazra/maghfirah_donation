import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:maghfirah_donation/shared/providers/providers.dart';
import 'package:maghfirah_donation/shared/shared.dart';

import '../../../modules.dart';

class PaymentTutorialPage extends StatelessWidget {
  final String paymentId;
  final bool isBank;

  const PaymentTutorialPage({Key key, this.paymentId, this.isBank}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    print(paymentId);
    print(isBank);
    final network = AppConfigProvider.of(context).network;
    return BlocProvider(
      create: (context) => PaymentTutorialBloc(
        repository: DonationRepository(network: network)
      ),
      child: PaymentTutorialBody(
        isBank:isBank,
        paymentId: paymentId,
      ),
    );
  }
}
