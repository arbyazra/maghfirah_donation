import 'package:flutter/material.dart';
import 'package:maghfirah_donation/components/components.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:maghfirah_donation/shared/shared.dart';
import '../../../modules.dart';

class PaymentOptionsPage extends StatefulWidget {
  @override
  _PaymentOptionsPageState createState() => _PaymentOptionsPageState();
}

class _PaymentOptionsPageState extends State<PaymentOptionsPage> {
  @override
  void initState() {
    super.initState();
    context.read<PaymentMethodsBloc>().add(GetPaymentMethods());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: DefaultAppBarAtm(
        title: "Pilih Metode Pembayaran",
      ),
      body: _buildBody(),
    );
  }

  Widget _buildBody() {
    return BlocBuilder<PaymentMethodsBloc, PaymentMethodsState>(
      builder: (context, state) {
        if (state is PaymentMethodsLoaded) {
          if (state.paymentMethods.data.isEmpty) {
            return Center(
              child: Column(
                children: [
                  Container(
                    height: 100,
                    child: Image.asset(AssetPaths.noItems),
                  ),
                  SizedBox(
                    height: 12,
                  ),
                  H2Atm(
                    "Metode pembayaran tidak tersedia",
                    style: TextStyle(fontWeight: FontWeight.w500),
                  )
                ],
              ),
            );
          }
          return ListView.builder(
            padding: EdgeInsets.symmetric(vertical: 12, horizontal: 12),
            itemCount: state.paymentMethods.data.length,
            itemBuilder: (context, index) {
              var data = state.paymentMethods.data[index];
              return Padding(
                padding: const EdgeInsets.symmetric(vertical: 4),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    H2Atm(
                      data.fulltype,
                      style: TextStylesTheme.sectionTitleText,
                    ),
                    SizedBox(
                      height: 12,
                    ),
                    ListView.separated(
                      padding: EdgeInsets.symmetric(vertical: 4),
                      shrinkWrap: true,
                      physics: NeverScrollableScrollPhysics(),
                      itemCount: data.items.length,
                      separatorBuilder: (context, index) => DefaultDividerAtm(),
                      itemBuilder: (context, index) {
                        var e = data.items[index];
                        return ListTile(
                          onTap: (){
                            context.read<DonatingCubit>().onPaymentMethodCodeChanged(e);
                            Navigator.pop(context);
                          },
                          leading: ImageURLAtm(
                              imageUrl:
                                  "${Config.endpoint}/${e.icon ?? "storage/"+e.pathIcon}",
                              hasBorder: false,
                              width: 80,
                              bgColor: Colors.transparent,
                              fit: BoxFit.fitWidth),
                          title: H2Atm(
                            e.name ?? e.bankName,
                            style: TextStylesTheme.contentText,
                          ),
                        );
                      },
                    ),
                  ],
                ),
              );
            },
          );
        }
        if (state is PaymentMethodsError) {
          return Center(
            child: H2Atm("An error occured"),
          );
        }
        return Center(
          child: CircularProgressIndicator(),
        );
      },
    );
  }
}
