import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:maghfirah_donation/components/components.dart';
import 'package:maghfirah_donation/shared/providers/providers.dart';
import 'package:formz/formz.dart';
import 'package:maghfirah_donation/shared/shared.dart';
import '../../../../main.dart';
import '../../../modules.dart';

class PaymentDetailPage extends StatelessWidget {
  final PaymentTutorial paymentTutorial;

  PaymentDetailPage({Key key, this.paymentTutorial}) : super(key: key);
  final scaffoldKey = GlobalKey<ScaffoldState>();
  @override
  Widget build(BuildContext context) {
    final network = AppConfigProvider.of(context).network;
    return BlocProvider(
      create: (context) =>
          PaymentProofCubit(repository: DonationRepository(network: network)),
      child: BlocListener<PaymentProofCubit, PaymentProofState>(
        listener: (context, state) {
          if (state.status.isSubmissionInProgress) {
            scaffoldKey.currentState.showSnackBar(
                Utils.showLoadingSnackBar(duration: Duration(minutes: 1)));
          }
          if (state.status.isSubmissionFailure) {
            scaffoldKey.currentState
              ..hideCurrentSnackBar()
              ..showSnackBar(SnackBar(
                duration: Duration(seconds: 2),
                content: H3Atm(
                  "Suatu kesalahan telah terjadi, maaf",
                ),
              ));
          }
          if (state.status.isSubmissionSuccess) {
            scaffoldKey.currentState
              ..hideCurrentSnackBar()
              ..showSnackBar(SnackBar(
                content: H3Atm("Berhasil Mengunggah!"),
              )).closed.then(
                (value) {
                  Utils.addNewStack(context, NavigateToDonation());
                  context.read<ScreenNavigationBloc>().add(AppConfigProvider.of(context).stacks.last);
                  Navigator.pushAndRemoveUntil(
                      context, AppNavigator().route(), (route) => false);
                },
              );
          }
        },
        child: PaymentDetailBody(
          paymentTutorial: paymentTutorial,
          scaffoldKey: scaffoldKey,
        ),
      ),
    );
  }
}
