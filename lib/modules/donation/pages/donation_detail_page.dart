import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:maghfirah_donation/components/components.dart';
import 'package:maghfirah_donation/shared/shared.dart';
import 'package:share/share.dart';

import '../../modules.dart';

class DonationDetailPage extends StatefulWidget {
  final Donation donation;

  const DonationDetailPage({Key key, this.donation}) : super(key: key);

  @override
  _DonationDetailPageState createState() => _DonationDetailPageState();
}

class _DonationDetailPageState extends State<DonationDetailPage> {
  int amount = 1;
  String price;
  @override
  void initState() {
    super.initState();
    price = widget.donation.wakafPrice;
    context
        .read<DonationDetailBloc>()
        .add(GetDonationDetail(id: widget.donation.id));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _buildAppBar(context),
      body: _buildBody(context),
      bottomSheet: _buildBottomSheet(context),
    );
  }

  Widget _buildBottomSheet(BuildContext context) {
    return widget.donation.type.toLowerCase() != "wakaf"
        ? _buildBottomActionButton()
        : _buildManipulateWakaf();
  }

  Widget _buildBottomActionButton() {
    return Padding(
      padding: const EdgeInsets.all(14.0),
      child: DefaultButtonMol(
        text: widget.donation.type.toLowerCase() == "donation"
            ? "Sedekah Sekarang"
            : widget.donation.type.toLowerCase() == "wakaf"
                ? "Wakaf Sekarang"
                : "Tunaikan Zakat Sekarang",
        whiteMode: false,
        onClick: () => Navigator.push(
            context,
            DonatingPage(
              donation: widget.donation,
              price: widget.donation.wakafUnit == null
                  ? null
                  : (int.parse(price) * amount).toString(),
            ).route()),
      ),
    );
  }

  Widget _buildAppBar(BuildContext context) {
    return SearchableAppBar(
      onForceBack: () {
        Navigator.pop(context);
      },
      actions: [
        Padding(
          padding: const EdgeInsets.only(top: Dimens.dp10, right: Dimens.dp6),
          child: IconButton(
              icon: Icon(
                Icons.share,
                size: 28,
              ),
              onPressed: () {
                Share.share(Config.endpoint + "/${widget.donation.slug}}");
              }),
        )
      ],
    );
  }

  Widget _buildBody(BuildContext context) {
    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.only(bottom: 100),
        child: Column(
          children: [
            //should be DetailedImage(image: url)
            DetailedImage(
              image: widget.donation.pathFeatured,
            ),
            DonationDetailHeader(
              donation: widget.donation,
            ),
            DefaultDividerAtm(
              thickness: 8,
            ),
            DonationDetailDescription(
              donation: widget.donation,
            ),
            DefaultDividerAtm(
              thickness: 8,
            ),
            _buildDonatioNewsAndDonaturs(),
          ],
        ),
      ),
    );
  }

  Widget _buildDonatioNewsAndDonaturs() {
    return BlocBuilder<DonationDetailBloc, DonationDetailState>(
      builder: (context, state) {
        if (state is DonationDetailLoaded) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              DonationDetailNews(
                news: state.news,
              ),
              DefaultDividerAtm(
                thickness: 8,
              ),
              DonationDetailDonors(
                donaturs: state.donaturs,
              ),
            ],
          );
        }
        if (state is DonationDetailError) {
          return Container(
            height: 100,
            child: Center(
              child: H3Atm(Strings.loadingErrorText),
            ),
          );
        }
        return Center(
          child: CircularProgressIndicator(),
        );
      },
    );
  }

  Widget _buildManipulateWakaf() {
    return Container(
      height: 122,
      padding: EdgeInsets.only(top: 12),
      decoration: BoxDecoration(color: Colors.white, boxShadow: [
        BoxShadow(
            spreadRadius: 2,
            blurRadius: 1,
            offset: Offset(0, -1),
            color: Colors.black.withOpacity(0.05)),
      ]),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          _buildManipulateButtons(),
          _buildBottomActionButton(),
        ],
      ),
    );
  }

  Widget _buildManipulateButtons() {
    final border = BorderSide(width: 1, color: Colors.grey);
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 14),
      child: Row(
        children: [
          Expanded(
            child: Container(
              width: 100,
              child: Row(
                children: [
                  InkWell(
                    onTap: () {
                      if (amount == 1) {
                        return;
                      }
                      setState(() {
                        amount -= 1;
                      });
                    },
                    child: Container(
                      height: 35,
                      width: 40,
                      decoration: BoxDecoration(
                        border:
                            Border(left: border, top: border, bottom: border),
                      ),
                      child: Center(child: Icon(Icons.remove, size: 18)),
                    ),
                  ),
                  Container(
                    height: 35,
                    width: 50,
                    decoration: BoxDecoration(
                      border: Border(
                          top: border,
                          bottom: border,
                          left: border,
                          right: border),
                    ),
                    child: Center(
                      child: H2Atm(
                        amount.toString(),
                        style: TextStylesTheme.contentText
                            .copyWith(color: AppColors.primaryColor),
                      ),
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      setState(() {
                        amount += 1;
                      });
                    },
                    child: Container(
                      height: 35,
                      width: 40,
                      decoration: BoxDecoration(
                        border: Border(
                          top: border,
                          bottom: border,
                          right: border,
                        ),
                      ),
                      child: Center(
                          child: Icon(
                        Icons.add,
                        size: 18,
                      )),
                    ),
                  ),
                ],
              ),
            ),
          ),
          H2Atm(
            NumberFormat.simpleCurrency(locale: 'id')
                .format((int.parse(price) * amount)),
            style: TextStylesTheme.titleText,
          ),
        ],
      ),
    );
  }
}
