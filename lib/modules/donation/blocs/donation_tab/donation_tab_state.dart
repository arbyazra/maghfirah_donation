part of 'donation_tab_cubit.dart';

class DonationTabState extends Equatable {
  final int curIdx;
  final String type;
  final FormzStatus status;
  DonationTabState({
    this.type,
    this.curIdx,
    this.status = FormzStatus.pure,
  });

  copyWith({idx, status, type}) => DonationTabState(
        curIdx: idx ?? this.curIdx,
        status: status ?? this.status,
        type: type ?? this.type,
      );

  @override
  List<Object> get props => [curIdx, status,type];
}
