import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:formz/formz.dart';

import '../../../modules.dart';

part 'donation_tab_state.dart';

class DonationTabCubit extends Cubit<DonationTabState> {
  DonationTabCubit({this.repository})
      : super(
          DonationTabState(
            curIdx: 0,
          ),
        );
  final DonationRepository repository;
  int currentIndex = 1;
  String currentType = "";
  List<DonationCategory> categories = [];
  void load({DonationCategoryResponse result}) async {
    emit(state.copyWith(
        idx: state.curIdx, status: FormzStatus.submissionInProgress));
    // try {
      if (result.success) {
        categories = result.data;
        currentType = categories[currentIndex].category;
        emit(state.copyWith(
            idx: state.curIdx, status: FormzStatus.submissionSuccess));
      } else {
        emit(state.copyWith(
          idx: state.curIdx,
          status: FormzStatus.submissionFailure,
        ));
      }
    // } catch (e) {
    //   print(e);
    //   emit(state.copyWith(
    //     idx: state.curIdx,
    //     status: FormzStatus.submissionFailure,
    //   ));
    // }
  }

  void change(int curIdx) {
    currentType = categories[curIdx].category;
    currentIndex = categories[curIdx].id;
    emit(state.copyWith(
      idx: curIdx,
      status: state.status,
    ));
  }
}
