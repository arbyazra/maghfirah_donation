part of 'donation_detail_bloc.dart';

abstract class DonationDetailEvent extends Equatable {
  const DonationDetailEvent();

  @override
  List<Object> get props => [];
}

class GetDonationDetail extends DonationDetailEvent{
  final String id;

  GetDonationDetail({this.id});
}
