part of 'donation_detail_bloc.dart';

abstract class DonationDetailState extends Equatable {
  const DonationDetailState();
  
  @override
  List<Object> get props => [];
}

class DonationDetailInitial extends DonationDetailState {}
class DonationDetailLoaded extends DonationDetailState {
  final DonationDonatursResponse donaturs;
  final DonationNewsResponse news;

  DonationDetailLoaded(this.donaturs, this.news);
  @override
  List<Object> get props => [news,donaturs];
}
class DonationDetailError extends DonationDetailState {}
class DonationDetailLoading extends DonationDetailState {
}
