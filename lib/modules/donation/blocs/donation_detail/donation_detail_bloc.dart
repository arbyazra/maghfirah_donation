import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:maghfirah_donation/modules/donation/donation.dart';

part 'donation_detail_event.dart';
part 'donation_detail_state.dart';

class DonationDetailBloc
    extends Bloc<DonationDetailEvent, DonationDetailState> {
  DonationDetailBloc({this.repository}) : super(DonationDetailInitial());
  final DonationRepository repository;
  @override
  Stream<DonationDetailState> mapEventToState(
    DonationDetailEvent event,
  ) async* {
    if (event is GetDonationDetail) {
      yield DonationDetailLoading();
      // try {
        print(event.id);
        var donaturs =
            await repository.getDonationDonaturs(id: event.id.toString());
        var news = await repository.getDonationNews(id: event.id.toString());
        yield DonationDetailLoaded(donaturs, news);
      // } catch (e) {
      //   print(e);
      //   yield DonationDetailError();
      // }
    }
  }
}
