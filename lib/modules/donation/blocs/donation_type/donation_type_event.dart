part of 'donation_type_bloc.dart';

abstract class DonationTypeEvent extends Equatable {
  const DonationTypeEvent();

  @override
  List<Object> get props => [];
}

class GetDonationType extends DonationTypeEvent{
  final bool isMyDonation;

  GetDonationType({this.isMyDonation=false});
}