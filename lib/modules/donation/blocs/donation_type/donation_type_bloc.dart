import 'dart:async';
import '../../../modules.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

part 'donation_type_event.dart';
part 'donation_type_state.dart';

class DonationTypeBloc extends Bloc<DonationTypeEvent, DonationTypeState> {
  DonationTypeBloc({this.repository}) : super(DonationTypeInitial());
  final DonationRepository repository;
  @override
  Stream<DonationTypeState> mapEventToState(
    DonationTypeEvent event,
  ) async* {
    if (event is GetDonationType) {
      var result = await repository.getCategories();
      try {
        if (result.success) {
          // var

          if (event.isMyDonation) {
            var inserted = result.data;
            inserted.insert(
                0, DonationCategory(category: "Semua Kategori", id: -1));
            DonationCategoryResponse res = result;
            yield DonationTypeLoaded(res.copyWith(data: inserted));
          } else {
            yield DonationTypeLoaded(result);
          }
        } else {
          yield DonationTypeError();
        }
      } catch (e) {
        print(e);
        yield DonationTypeError();
      }
    }
  }
}
