part of 'donation_type_bloc.dart';



abstract class DonationTypeState extends Equatable {
  const DonationTypeState();
  
  @override
  List<Object> get props => [];
}

class DonationTypeInitial extends DonationTypeState {}
class DonationTypeLoading extends DonationTypeState {}
class DonationTypeError extends DonationTypeState {}
class DonationTypeLoaded extends DonationTypeState {
  final DonationCategoryResponse result;

  DonationTypeLoaded(this.result);
}
