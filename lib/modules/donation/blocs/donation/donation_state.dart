part of 'donation_bloc.dart';

abstract class DonationState extends Equatable {
  const DonationState();
  
  @override
  List<Object> get props => [];
}

class DonationInitial extends DonationState {}
class DonationLoaded extends DonationState {
  final DonationResponse result;

  DonationLoaded({this.result});
  @override
  List<Object> get props => [result];
}
class DonationLoading extends DonationState {}
class DonationError extends DonationState {}
