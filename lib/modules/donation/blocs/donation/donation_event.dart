part of 'donation_bloc.dart';

abstract class DonationEvent extends Equatable {
  const DonationEvent();

  @override
  List<Object> get props => [];
}

class GetDonation extends DonationEvent {
  final String categoryId;
  final String type;
  GetDonation(this.categoryId, this.type);
}

class ResetDonation extends DonationEvent {}
