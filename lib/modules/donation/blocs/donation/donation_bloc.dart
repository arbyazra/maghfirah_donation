import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:maghfirah_donation/modules/donation/donation.dart';

part 'donation_event.dart';
part 'donation_state.dart';

class DonationBloc extends Bloc<DonationEvent, DonationState> {
  DonationBloc({this.repository}) : super(DonationInitial());
  final DonationRepository repository;
  @override
  Stream<DonationState> mapEventToState(
    DonationEvent event,
  ) async* {
    if(event is ResetDonation){
      yield DonationInitial();
    }
    if (event is GetDonation) {
      yield DonationLoading();
      print(event.categoryId);
      print(event.type);
      try {
        var result = await repository.getDonation(
            type: event.type?.toLowerCase(),categoryId: event.categoryId);
        yield DonationLoaded(result: result);
      } on Exception catch (e) {
        print(e);
        yield DonationError();
      }
    }
  }
}
