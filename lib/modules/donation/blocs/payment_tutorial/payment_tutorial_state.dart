part of 'payment_tutorial_bloc.dart';

abstract class PaymentTutorialState extends Equatable {
  const PaymentTutorialState();
  
  @override
  List<Object> get props => [];
}

class PaymentTutorialInitial extends PaymentTutorialState {}
class PaymentTutorialLoaded extends PaymentTutorialState {
  final PaymentTutorialResponse result;

  PaymentTutorialLoaded(this.result);
  @override
  List<Object> get props => [result];
}
class PaymentTutorialLoading extends PaymentTutorialState {}
class PaymentTutorialError extends PaymentTutorialState {
  
}
