import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

import '../../../modules.dart';

part 'payment_tutorial_event.dart';
part 'payment_tutorial_state.dart';

class PaymentTutorialBloc extends Bloc<PaymentTutorialEvent, PaymentTutorialState> {
  PaymentTutorialBloc({this.repository}) : super(PaymentTutorialInitial());
  final DonationRepository repository;
  var msg;
  @override
  Stream<PaymentTutorialState> mapEventToState(
    PaymentTutorialEvent event,
  ) async* {
    if(event is ResetPaymentTutorial){
      yield PaymentTutorialInitial();
    }
    if(event is GetPaymentTutorial){
      try {
        var result = await repository.getPaymentTutorial(event.id);
        if(result.success){
          yield PaymentTutorialLoaded(result);
        } else {
          msg = result.message;
          yield PaymentTutorialError();
        }
      } catch (e) {
        print(e);
        yield PaymentTutorialError();
      }
    }
  }
}
