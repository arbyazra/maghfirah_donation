part of 'payment_tutorial_bloc.dart';

abstract class PaymentTutorialEvent extends Equatable {
  const PaymentTutorialEvent();

  @override
  List<Object> get props => [];
}

class ResetPaymentTutorial extends PaymentTutorialEvent{}
class GetPaymentTutorial extends PaymentTutorialEvent{
  final String id;

  GetPaymentTutorial(this.id);
}