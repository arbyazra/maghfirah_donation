import 'dart:io';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:formz/formz.dart';
import 'package:maghfirah_donation/modules/donation/donation.dart';

part 'payment_proof_state.dart';

class PaymentProofCubit extends Cubit<PaymentProofState> {
  PaymentProofCubit({this.repository}) : super(PaymentProofState());
  final DonationRepository repository;
  var msg;
  void onFileChanged(File v) {
    emit(state.copyWith(
      file: v,
    ));
  }

  void send(String transactionId) async {
    emit(state.copyWith(status: FormzStatus.submissionInProgress));
    try {
      var result = await repository.sendProof(transactionId, state.file);
      if (result) {
        emit(state.copyWith(status: FormzStatus.submissionSuccess));
        emit(state.copyWith(status: FormzStatus.valid));
      } else {
        emit(state.copyWith(status: FormzStatus.submissionFailure));
        emit(state.copyWith(status: FormzStatus.valid));
      }
    } catch (e) {
      print(e);
      emit(state.copyWith(status: FormzStatus.submissionFailure));
    }
  }
}
