part of 'payment_proof_cubit.dart';

class PaymentProofState extends Equatable {
  final FormzStatus status;
  final File file;

  PaymentProofState({
    this.status=FormzStatus.pure,
    this.file,
  });

  copyWith({
    status,
    file,
  }) =>
      PaymentProofState(
        status: status ?? this.status,
        file: file ?? this.file,
      );

  @override
  List<Object> get props => [this.status, this.file];
}
