part of 'donating_cubit.dart';

class DonatingState extends Equatable {
  final int nominal;
  final Item paymentMethodCode;
  final String specialMessage;
  final int isAnonymous;
  final String wishMessage;
  final FormzStatus status;

  DonatingState(
      {this.nominal = 0,
      this.isAnonymous=0,
      this.status = FormzStatus.pure,
      this.paymentMethodCode,
      this.specialMessage = "",
      this.wishMessage = ""});

  copyWith({
    status,
    nominal,
    paymentMethodCode,
    isAnonymous,
    specialMessage,
    wishMessage,
  }) =>
      DonatingState(
        status: status ?? this.status,
        nominal: nominal ?? this.nominal,
        isAnonymous: isAnonymous ?? this.isAnonymous,
        paymentMethodCode: paymentMethodCode ?? this.paymentMethodCode,
        specialMessage: specialMessage ?? this.specialMessage,
        wishMessage: wishMessage ?? this.wishMessage,
      );

  @override
  List<Object> get props =>
      [nominal, paymentMethodCode, specialMessage, wishMessage, status,isAnonymous];
}
