import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:formz/formz.dart';
import 'package:maghfirah_donation/modules/modules.dart';
import 'package:maghfirah_donation/shared/shared.dart';

part 'donating_state.dart';

class DonatingCubit extends Cubit<DonatingState> {
  DonatingCubit({this.repository}) : super(DonatingState());
  final DonationRepository repository;
  DonationRequest request = DonationRequest();
  PaymentResponse response;
  var msg;
  void reset() {
    emit(new DonatingState());
  }

  void onNominalChanged(String v) {
    print(v);
    request.nominal = int.parse(v);
    emit(state.copyWith(nominal: int.parse(v)));
  }

  void onPaymentMethodCodeChanged(Item v) {
    request.paymentCode = v.requestCode;
    emit(state.copyWith(paymentMethodCode: v));
  }

  void onAnonymousChanged(int v) {
    request.isAnonymous = v;
    emit(state.copyWith(isAnonymous: v));
  }

  void onSpecialMessageChanged(String v) {
    request.specialMessage = v;
    emit(state.copyWith(specialMessage: v));
  }

  void onWishMessageChanged(String v) {
    request.wishMessage = v;
    emit(state.copyWith(wishMessage: v));
  }

  void donate(String id) async {
    emit(state.copyWith(status: FormzStatus.submissionInProgress));
    try {
      UserResponse user = userResponseFromJson(await Cache.getCache(key: CACHE_USER));
      var result = await repository.donate(request.copyWith(
        id:id,
        donatureEmail: user.data.email,
        donatureName: user.data.name,
        donaturePhone: ''
      ));
      if (result.success) {
        response = result;
        emit(state.copyWith(status: FormzStatus.submissionSuccess));
        emit(state.copyWith(status: FormzStatus.valid));
      } else {
        msg = result.message;
        emit(state.copyWith(status: FormzStatus.submissionFailure));
        emit(state.copyWith(status: FormzStatus.valid));
      }
    } catch (e) {
      print(e);
      emit(state.copyWith(status: FormzStatus.submissionFailure));
    }
  }
}
