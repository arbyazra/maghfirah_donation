import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:maghfirah_donation/modules/donation/donation.dart';

part 'payment_methods_event.dart';
part 'payment_methods_state.dart';

class PaymentMethodsBloc
    extends Bloc<PaymentMethodsEvent, PaymentMethodsState> {
  PaymentMethodsBloc({this.repository}) : super(PaymentMethodsInitial());
  final DonationRepository repository;
  @override
  Stream<PaymentMethodsState> mapEventToState(
    PaymentMethodsEvent event,
  ) async* {
    if (event is GetPaymentMethods) {
      yield PaymentMethodsLoading();
      try {
        var result = await repository.getPaymentMethods();
        yield PaymentMethodsLoaded(result);
      } catch (e) {
        print(e);
        yield PaymentMethodsError();
      }
    }
  }
}
