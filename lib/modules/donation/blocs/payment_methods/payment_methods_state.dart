part of 'payment_methods_bloc.dart';

abstract class PaymentMethodsState extends Equatable {
  const PaymentMethodsState();
  
  @override
  List<Object> get props => [];
}

class PaymentMethodsInitial extends PaymentMethodsState {}
class PaymentMethodsLoading extends PaymentMethodsState {}
class PaymentMethodsError extends PaymentMethodsState {}
class PaymentMethodsLoaded extends PaymentMethodsState {
  final PaymentMethodResponse paymentMethods;

  PaymentMethodsLoaded(this.paymentMethods);
}
