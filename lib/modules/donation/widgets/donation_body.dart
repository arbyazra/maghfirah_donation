import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:formz/formz.dart';
import '../../modules.dart';

class DonationBody extends StatefulWidget {
  @override
  _DonationBodyState createState() => _DonationBodyState();
}

class _DonationBodyState extends State<DonationBody> {
  @override
  void initState() {
    context.read<DonationTypeBloc>().add(GetDonationType(isMyDonation: false));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<DonationTypeBloc, DonationTypeState>(
      listener: (context, state) {
        if (state is DonationTypeLoaded) {
          context.read<DonationTabCubit>().load(result: state.result);
        }
      },
      builder: (context, state) {
        if (state is DonationTypeLoaded) {
          return _buildTab(state);
        }
        return Center(child: CircularProgressIndicator());
      },
    );
  }

  BlocBuilder<DonationTabCubit, DonationTabState> _buildTab(DonationTypeLoaded result) {
    return BlocBuilder<DonationTabCubit, DonationTabState>(
      builder: (context, state) {
        switch (state.status) {
          case FormzStatus.submissionSuccess:
            return DefaultTabController(
              length: context.read<DonationTabCubit>().categories.length,
              child: _buildBody(result),
            );

          case FormzStatus.submissionFailure:
            return Center(
              child: Text("an error occured"),
            );
          default:
            return Center(child: CircularProgressIndicator());
        }
      },
    );
  }

  Widget _buildBody(DonationTypeLoaded state) {
    return Column(
      children: [
        DonationTab(
          categories: state.result,
          tabs: context
              .read<DonationTabCubit>()
              .categories
              .map((e) => e.category)
              .toList(),
        ),
        Expanded(
          child: BlocBuilder<DonationTabCubit, DonationTabState>(
            builder: (context, state) {
              if (state.status.isSubmissionFailure) {
                if (state.status.isSubmissionFailure) {
                  return Center(
                    child: Text("an error occured"),
                  );
                }
              }
              if (state.status.isSubmissionSuccess) {
                return SingleChildScrollView(
                  child: DonationDataView(),
                );
              }
              return Center(child: CircularProgressIndicator());
            },
          ),
        ),
      ],
    );
  }
}
