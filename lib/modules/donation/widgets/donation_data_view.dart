import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:maghfirah_donation/components/components.dart';
import 'package:maghfirah_donation/shared/shared.dart';

import '../../modules.dart';

class DonationDataView extends StatefulWidget {
  @override
  _DonationDataState createState() => _DonationDataState();
}

class _DonationDataState extends State<DonationDataView> {
  @override
  void initState() {
    super.initState();
    context.read<DonationBloc>().add(
          GetDonation(
            (context.read<DonationTabCubit>().categories.first.id+1).toString(),
            context.read<DonationTabCubit>().currentType,
          ),
        );
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<DonationTabCubit, DonationTabState>(
      listenWhen: (previous, current) => previous.curIdx != current.curIdx,
      listener: (context, state) {
        print(state.curIdx+1);
        context.read<DonationBloc>().add(GetDonation(
              context.read<DonationTabCubit>().currentIndex.toString(),
              context.read<DonationTabCubit>().currentType,
            ));
      },
      child: BlocBuilder<DonationBloc, DonationState>(
        builder: (context, state) {
          if (state is DonationLoaded) {
            if (state.result.data==null|| state.result.data.isEmpty) {
              return Padding(
                padding: EdgeInsets.only(top: Dimens.height(context) * .25),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    SizedBox(
                      height: 120,
                      child: Image.asset(
                        AssetPaths.noItems,
                      ),
                    ),
                    SizedBox(
                      height: 25,
                    ),
                    H3Atm(
                      "Belum ada kegiatan",
                      style: TextStylesTheme.primaryText,
                    )
                  ],
                ),
              );
            }
            return DonationListView(
              hasTitle: false,
              donations: state.result.data,
            );
          }
          return SizedBox(
            height: Dimens.height(context) * .65,
            child: Center(
              child: CircularProgressIndicator(),
            ),
          );
        },
      ),
    );
  }
}
