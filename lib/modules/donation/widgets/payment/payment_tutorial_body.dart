import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:maghfirah_donation/components/components.dart';
import 'package:maghfirah_donation/shared/shared.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import '../../../modules.dart';

class PaymentTutorialBody extends StatefulWidget {
  final String paymentId;
  final bool isBank;
  const PaymentTutorialBody({Key key, this.paymentId, this.isBank})
      : super(key: key);
  @override
  _PaymentTutorialPageState createState() => _PaymentTutorialPageState();
}

class _PaymentTutorialPageState extends State<PaymentTutorialBody> {
  final scaffoldKey = GlobalKey<ScaffoldState>();
  RefreshController _refreshController =
      RefreshController(initialRefresh: false);
  @override
  void initState() {
    super.initState();
    context
        .read<PaymentTutorialBloc>()
        .add(GetPaymentTutorial(widget.paymentId));
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<PaymentTutorialBloc, PaymentTutorialState>(
      listener: (context, state) {
        if (state is PaymentTutorialInitial) {
          context
              .read<PaymentTutorialBloc>()
              .add(GetPaymentTutorial(widget.paymentId));
        }
      },
      builder: (context, state) {
        if (state is PaymentTutorialError) {
          return _buildErrorState();
        }
        if (state is PaymentTutorialLoaded) {
          return Scaffold(
            key: scaffoldKey,
            appBar: DefaultAppBarAtm(
              title: "Selesaikan Pembayaran",
            ),
            bottomNavigationBar: _buildBottomButton(context, state),
            body: SmartRefresher(
              controller: _refreshController,
              onRefresh: () => context
                  .read<PaymentTutorialBloc>()
                  .add(ResetPaymentTutorial()),
              child: _SuccessState(
                state: state,
                isBank: widget.isBank,
                scaffoldKey: scaffoldKey,
              ),
            ),
          );
        }
        return Scaffold(
            key: scaffoldKey,
            appBar: DefaultAppBarAtm(
              title: "Selesaikan Pembayaran",
            ),
            body: Center(
              child: CircularProgressIndicator(),
            ));
      },
    );
  }

  Widget _buildBottomButton(BuildContext context, PaymentTutorialLoaded state) {
    return Padding(
      padding: const EdgeInsets.all(14),
      child: DefaultButtonMol(
        text: "Saya Sudah Transfer",
        whiteMode: false,
        onClick: () => Navigator.push(
            context,
            PaymentDetailPage(
              paymentTutorial: state.result.data,
            ).route()),
      ),
    );
  }

  Widget _buildErrorState() {
    return Scaffold(
      appBar: DefaultAppBarAtm(
        title: "Selesaikan Pembayaran",
      ),
      body: Center(
        child: H3Atm("Ups, Ada kesalahan, coba refresh halaman ini"),
      ),
    );
  }
}

class _SuccessState extends StatelessWidget {
  final PaymentTutorialLoaded state;
  final GlobalKey<ScaffoldState> scaffoldKey;
  final bool isBank;
  const _SuccessState({Key key, this.state, this.scaffoldKey, this.isBank})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 40),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            _buildHeader(),
            SizedBox(
              height: 28,
            ),
            _buildNominalAndWarning(),
            SizedBox(height: 20),
            _buildQRISDisplay(context),
            _buildBANKDisplay(),
            SizedBox(height: 24),
            _buildTimeLimit(),
            SizedBox(height: 28),
            DefaultDividerAtm(
              thickness: 8,
            ),
            SizedBox(height: 20),
            isBank != null ? _buildInstructions() : SizedBox(),
            SizedBox(height: 100),
          ],
        ),
      ),
    );
  }

  Widget _buildHeader() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          H1Atm(
            "Yuk Selangkah Lagi Anda Berhasil",
            style: TextStylesTheme.titleText,
            align: TextAlign.center,
          ),
          SizedBox(
            height: 16,
          ),
          H2Atm(
            "Transfer sesuai nominal dibawah ini :",
            style: TextStylesTheme.contentText,
            align: TextAlign.center,
          )
        ],
      ),
    );
  }

  Widget _buildNominalAndWarning() {
    var totalSplitted = NumberFormat.decimalPattern()
        .format(state.result.data.transaction.nominal)
        .split(".");
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              RichText(
                text: TextSpan(
                    text: "Rp. " +
                        List.generate(totalSplitted.length - 1,
                            (index) => totalSplitted[index]).join('.') +
                        ".",
                    style: TextStylesTheme.titleText.copyWith(
                      letterSpacing: 0,
                      color: Colors.black,
                      fontSize: 26,
                    ),
                    children: [
                      TextSpan(
                        text: totalSplitted.last,
                        style: TextStylesTheme.titleText.copyWith(
                          color: totalSplitted.last != "000"
                              ? Color(0xFFF7AF02)
                              : Colors.black,
                          letterSpacing: 0,
                          fontSize: 26,
                        ),
                      )
                    ]),
              ),
              SizedBox(
                width: 14,
              ),
              InkWell(
                  onTap: () => Utils.copyToClipboard(
                      state.result.data.transaction.nominal.toString(),
                      scaffoldKey),
                  child: Icon(
                    Icons.content_copy,
                    size: 20,
                    color: Colors.grey,
                  )),
            ],
          ),
          SizedBox(height: 24),
          totalSplitted.last != "000" ? _buildWarning() : SizedBox(),
        ],
      ),
    );
  }

  Widget _buildWarning() {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 10, vertical: 8),
      decoration: BoxDecoration(
          border: Border.fromBorderSide(BorderSide(color: Color(0xFFF7AF02))),
          borderRadius: BorderRadius.circular(4),
          color: Color(0xFFFFF8E5)),
      child: RichText(
        text: TextSpan(
            text: "PENTING! ",
            style: TextStylesTheme.sectionTitleText.copyWith(
              letterSpacing: 0,
              fontSize: 13,
              color: AppColors.textColor,
            ),
            children: [
              TextSpan(
                text:
                    "Mohon transfer tepat sampai 3 angka terakhir agar donasi anda lebih mudah  diverifikasi",
                style: TextStylesTheme.contentText.copyWith(
                  letterSpacing: 0,
                  fontSize: 13,
                  color: AppColors.textColor,
                ),
              )
            ]),
      ),
    );
  }

  Widget _buildQRISDisplay(BuildContext context) {
    return state.result.data.payment.qrUrl != null
        ? Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16),
            child: ImageURLAtm(
              imageUrl: state.result.data.payment.qrUrl,
              hasBorder: false,
              width: 180,
              height: 180,
              borderRadius: BorderRadius.circular(1),
            ),
          )
        : SizedBox();
  }

  Widget _buildBANKDisplay() {
    return state.result.data.payment.qrUrl == null
        ? Container(
            height: 65,
            margin: EdgeInsets.symmetric(horizontal: 16),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(6),
              border: Border.fromBorderSide(
                BorderSide(
                  color: Colors.grey,
                  width: 1,
                ),
              ),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Padding(
                  padding: const EdgeInsets.only(left: 16),
                  child: ImageURLAtm(
                    imageUrl:
                        "${Config.endpoint}/${state.result.data.transaction.icon}",
                    hasBorder: false,
                    width: 70,
                    bgColor: Colors.transparent,
                    fit: BoxFit.fitWidth,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(right: 6),
                  child: H2Atm(
                    state.result.data.payment.payCode,
                    style: TextStylesTheme.sectionTitleText,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(right: 26),
                  child: InkWell(
                    onTap: () => Utils.copyToClipboard(
                        state.result.data.payment.payCode, scaffoldKey),
                    child: Icon(
                      Icons.content_copy,
                      size: 20,
                      color: Colors.grey,
                    ),
                  ),
                ),
              ],
            ),
          )
        : SizedBox();
  }

  Widget _buildTimeLimit() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16),
      child: RichText(
        text: TextSpan(
            text: "Silahkan melakukan transfer sebelum ",
            style: TextStylesTheme.sectionTitleText.copyWith(
              letterSpacing: 0,
              fontSize: 14,
              fontWeight: FontWeight.w400,
              color: AppColors.textColor,
            ),
            children: [
              TextSpan(
                text: "${state.result.data.transaction.timeLimit} WIB",
                style: TextStylesTheme.contentText.copyWith(
                  letterSpacing: 0,
                  fontSize: 14,
                  fontWeight: FontWeight.w700,
                  color: Colors.black,
                ),
              ),
              TextSpan(
                text:
                    ", atau transaksi akan otomatis dibatalkan oleh sistem kami",
                style: TextStylesTheme.contentText.copyWith(
                  letterSpacing: 0,
                  fontSize: 14,
                  fontWeight: FontWeight.w400,
                  color: AppColors.textColor,
                ),
              ),
            ]),
      ),
    );
  }

  Widget _buildInstructions() {
    return !isBank
        ? Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                H1Atm(
                  "Panduan Pembayaran",
                  style: TextStylesTheme.titleText,
                ),
                ListView.separated(
                  separatorBuilder: (context, index) => DefaultDividerAtm(
                    thickness: 1,
                    color: Colors.grey[300],
                  ),
                  shrinkWrap: true,
                  physics: NeverScrollableScrollPhysics(),
                  itemCount: state.result.data.payment.instructions.length,
                  itemBuilder: (context, index) {
                    var data = state.result.data.payment.instructions[index];
                    return PaymentItemInstruction(
                      instruction: data,
                      isFirstItem: index == 0,
                    );
                  },
                ),
              ],
            ),
          )
        : SizedBox();
  }
}
