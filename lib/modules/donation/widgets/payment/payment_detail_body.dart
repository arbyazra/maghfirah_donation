import 'dart:io';

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'package:maghfirah_donation/components/components.dart';
import 'package:maghfirah_donation/modules/donation/donation.dart';
import 'package:maghfirah_donation/shared/shared.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class PaymentDetailBody extends StatefulWidget {
  final PaymentTutorial paymentTutorial;
  final GlobalKey<ScaffoldState> scaffoldKey;
  const PaymentDetailBody({Key key, this.paymentTutorial, this.scaffoldKey})
      : super(key: key);
  @override
  _PaymentDetailBodyState createState() => _PaymentDetailBodyState();
}

class _PaymentDetailBodyState extends State<PaymentDetailBody> {
  PaymentProofCubit _bloc;
  PaymentProofState _state;
  PickedFile _imageFile;
  @override
  void initState() {
    super.initState();
    _bloc = context.read<PaymentProofCubit>();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: widget.scaffoldKey,
      appBar: DefaultAppBarAtm(
        title: "Detail Pembayaran",
      ),
      bottomNavigationBar: _buildBottomButton(context),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.symmetric(
            vertical: 20,
          ),
          child: BlocBuilder<PaymentProofCubit, PaymentProofState>(
            builder: (context, state) {
              _state = state;
              return Column(
                children: [
                  SizedBox(height: 20),
                  _buildHeader(),
                  SizedBox(height: 32),
                  _buildPaymentDetails(),
                  SizedBox(height: 32),
                  DefaultDividerAtm(
                    thickness: 8,
                  ),
                  SizedBox(height: 32),
                  _buildPaymentProofField(),
                ],
              );
            },
          ),
        ),
      ),
    );
  }

  Widget _buildBottomButton(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(14),
      child: DefaultButtonMol(
        text: "Kirim Bukti Pembayaran",
        whiteMode: false,
        onClick: () {
          if (_state.file != null) {
            _bloc.send(widget.paymentTutorial.transaction.id.toString());
          } else {
            widget.scaffoldKey.currentState.showSnackBar(SnackBar(
              content: H3Atm("Cek kolom file"),
              duration: Duration(milliseconds: 1500),
            ));
          }
        },
      ),
    );
  }

  Widget _buildHeader() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16),
      child: Column(
        children: [
          H1Atm(
            "Menunggu Pembayaran Anda",
            style: TextStylesTheme.titleText,
          ),
          SizedBox(
            height: 14,
          ),
          H2Atm(
            "Segera lakukan transfer sebelum",
            style: TextStylesTheme.contentText,
          ),
          SizedBox(
            height: 2,
          ),
          H2Atm(
            widget.paymentTutorial.transaction.timeLimit + " WIB",
            style: TextStylesTheme.sectionTitleText
                .copyWith(fontWeight: FontWeight.w500),
          ),
        ],
      ),
    );
  }

  Widget _buildPaymentDetails() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16),
      child: Column(
        children: [
          _buildRow("Metode Pembayaran",
              widget.paymentTutorial.transaction.paymentMethod),
          DefaultDividerAtm(),
          _buildRow("ID Sedekah", widget.paymentTutorial.transaction.reference),
          DefaultDividerAtm(),
          _buildRow(
            "Jumlah Sedekah",
            NumberFormat.simpleCurrency(locale: 'id')
                .format(widget.paymentTutorial.transaction.nominal),
          ),
          DefaultDividerAtm(),
          _buildRow(
            "Status",
            Utils.getStatusOrder(widget.paymentTutorial.transaction.status),
          ),
        ],
      ),
    );
  }

  Widget _buildRow(String key, String value) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 14),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          H2Atm(
            key,
            style: TextStylesTheme.contentText,
          ),
          H2Atm(
            value,
            style: TextStylesTheme.sectionTitleText
                .copyWith(fontWeight: FontWeight.w500),
          ),
        ],
      ),
    );
  }

  Widget _buildPaymentProofField() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              H1Atm(
                "Bukti Pembayaran",
                style: TextStylesTheme.titleText,
                align: TextAlign.left,
              ),
              _imageFile != null
                  ? InkWell(
                      onTap: _showImagePicker,
                      child: H3Atm(
                        "Ganti Gambar",
                        style: TextStylesTheme.titleText
                            .copyWith(color: AppColors.primaryColor),
                      ),
                    )
                  : SizedBox(),
            ],
          ),
          SizedBox(height: 18),
          Ink(
            decoration: BoxDecoration(
              border: Border.fromBorderSide(
                BorderSide(
                  color: Colors.grey,
                ),
              ),
              borderRadius: BorderRadius.circular(6),
            ),
            child: InkWell(
              onTap: _showImagePicker,
              child: Container(
                padding: EdgeInsets.all(12),
                height: _imageFile != null ? null : 150,
                width: double.maxFinite,
                child: _imageFile != null
                    ? Image.file(
                        File(_imageFile.path),
                        fit: BoxFit.fitHeight,
                      )
                    : Center(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Icon(
                              Icons.camera_alt,
                              color: Colors.black.withOpacity(.15),
                            ),
                            SizedBox(
                              height: 4,
                            ),
                            H3Atm(
                              "No file choosen",
                              style: TextStylesTheme.contentText.copyWith(
                                color: Colors.black.withOpacity(.15),
                              ),
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            H3Atm(
                              "Choose File",
                              style: TextStylesTheme.contentText.copyWith(
                                color: Colors.green,
                              ),
                            ),
                          ],
                        ),
                      ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  void _showImagePicker() async {
    var isFromCamera = await Utils.showSourcesCamera(context);

    try {
      final pickedFile = await ImagePicker().getImage(
        source: isFromCamera ? ImageSource.camera : ImageSource.gallery,
      );

      setState(() {
        _imageFile = pickedFile ?? null;
      });
      _bloc.onFileChanged(_imageFile != null ? File(_imageFile.path) : null);
    } catch (e) {
      print(e);
    }
  }
}
