import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart' as intl;
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:maghfirah_donation/components/components.dart';
import 'package:maghfirah_donation/modules/donation/donation.dart';
import 'package:maghfirah_donation/shared/shared.dart';

class DonatingBody extends StatefulWidget {
  final Donation donation;
  final String price;
  final GlobalKey<ScaffoldState> scaffoldKey;
  const DonatingBody(
      {Key key, this.scaffoldKey, this.donation, this.price})
      : super(key: key);
  @override
  _DonatingBodyState createState() => _DonatingBodyState();
}

class _DonatingBodyState extends State<DonatingBody> {
  bool nameVisibility = false;
  bool isPrayingActivated = true;

  final nominalCon = TextEditingController();

  DonatingCubit _bloc;
  DonatingState _state;

  @override
  void initState() {
    super.initState();
    _bloc = context.read<DonatingCubit>();
    if (widget.price != null) {
      _bloc.onNominalChanged(widget.price);
      nominalCon.text = intl.NumberFormat.decimalPattern('id').format(int.parse(widget.price));
    }
  }

  String nominalErrorText(DonatingState state) =>
      state.nominal > 0 ? null : Strings.nominalErrorText;

  String paymentMethodErrorText(DonatingState state) =>
      state.paymentMethodCode != null ? null : Strings.paymentMethodErrorText;

  void _onPayTapped() async {
    if (_state.nominal == 0 || _state.paymentMethodCode == null) {
      widget.scaffoldKey.currentState.showSnackBar(SnackBar(
        content: H3Atm("Cek kolom yang belum Anda isi"),
        duration: Duration(milliseconds: 1500),
      ));
      return;
    }
    _bloc.donate(widget.donation.id);
  }

  @override
  build(BuildContext context) {
    return BlocBuilder<DonatingCubit, DonatingState>(
      builder: (context, state) {
        _state = state;
        return SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.fromLTRB(14, 16, 14, 100),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                _buildHeader(),
                SizedBox(height: Dimens.dp20),
                CalculatorTextField(
                  controller: nominalCon,
                  onChanged: (value) =>
                      _bloc.onNominalChanged(value.split(".").join()),
                  title: Strings.fieldDonationAmount,
                  errorText: nominalErrorText(state),
                ),
                _buildDonationAmountOptions(),
                SizedBox(height: Dimens.dp20),
                _buildPaymentMethod(state),
                SizedBox(height: Dimens.dp10),
                _buildNameVisibilityOptions(),
                _buildPrayingOptions(state),
                _buildDonatedPray(),
                SizedBox(
                  height: 20,
                ),
                DefaultButtonMol(
                  text: "Lanjut Pembayaran",
                  whiteMode: false,
                  onClick: _onPayTapped,
                ),
              ],
            ),
          ),
        );
      },
    );
  }

  Widget _buildHeader() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        H3Atm(
          "Anda akan bersedekah pada program:",
          style: TextStylesTheme.contentText,
        ),
        SizedBox(
          height: Dimens.dp10,
        ),
        H1Atm(
          widget.donation.title,
          style: TextStylesTheme.titleText,
        ),
      ],
    );
  }

  Widget _buildDonationAmountOptions() {
    return widget.price != null
        ? SizedBox()
        : Padding(
          padding: const EdgeInsets.only(top: 14),
          child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                H2Atm(
                  "Atau Pilih Nominal Donasi",
                  style: TextStylesTheme.contentText
                      .copyWith(fontWeight: FontWeight.w500),
                ),
                SizedBox(
                  height: 12,
                ),
                GridView.builder(
                  shrinkWrap: true,
                  physics: NeverScrollableScrollPhysics(),
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 2,
                      crossAxisSpacing: 18,
                      mainAxisSpacing: 18,
                      childAspectRatio: 3.6),
                  itemCount: DonatingDummy.donationAmounts.length,
                  itemBuilder: (context, index) {
                    var data = DonatingDummy.donationAmounts[index];
                    return Container(
                      decoration: BoxDecoration(
                          border: Border.fromBorderSide(
                              BorderSide(color: Colors.grey[300])),
                          borderRadius: BorderRadius.circular(8)),
                      child: InkWell(
                        onTap: () {
                          _bloc.onNominalChanged(data.toInt().toString());
                          nominalCon.text = intl.NumberFormat.decimalPattern('id')
                              .format(data.toInt());
                        },
                        child: Center(
                          child: H2Atm(
                              intl.NumberFormat.simpleCurrency(locale: "id_ID")
                                  .format(data),
                              style: TextStylesTheme.contentText),
                        ),
                      ),
                    );
                  },
                ),
              ],
            ),
        );
  }

  Widget _buildPaymentMethod(DonatingState state) {
    return SizedBox(
      child: DefaultTextFieldAtm(
        hasOutlineBorder: true,
        textDirection: TextDirection.rtl,
        keyboardType: TextInputType.number,
        errorText: paymentMethodErrorText(state),
        hintText: state.paymentMethodCode != null
            ? state.paymentMethodCode.name ?? state.paymentMethodCode.bankName
            : Strings.fieldPaymentMethod,
        onTap: () {
          print('onTap : should be navigating to payment method page');
          Navigator.push(context, PaymentOptionsPage().route());
        },
        hintStyle: TextStylesTheme.contentText
            .copyWith(height: 1.2, color: Colors.black),
        suffixIcon: SizedBox(
          width: 60,
          child: IconButton(
            onPressed: () {},
            padding: const EdgeInsets.symmetric(horizontal: Dimens.dp14),
            icon: H3Atm(
              "Pilih",
              style: TextStyle(
                  fontWeight: FontWeight.w500, color: AppColors.primaryColor),
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildNameVisibilityOptions() {
    return SwitchListTile(
      contentPadding: EdgeInsets.zero,
      title: H2Atm(
        "Sembunyikan nama saya (hamba Allah)",
        style: TextStylesTheme.contentText,
      ),
      value: nameVisibility,
      onChanged: (v) {
        setState(() {
          nameVisibility = !nameVisibility;
        });
        _bloc.onAnonymousChanged(nameVisibility ? 1 : 0);
      },
    );
  }

  Widget _buildPrayingOptions(DonatingState state) {
    return Column(
      children: [
        SwitchListTile(
          contentPadding: EdgeInsets.zero,
          title: H2Atm(
            "Doa yang diharapkan",
            style: TextStylesTheme.contentText
                .copyWith(fontWeight: FontWeight.w500),
          ),
          value: isPrayingActivated,
          onChanged: (v) {
            setState(() {
              isPrayingActivated = !isPrayingActivated;
            });
            _bloc.onWishMessageChanged(
                isPrayingActivated ? state.wishMessage : null);
          },
        ),
        SizedBox(
          height: 6,
        ),
        AnimatedCrossFade(
          duration: Duration(milliseconds: 500),
          crossFadeState: isPrayingActivated
              ? CrossFadeState.showFirst
              : CrossFadeState.showSecond,
          firstChild: Container(
            margin: EdgeInsets.only(bottom: 12),
            decoration: BoxDecoration(
                border: Border.fromBorderSide(
                    BorderSide(color: Colors.grey, width: 0.6)),
                borderRadius: BorderRadius.circular(6)),
            padding:
                const EdgeInsets.only(left: 12, right: 12, bottom: 10, top: 4),
            child: DefaultTextFieldAtm(
              hasBorder: false,
              onChanged: (value) => _bloc.onWishMessageChanged(value),
              hintText: Strings.fieldDonorPrays,
              maxLines: 7,
              maxLength: 140,
            ),
          ),
          sizeCurve: Curves.easeInOutQuint,
          secondChild: SizedBox(),
        )
      ],
    );
  }

  Widget _buildDonatedPray() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        H2Atm(
          "Doa Untuk Yang Didonasikan",
          style:
              TextStylesTheme.contentText.copyWith(fontWeight: FontWeight.w500),
        ),
        SizedBox(
          height: 16,
        ),
        Container(
          decoration: BoxDecoration(
              border: Border.fromBorderSide(
                  BorderSide(color: Colors.grey, width: 0.6)),
              borderRadius: BorderRadius.circular(6)),
          padding:
              const EdgeInsets.only(left: 12, right: 12, bottom: 10, top: 4),
          child: DefaultTextFieldAtm(
            hasBorder: false,
            onChanged: (value) => _bloc.onSpecialMessageChanged(value),
            hintText: Strings.fieldDonorPrays,
            maxLines: 7,
            maxLength: 140,
          ),
        )
      ],
    );
  }
}
