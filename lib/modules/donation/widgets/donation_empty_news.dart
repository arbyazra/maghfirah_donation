import 'package:flutter/material.dart';
import 'package:maghfirah_donation/components/components.dart';
import 'package:maghfirah_donation/shared/shared.dart';

class DonationEmptyNews extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Center(
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                height: 100,
                child: Image.asset(AssetPaths.noItems),
              ),
              SizedBox(
                height: 12,
              ),
              H2Atm(
                "Belum ada kabar terbaru",
                style: TextStyle(fontWeight: FontWeight.w500),
              )
            ],
          ),
        ),
      ),
    );
  }
}
