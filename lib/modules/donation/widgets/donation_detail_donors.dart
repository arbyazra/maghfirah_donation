import 'package:flutter/material.dart';
import 'package:maghfirah_donation/components/components.dart';
import 'package:maghfirah_donation/shared/shared.dart';

import '../../modules.dart';

class DonationDetailDonors extends StatelessWidget {
  final DonationDonatursResponse donaturs;
  final hasMuchDonors;
  const DonationDetailDonors(
      {Key key, this.hasMuchDonors = false, this.donaturs})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(
          horizontal: Dimens.dp16, vertical: Dimens.dp20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          H1Atm(
            "Donatur ${donaturs.data.length > 0 ? "(${donaturs.data.length})" : ""}",
            // activity.title,
            maxLine: 4,
            overflow: TextOverflow.ellipsis,
            style: TextStylesTheme.titleText,
          ),
          SizedBox(height: Dimens.dp24),
          donaturs.data.length == 0
              ? DonationEmptyDonaturs()
              : ListView.separated(
                  shrinkWrap: true,
                  physics: NeverScrollableScrollPhysics(),
                  itemCount: hasMuchDonors
                      ? donaturs.data.length
                      : donaturs.data.length > 3
                          ? 3
                          : donaturs.data.length,
                  padding: EdgeInsets.only(right: Dimens.dp10),
                  separatorBuilder: (context, index) => SizedBox(
                    height: Dimens.dp12,
                  ),
                  itemBuilder: (context, index) => DonationDonorsItemCard(
                    donatur: donaturs.data[index],
                  ),
                ),
          SizedBox(
            height: Dimens.dp18,
          ),
          donaturs.data.length > 3
              ? !hasMuchDonors
                  ? GestureDetector(
                      onTap: () => Navigator.push(
                          context,
                          DonationDonorsDetailPage(
                            donaturs: donaturs,
                          ).route()),
                      child: Container(
                        width: double.maxFinite,
                        child: H2Atm("Lihat Semua",
                            style: TextStylesTheme.contentText
                                .copyWith(color: AppColors.primaryColor)),
                      ),
                    )
                  : SizedBox()
              : SizedBox(),
        ],
      ),
    );
  }
}
