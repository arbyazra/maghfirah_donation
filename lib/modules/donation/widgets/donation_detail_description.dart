import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:maghfirah_donation/components/components.dart';
import 'package:maghfirah_donation/shared/shared.dart';

import '../../modules.dart';

class DonationDetailDescription extends StatefulWidget {
  final Donation donation;

  const DonationDetailDescription({Key key, this.donation}) : super(key: key);

  @override
  _DonationDetailDescriptionState createState() =>
      _DonationDetailDescriptionState();
}

class _DonationDetailDescriptionState extends State<DonationDetailDescription> {
  bool isMsgExpanded = false;
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(
          horizontal: Dimens.dp14, vertical: Dimens.dp16),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          H1Atm(
            "Deskripsi",
            maxLine: 4,
            overflow: TextOverflow.ellipsis,
            style: TextStylesTheme.titleText,
          ),
          SizedBox(height: Dimens.dp10),
          Html(
            data: Utils.removeAllHtmlTags(widget.donation.content).length > 1000
                ? isMsgExpanded
                    ? widget.donation.content
                    : widget.donation.content.substring(0, 1000)
                : widget.donation.content,
          ),
          // H2Atm(
          //   Html(
          //     data: widget.donation.content,
          //   ).data,
          //   maxLine: isMsgExpanded ? 200 : 4,
          //   overflow: TextOverflow.ellipsis,
          //   style: TextStylesTheme.contentText,
          // ),
          // SizedBox(height: Dimens.dp10),
          _buildDescriptionText(),
          SizedBox(
            height: 18,
          ),
        ],
      ),
    );
  }

  Widget _buildDescriptionText() {
    return Utils.removeAllHtmlTags(widget.donation.content).length>1000? InkWell(
            onTap: () {
              isMsgExpanded = !isMsgExpanded;
              setState(() {});
            },
            child: H2Atm(!isMsgExpanded ? "Baca Selengkapnya" : "Secukupnya",
                style: TextStylesTheme.contentText
                    .copyWith(color: AppColors.primaryColor)),
          ):SizedBox();
    // return LayoutBuilder(
    //   builder: (context, size) {
    //     final span = TextSpan(
    //       text: Html(
    //         data: widget.donation.content,
    //       ).data,
    //       style: TextStylesTheme.contentText,
    //     );
    //     final tp = TextPainter(
    //         text: span, maxLines: 6, textDirection: TextDirection.rtl);
    //     tp.layout(maxWidth: size.maxWidth);

    //     if (tp.didExceedMaxLines) {
    //       return InkWell(
    //         onTap: () {
    //           isMsgExpanded = !isMsgExpanded;
    //           setState(() {});
    //         },
    //         child: H2Atm(!isMsgExpanded ? "Baca Selengkapnya" : "Secukupnya",
    //             style: TextStylesTheme.contentText
    //                 .copyWith(color: AppColors.primaryColor)),
    //       );
    //     } else {
    //       return SizedBox();
    //     }
    //   },
    // );
  }
}
