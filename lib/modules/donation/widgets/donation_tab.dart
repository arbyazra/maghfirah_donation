import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:maghfirah_donation/shared/shared.dart';
import 'package:formz/formz.dart';
import '../../modules.dart';

class DonationTab extends StatefulWidget {
  final List<String> tabs;
  final dynamic categories;
  const DonationTab({Key key, this.tabs, this.categories}) : super(key: key);
  @override
  _DonationTabState createState() => _DonationTabState();
}

class _DonationTabState extends State<DonationTab> {
  DonationTabCubit _bloc;
  @override
  void initState() {
    super.initState();
    _bloc = context.read<DonationTabCubit>();
    _bloc.load(result: widget.categories);
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<DonationTabCubit, DonationTabState>(
      builder: (context, state) {
        if (state.status.isSubmissionSuccess) {
          return _buildTabs(state);
        }
        if (state.status.isSubmissionFailure) {
          return Center(
            child: Text("an error occured"),
          );
        }
        return Container(
          height: 40,
          child: Center(child: CircularProgressIndicator()),
        );
      },
    );
  }

  Widget _buildTabs(DonationTabState state) {
    return Container(
      color: Colors.white,
      child: Container(
        height: 40,
        width: double.maxFinite,
        margin: const EdgeInsets.only(top: Dimens.dp16, bottom: Dimens.dp10),
        child: TabBar(
          isScrollable: true,
          onTap: (value) {
            _bloc.change(value);
          },
          indicator: BoxDecoration(),
          labelColor: Colors.white,
          unselectedLabelColor: AppColors.primaryColor,
          indicatorPadding: EdgeInsets.zero,
          labelPadding: EdgeInsets.zero,
          tabs: _bloc.categories
              .map((e) => _buildTabItem(e.category, state))
              .toList(),
        ),
      ),
    );
  }

  Widget _buildTabItem(String e, DonationTabState state) {
    return Padding(
      padding: EdgeInsets.only(
          right: widget.tabs.indexOf(e) == widget.tabs.length - 1
              ? Dimens.dp16
              : 10,
          left: widget.tabs.indexOf(e) == 0 ? Dimens.dp16 : 0),
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: Dimens.dp10),
        decoration: BoxDecoration(
          color: state.curIdx == widget.tabs.indexOf(e)
              ? AppColors.primaryColor
              : Colors.white,
          border: Border.fromBorderSide(BorderSide(
              color: state.curIdx == widget.tabs.indexOf(e)
                  ? Colors.transparent
                  : Colors.grey[400])),
          borderRadius: BorderRadius.circular(6),
        ),
        child: Tab(
          text: e,
        ),
      ),
    );
  }
}
