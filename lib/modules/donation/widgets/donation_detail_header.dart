import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:maghfirah_donation/components/components.dart';
import 'package:maghfirah_donation/shared/shared.dart';
import 'package:percent_indicator/linear_percent_indicator.dart';

import '../../modules.dart';

class DonationDetailHeader extends StatelessWidget {
  final Donation donation;

  const DonationDetailHeader({Key key, this.donation}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: Dimens.dp14),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          _buildDonationName(),
          SizedBox(
            height: Dimens.dp10,
          ),
          _buildTargetDonation(),
          _buildDonorProgress(context),
          _buildDonationDurationAndDonors(),
          _buildWakafPrice(),
          donation.type != "zakat"
              ? SizedBox()
              : Padding(
                padding: const EdgeInsets.only(top: 16),
                child: DefaultButtonMol(
                    text: "Buka Kalkulator Zakat",
                    whiteMode: true,
                    onClick: () {
                      Navigator.push(
                          context,
                          ZakatPage(
                            isFromDonation: true,
                          ).route());
                    },
                  ),
              ),
          SizedBox(
            height: 28,
          ),
        ],
      ),
    );
  }

  Widget _buildDonationName() {
    return Padding(
      padding: const EdgeInsets.only(
        top: Dimens.dp20,
        bottom: Dimens.dp4,
      ),
      child: H1Atm(
        donation.title,
        // activity.title,
        maxLine: 4,
        overflow: TextOverflow.ellipsis,
        style: TextStyle(
            fontWeight: FontWeight.w700,
            height: 1.1,
            letterSpacing: 0.8,
            color: Color(0xFF353535)),
      ),
    );
  }

  Widget _buildWakafPrice() {
    return donation.wakafUnit == null
        ? SizedBox()
        : Padding(
            padding: const EdgeInsets.only(top: 6),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                H3Atm(
                  "Harga per satuan",
                  style: TextStylesTheme.contentText,
                ),
                SizedBox(
                  height: 2,
                ),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    H2Atm(
                      NumberFormat.simpleCurrency(locale: 'id')
                          .format(int.parse(donation.wakafPrice)),
                      style: TextStyle(
                        fontWeight: FontWeight.w700,
                        height: 1.1,
                        letterSpacing: 0.8,
                        color: Color(0xFF353535),
                      ),
                    ),
                    SizedBox(
                      width: 2,
                    ),
                    H2Atm(
                      "/${donation.wakafUnit}",
                      style: TextStyle(
                        fontWeight: FontWeight.w700,
                        height: 1.1,
                        letterSpacing: 0.8,
                        color: Colors.grey,
                      ),
                    )
                  ],
                ),
              ],
            ),
          );
  }

  Widget _buildTargetDonation() {
    return Row(
      children: [
        H3Atm(
          NumberFormat.simpleCurrency(locale: 'id')
              .format(int.parse(donation.donations)),
          // activity.title,
          style: TextStyle(
            fontWeight: FontWeight.w500,
          ),
        ),
        Expanded(
          child: H3Atm(
            " Terkumpul ${!donation.isUnlimited ? "dari ${NumberFormat.simpleCurrency(locale: 'id').format(int.parse(donation.nominalTarget.toString()))}" : ""}",
            // activity.title,
            style: TextStyle(
              fontWeight: FontWeight.w300,
            ),
          ),
        ),
      ],
    );
  }

  Widget _buildDonorProgress(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(
        top: 8,
        bottom: 12,
      ),
      child: LinearPercentIndicator(
        width: Dimens.width(context) - 28,
        padding: EdgeInsets.symmetric(horizontal: Dimens.dp4),
        lineHeight: 8.0,
        clipLinearGradient: true,
        percent: int.parse(donation.percentage) / 100,
        backgroundColor: AppColors.bgGrey,
        progressColor: (int.parse(donation.percentage) / 100) > 0.5
            ? AppColors.primaryColor
            : Colors.orange[400],
      ),
    );
  }

  Widget _buildDonationDurationAndDonors() {
    return Row(
      children: [
        donation.isUnlimited
            ? H2Atm(
                "Tidak Terbatas",
                style: TextStyle(
                    fontWeight: FontWeight.w300, color: AppColors.greyDark),
              )
            : RichText(
                text: TextSpan(
                  text: "${donation.dateCount} ",
                  style: TextStyle(
                      fontWeight: FontWeight.w700, color: Colors.black),
                  children: <TextSpan>[
                    TextSpan(
                      text: "Hari lagi",
                      style: TextStyle(
                          fontWeight: FontWeight.w300,
                          color: AppColors.greyDark),
                    )
                  ],
                ),
              ),
        Spacer(),
        RichText(
          text: TextSpan(
            text: "${donation.donaturs} ",
            style: TextStyle(fontWeight: FontWeight.w700, color: Colors.black),
            children: <TextSpan>[
              TextSpan(
                text: "Donatur",
                style: TextStyle(
                    fontWeight: FontWeight.w300, color: AppColors.greyDark),
              )
            ],
          ),
        ),
      ],
    );
  }
}
