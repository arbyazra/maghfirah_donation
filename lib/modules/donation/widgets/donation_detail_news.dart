import 'package:flutter/material.dart';
import 'package:maghfirah_donation/components/components.dart';
import 'package:maghfirah_donation/modules/donation/donation.dart';
import 'package:maghfirah_donation/modules/donation/widgets/donation_empty_news.dart';
import 'package:maghfirah_donation/shared/shared.dart';

class DonationDetailNews extends StatelessWidget {
  final DonationNewsResponse news;

  const DonationDetailNews({Key key, this.news}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(
          horizontal: Dimens.dp14, vertical: Dimens.dp16),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          H1Atm(
            "Kabar Terbaru",
            // activity.title,
            maxLine: 4,
            overflow: TextOverflow.ellipsis,
            style: TextStylesTheme.titleText,
          ),
          SizedBox(height: Dimens.dp24),
          news.data.length == 0
              ? DonationEmptyNews()
              : DonationNewsItemCard(
                  news: news.data[0],
                ),
          news.data.length > 1
              ? Padding(
                  padding: const EdgeInsets.symmetric(vertical: 12),
                  child: GestureDetector(
                    onTap: () => Navigator.push(
                        context, DonationNewsDetailPage().route()),
                    child: Container(
                      width: double.maxFinite,
                      child: H2Atm("Kabar Baru Selengkapnya",
                          style: TextStylesTheme.contentText
                              .copyWith(color: AppColors.primaryColor)),
                    ),
                  ),
                )
              : SizedBox(
                  height: 12,
                ),
        ],
      ),
    );
  }
}
