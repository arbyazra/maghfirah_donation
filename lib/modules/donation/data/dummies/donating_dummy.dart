
class DonatingDummy{
  static List<double> get donationAmounts => [
    50000,
    100000,
    500000,
    1000000,
  ];
}