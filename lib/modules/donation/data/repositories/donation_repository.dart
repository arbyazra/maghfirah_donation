import 'dart:io';

import 'package:http/http.dart';
import 'package:maghfirah_donation/modules/donation/donation.dart';
import 'package:maghfirah_donation/shared/shared.dart';

class DonationRepository {
  final Network network;

  DonationRepository({this.network});

  Future<DonationCategoryResponse> getCategories() async {
    var url = parseUrl("/categories");
    final response = await network.getRequest(
      url,
    );
    if (response.statusCode >= 200 && response.statusCode < 300) {
      return donationCategoryResponseFromJson(response.body);
    } else {
      print(response.body);
      return donationCategoryResponseFromJson(response.body);
    }
  }

  Future<DonationResponse> getAllDonation(
      {DonationType type, String categoryId}) async {
    var url = parseUrl("/projects");
    print(url);
    final response = await network.getRequest(url);
    if (response.statusCode >= 200 && response.statusCode < 300) {
      return donationResponseFromJson(response.body);
    } else {
      print(response.body);
      return donationResponseFromJson(response.body);
    }
  }

  Future<DonationResponse> getDonation(
      {String type, String categoryId}) async {
    var url;
    if (categoryId != null) {
      url = Uri.parse(parseUrl("/projects")).replace(queryParameters: {
        // "type": type,
        "category_id": "$categoryId"
      }).toString();
    } else {
      url = parseUrl("/projects");
    }
    print(url);
    final response = await network.getRequest(url);
    if (response.statusCode >= 200 && response.statusCode < 300) {
      return donationResponseFromJson(response.body);
    } else {
      print(response.body);
      return donationResponseFromJson(response.body);
    }
  }

  Future<DonationDonatursResponse> getDonationDonaturs({String id}) async {
    var url = parseUrl("/project/$id/donation");
    final response = await network.getRequest(url);
    if (response.statusCode >= 200 && response.statusCode < 300) {
      return donationDonatursResponseFromJson(response.body);
    } else {
      print(response.body);
      return donationDonatursResponseFromJson(response.body);
    }
  }

  Future<DonationNewsResponse> getDonationNews({String id}) async {
    var url = parseUrl("/project/$id/news");
    final response = await network.getRequest(url);
    if (response.statusCode >= 200 && response.statusCode < 300) {
      return donationNewsResponseFromJson(response.body);
    } else {
      print(response.body);
      return donationNewsResponseFromJson(response.body);
    }
  }

  Future<PaymentMethodResponse> getPaymentMethods() async {
    var url = parseUrl("/payment/method");
    final response = await network.getRequest(url);
    if (response.statusCode >= 200 && response.statusCode < 300) {
      print(response.body);
      return paymentMethodResponseFromJson(response.body);
    } else {
      print(response.body);
      return paymentMethodResponseFromJson(response.body);
    }
  }

  Future<PaymentTutorialResponse> getPaymentTutorial(String id) async {
    var url = parseUrl("/transaction/$id/how-to-pay");
    final response = await network.getRequest(url);
    if (response.statusCode >= 200 && response.statusCode < 300) {
      print(response.body);
      return paymentTutorialResponseFromJson(response.body);
    } else {
      print(response.body);
      return paymentTutorialResponseFromJson(response.body);
    }
  }

  Future<PaymentResponse> donate(DonationRequest request) async {
    var url = parseUrl("/payment/request/${request.id}");
    // print(request.toJson());
    final response = await network.postRequest(
      url,
      request.toJson(),
    );
    if (response.statusCode >= 200 && response.statusCode < 300) {
      print(response.body);
      return paymentResponseFromJson(response.body);
    } else {
      print(response.body);
      return paymentResponseFromJson(response.body);
    }
  }

  Future<bool> sendProof(String id, File file) async {
    var url = parseUrl("/transaction/$id/proof");
    var request = MultipartRequest("POST", Uri.parse(url));
    var pic = await MultipartFile.fromPath("file", file.path,
        filename: file.path.split("/").last);
    request.files.add(pic);
    return await network.postFiles(
      request,
    );
  }
}
