// To parse this JSON data, do
//
//     final donationResponse = donationResponseFromJson(jsonString);

import 'dart:convert';

DonationResponse donationResponseFromJson(String str) => DonationResponse.fromJson(json.decode(str));

String donationResponseToJson(DonationResponse data) => json.encode(data.toJson());

class DonationResponse {
    DonationResponse({
        this.success,
        this.message,
        this.data,
    });

    bool success;
    String message;
    List<Donation> data;

    DonationResponse copyWith({
        bool success,
        String message,
        List<Donation> data,
    }) => 
        DonationResponse(
            success: success ?? this.success,
            message: message ?? this.message,
            data: data ?? this.data,
        );

    factory DonationResponse.fromJson(Map<String, dynamic> json) => DonationResponse(
        success: json["success"] == null ? null : json["success"],
        message: json["message"] == null ? null : json["message"],
        data: json["data"] == null ? null : List<Donation>.from(json["data"].map((x) => Donation.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "success": success == null ? null : success,
        "message": message == null ? null : message,
        "data": data == null ? null : List<dynamic>.from(data.map((x) => x.toJson())),
    };
}

class Donation {
    Donation({
        this.id,
        this.title,
        this.content,
        this.pathFeatured,
        this.nominalTarget,
        this.dateTarget,
        this.categoryId,
        this.userId,
        this.isFixed,
        this.status,
        this.createdAt,
        this.updatedAt,
        this.slug,
        this.type,
        this.wakafPrice,
        this.wakafUnit,
        this.isUnlimited,
        this.dateCount,
        this.percentage,
        this.donations,
        this.donaturs,
        this.category,
    });

    String id;
    String title;
    String content;
    dynamic pathFeatured;
    dynamic nominalTarget;
    dynamic dateTarget;
    String categoryId;
    String userId;
    String isFixed;
    String status;
    DateTime createdAt;
    DateTime updatedAt;
    String slug;
    String type;
    String wakafPrice;
    dynamic wakafUnit;
    bool isUnlimited;
    dynamic dateCount;
    String percentage;
    String donations;
    String donaturs;
    Category category;

    Donation copyWith({
        int id,
        String title,
        String content,
        dynamic pathFeatured,
        dynamic nominalTarget,
        dynamic dateTarget,
        String categoryId,
        String userId,
        String isFixed,
        String status,
        DateTime createdAt,
        DateTime updatedAt,
        String slug,
        String type,
        String wakafPrice,
        dynamic wakafUnit,
        bool isUnlimited,
        dynamic dateCount,
        int percentage,
        int donations,
        String donaturs,
        Category category,
    }) => 
        Donation(
            id: id ?? this.id,
            title: title ?? this.title,
            content: content ?? this.content,
            pathFeatured: pathFeatured ?? this.pathFeatured,
            nominalTarget: nominalTarget ?? this.nominalTarget,
            dateTarget: dateTarget ?? this.dateTarget,
            categoryId: categoryId ?? this.categoryId,
            userId: userId ?? this.userId,
            isFixed: isFixed ?? this.isFixed,
            status: status ?? this.status,
            createdAt: createdAt ?? this.createdAt,
            updatedAt: updatedAt ?? this.updatedAt,
            slug: slug ?? this.slug,
            type: type ?? this.type,
            wakafPrice: wakafPrice ?? this.wakafPrice,
            wakafUnit: wakafUnit ?? this.wakafUnit,
            isUnlimited: isUnlimited ?? this.isUnlimited,
            dateCount: dateCount ?? this.dateCount,
            percentage: percentage ?? this.percentage,
            donations: donations ?? this.donations,
            donaturs: donaturs ?? this.donaturs,
            category: category ?? this.category,
        );

    factory Donation.fromJson(Map<String, dynamic> json) => Donation(
        id: json["id"] == null ? null : json["id"].toString(),
        title: json["title"] == null ? null : json["title"],
        content: json["content"] == null ? null : json["content"],
        pathFeatured: json["path_featured"],
        nominalTarget: json["nominal_target"],
        dateTarget: json["date_target"],
        categoryId: json["category_id"] == null ? null : json["category_id"].toString(),
        userId: json["user_id"] == null ? null : json["user_id"].toString(),
        isFixed: json["is_fixed"] == null ? null : json["is_fixed"].toString(),
        status: json["status"] == null ? null : json["status"].toString(),
        createdAt: json["created_at"] == null ? null : DateTime.parse(json["created_at"]),
        updatedAt: json["updated_at"] == null ? null : DateTime.parse(json["updated_at"]),
        slug: json["slug"] == null ? null : json["slug"],
        type: json["type"] == null ? null : json["type"],
        wakafPrice: json["wakaf_price"] == null ? null : json["wakaf_price"],
        wakafUnit: json["wakaf_unit"],
        isUnlimited: json["is_unlimited"] == null ? null : json["is_unlimited"],
        dateCount: json["date_count"],
        percentage: json["percentage"] == null ? null : json["percentage"].toString(),
        donations: json["donations"] == null ? null : json["donations"].toString(),
        donaturs: json["donaturs"] == null ? null : json["donaturs"].toString(),
        category: json["category"] == null ? null : Category.fromJson(json["category"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "title": title == null ? null : title,
        "content": content == null ? null : content,
        "path_featured": pathFeatured,
        "nominal_target": nominalTarget,
        "date_target": dateTarget,
        "category_id": categoryId == null ? null : categoryId,
        "user_id": userId == null ? null : userId,
        "is_fixed": isFixed == null ? null : isFixed,
        "status": status == null ? null : status,
        "created_at": createdAt == null ? null : createdAt.toIso8601String(),
        "updated_at": updatedAt == null ? null : updatedAt.toIso8601String(),
        "slug": slug == null ? null : slug,
        "type": type == null ? null : type,
        "wakaf_price": wakafPrice == null ? null : wakafPrice,
        "wakaf_unit": wakafUnit,
        "is_unlimited": isUnlimited == null ? null : isUnlimited,
        "date_count": dateCount,
        "percentage": percentage == null ? null : percentage,
        "donations": donations == null ? null : donations,
        "donaturs": donaturs == null ? null : donaturs,
        "category": category == null ? null : category.toJson(),
    };
}

class Category {
    Category({
        this.id,
        this.pathIcon,
        this.category,
        this.createdAt,
        this.updatedAt,
    });

    int id;
    dynamic pathIcon;
    String category;
    DateTime createdAt;
    DateTime updatedAt;

    Category copyWith({
        int id,
        dynamic pathIcon,
        String category,
        DateTime createdAt,
        DateTime updatedAt,
    }) => 
        Category(
            id: id ?? this.id,
            pathIcon: pathIcon ?? this.pathIcon,
            category: category ?? this.category,
            createdAt: createdAt ?? this.createdAt,
            updatedAt: updatedAt ?? this.updatedAt,
        );

    factory Category.fromJson(Map<String, dynamic> json) => Category(
        id: json["id"] == null ? null : json["id"],
        pathIcon: json["path_icon"],
        category: json["category"] == null ? null : json["category"],
        createdAt: json["created_at"] == null ? null : DateTime.parse(json["created_at"]),
        updatedAt: json["updated_at"] == null ? null : DateTime.parse(json["updated_at"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "path_icon": pathIcon,
        "category": category == null ? null : category,
        "created_at": createdAt == null ? null : createdAt.toIso8601String(),
        "updated_at": updatedAt == null ? null : updatedAt.toIso8601String(),
    };
}
