// To parse this JSON data, do
//
//     final paymentTutorialResponse = paymentTutorialResponseFromJson(jsonString);

import 'dart:convert';

PaymentTutorialResponse paymentTutorialResponseFromJson(String str) => PaymentTutorialResponse.fromJson(json.decode(str));

String paymentTutorialResponseToJson(PaymentTutorialResponse data) => json.encode(data.toJson());

class PaymentTutorialResponse {
    PaymentTutorialResponse({
        this.success,
        this.message,
        this.data,
    });

    bool success;
    String message;
    PaymentTutorial data;

    PaymentTutorialResponse copyWith({
        bool success,
        String message,
        PaymentTutorial data,
    }) => 
        PaymentTutorialResponse(
            success: success ?? this.success,
            message: message ?? this.message,
            data: data ?? this.data,
        );

    factory PaymentTutorialResponse.fromJson(Map<String, dynamic> json) => PaymentTutorialResponse(
        success: json["success"] == null ? null : json["success"],
        message: json["message"] == null ? null : json["message"],
        data: json["data"] == null ? null : PaymentTutorial.fromJson(json["data"]),
    );

    Map<String, dynamic> toJson() => {
        "success": success == null ? null : success,
        "message": message == null ? null : message,
        "data": data == null ? null : data.toJson(),
    };
}

class PaymentTutorial {
    PaymentTutorial({
        this.transaction,
        this.payment,
    });

    Transaction transaction;
    Payment payment;

    PaymentTutorial copyWith({
        Transaction transaction,
        Payment payment,
    }) => 
        PaymentTutorial(
            transaction: transaction ?? this.transaction,
            payment: payment ?? this.payment,
        );

    factory PaymentTutorial.fromJson(Map<String, dynamic> json) => PaymentTutorial(
        transaction: json["transaction"] == null ? null : Transaction.fromJson(json["transaction"]),
        payment: json["payment"] == null ? null : Payment.fromJson(json["payment"]),
    );

    Map<String, dynamic> toJson() => {
        "transaction": transaction == null ? null : transaction.toJson(),
        "payment": payment == null ? null : payment.toJson(),
    };
}

class Payment {
    Payment({
        this.reference,
        this.merchantRef,
        this.paymentSelectionType,
        this.paymentMethod,
        this.paymentName,
        this.customerName,
        this.customerEmail,
        this.customerPhone,
        this.callbackUrl,
        this.returnUrl,
        this.amount,
        this.fee,
        this.isCustomerFee,
        this.amountReceived,
        this.payCode,
        this.payUrl,
        this.checkoutUrl,
        this.status,
        this.paidAt,
        this.expiredTime,
        this.orderItems,
        this.instructions,
        this.qrString,
        this.qrUrl,
    });

    String reference;
    dynamic merchantRef;
    String paymentSelectionType;
    String paymentMethod;
    String paymentName;
    String customerName;
    String customerEmail;
    dynamic customerPhone;
    String callbackUrl;
    String returnUrl;
    int amount;
    int fee;
    int isCustomerFee;
    int amountReceived;
    dynamic payCode;
    dynamic payUrl;
    String checkoutUrl;
    String status;
    dynamic paidAt;
    int expiredTime;
    List<OrderItem> orderItems;
    List<Instruction> instructions;
    String qrString;
    String qrUrl;

    Payment copyWith({
        String reference,
        dynamic merchantRef,
        String paymentSelectionType,
        String paymentMethod,
        String paymentName,
        String customerName,
        String customerEmail,
        dynamic customerPhone,
        String callbackUrl,
        String returnUrl,
        int amount,
        int fee,
        int isCustomerFee,
        int amountReceived,
        dynamic payCode,
        dynamic payUrl,
        String checkoutUrl,
        String status,
        dynamic paidAt,
        int expiredTime,
        List<OrderItem> orderItems,
        List<Instruction> instructions,
        String qrString,
        String qrUrl,
    }) => 
        Payment(
            reference: reference ?? this.reference,
            merchantRef: merchantRef ?? this.merchantRef,
            paymentSelectionType: paymentSelectionType ?? this.paymentSelectionType,
            paymentMethod: paymentMethod ?? this.paymentMethod,
            paymentName: paymentName ?? this.paymentName,
            customerName: customerName ?? this.customerName,
            customerEmail: customerEmail ?? this.customerEmail,
            customerPhone: customerPhone ?? this.customerPhone,
            callbackUrl: callbackUrl ?? this.callbackUrl,
            returnUrl: returnUrl ?? this.returnUrl,
            amount: amount ?? this.amount,
            fee: fee ?? this.fee,
            isCustomerFee: isCustomerFee ?? this.isCustomerFee,
            amountReceived: amountReceived ?? this.amountReceived,
            payCode: payCode ?? this.payCode,
            payUrl: payUrl ?? this.payUrl,
            checkoutUrl: checkoutUrl ?? this.checkoutUrl,
            status: status ?? this.status,
            paidAt: paidAt ?? this.paidAt,
            expiredTime: expiredTime ?? this.expiredTime,
            orderItems: orderItems ?? this.orderItems,
            instructions: instructions ?? this.instructions,
            qrString: qrString ?? this.qrString,
            qrUrl: qrUrl ?? this.qrUrl,
        );

    factory Payment.fromJson(Map<String, dynamic> json) => Payment(
        reference: json["reference"] == null ? null : json["reference"],
        merchantRef: json["merchant_ref"],
        paymentSelectionType: json["payment_selection_type"] == null ? null : json["payment_selection_type"],
        paymentMethod: json["payment_method"] == null ? null : json["payment_method"],
        paymentName: json["payment_name"] == null ? null : json["payment_name"],
        customerName: json["customer_name"] == null ? null : json["customer_name"],
        customerEmail: json["customer_email"] == null ? null : json["customer_email"],
        customerPhone: json["customer_phone"],
        callbackUrl: json["callback_url"] == null ? null : json["callback_url"],
        returnUrl: json["return_url"] == null ? null : json["return_url"],
        amount: json["amount"] == null ? null : json["amount"],
        fee: json["fee"] == null ? null : json["fee"],
        isCustomerFee: json["is_customer_fee"] == null ? null : json["is_customer_fee"],
        amountReceived: json["amount_received"] == null ? null : json["amount_received"],
        payCode: json["pay_code"],
        payUrl: json["pay_url"],
        checkoutUrl: json["checkout_url"] == null ? null : json["checkout_url"],
        status: json["status"] == null ? null : json["status"],
        paidAt: json["paid_at"],
        expiredTime: json["expired_time"] == null ? null : json["expired_time"],
        orderItems: json["order_items"] == null ? null : List<OrderItem>.from(json["order_items"].map((x) => OrderItem.fromJson(x))),
        instructions: json["instructions"] == null ? null : List<Instruction>.from(json["instructions"].map((x) => Instruction.fromJson(x))),
        qrString: json["qr_string"] == null ? null : json["qr_string"],
        qrUrl: json["qr_url"] == null ? null : json["qr_url"],
    );

    Map<String, dynamic> toJson() => {
        "reference": reference == null ? null : reference,
        "merchant_ref": merchantRef,
        "payment_selection_type": paymentSelectionType == null ? null : paymentSelectionType,
        "payment_method": paymentMethod == null ? null : paymentMethod,
        "payment_name": paymentName == null ? null : paymentName,
        "customer_name": customerName == null ? null : customerName,
        "customer_email": customerEmail == null ? null : customerEmail,
        "customer_phone": customerPhone,
        "callback_url": callbackUrl == null ? null : callbackUrl,
        "return_url": returnUrl == null ? null : returnUrl,
        "amount": amount == null ? null : amount,
        "fee": fee == null ? null : fee,
        "is_customer_fee": isCustomerFee == null ? null : isCustomerFee,
        "amount_received": amountReceived == null ? null : amountReceived,
        "pay_code": payCode,
        "pay_url": payUrl,
        "checkout_url": checkoutUrl == null ? null : checkoutUrl,
        "status": status == null ? null : status,
        "paid_at": paidAt,
        "expired_time": expiredTime == null ? null : expiredTime,
        "order_items": orderItems == null ? null : List<dynamic>.from(orderItems.map((x) => x.toJson())),
        "instructions": instructions == null ? null : List<dynamic>.from(instructions.map((x) => x.toJson())),
        "qr_string": qrString == null ? null : qrString,
        "qr_url": qrUrl == null ? null : qrUrl,
    };
}

class Instruction {
    Instruction({
        this.title,
        this.steps,
    });

    String title;
    List<String> steps;

    Instruction copyWith({
        String title,
        List<String> steps,
    }) => 
        Instruction(
            title: title ?? this.title,
            steps: steps ?? this.steps,
        );

    factory Instruction.fromJson(Map<String, dynamic> json) => Instruction(
        title: json["title"] == null ? null : json["title"],
        steps: json["steps"] == null ? null : List<String>.from(json["steps"].map((x) => x)),
    );

    Map<String, dynamic> toJson() => {
        "title": title == null ? null : title,
        "steps": steps == null ? null : List<dynamic>.from(steps.map((x) => x)),
    };
}

class OrderItem {
    OrderItem({
        this.sku,
        this.name,
        this.price,
        this.quantity,
        this.subtotal,
    });

    String sku;
    String name;
    int price;
    int quantity;
    int subtotal;

    OrderItem copyWith({
        String sku,
        String name,
        int price,
        int quantity,
        int subtotal,
    }) => 
        OrderItem(
            sku: sku ?? this.sku,
            name: name ?? this.name,
            price: price ?? this.price,
            quantity: quantity ?? this.quantity,
            subtotal: subtotal ?? this.subtotal,
        );

    factory OrderItem.fromJson(Map<String, dynamic> json) => OrderItem(
        sku: json["sku"] == null ? null : json["sku"],
        name: json["name"] == null ? null : json["name"],
        price: json["price"] == null ? null : json["price"],
        quantity: json["quantity"] == null ? null : json["quantity"],
        subtotal: json["subtotal"] == null ? null : json["subtotal"],
    );

    Map<String, dynamic> toJson() => {
        "sku": sku == null ? null : sku,
        "name": name == null ? null : name,
        "price": price == null ? null : price,
        "quantity": quantity == null ? null : quantity,
        "subtotal": subtotal == null ? null : subtotal,
    };
}

class Transaction {
    Transaction({
        this.id,
        this.projectId,
        this.userId,
        this.nominal,
        this.paymentMethod,
        this.status,
        this.timeLimit,
        this.createdAt,
        this.updatedAt,
        this.pathProof,
        this.rejectReason,
        this.donatureName,
        this.donatureEmail,
        this.specialMessage,
        this.uniqueCode,
        this.isAnonymous,
        this.total,
        this.additionalFee,
        this.paymentType,
        this.donaturePhone,
        this.reference,
        this.isAdmin,
        this.paymentCode,
        this.fundType,
        this.wishMessage,
        this.nominalFormatted,
        this.icon,
    });

    int id;
    String projectId;
    String userId;
    int nominal;
    String paymentMethod;
    String status;
    String timeLimit;
    DateTime createdAt;
    DateTime updatedAt;
    dynamic pathProof;
    dynamic rejectReason;
    String donatureName;
    String donatureEmail;
    dynamic specialMessage;
    String uniqueCode;
    String isAnonymous;
    String total;
    String additionalFee;
    String paymentType;
    String donaturePhone;
    String reference;
    String isAdmin;
    String paymentCode;
    String fundType;
    dynamic wishMessage;
    String nominalFormatted;
    String icon;

    Transaction copyWith({
        int id,
        String projectId,
        String userId,
        int nominal,
        String paymentMethod,
        String status,
        String timeLimit,
        DateTime createdAt,
        DateTime updatedAt,
        dynamic pathProof,
        dynamic rejectReason,
        String donatureName,
        String donatureEmail,
        dynamic specialMessage,
        String uniqueCode,
        String isAnonymous,
        String total,
        String additionalFee,
        String paymentType,
        String donaturePhone,
        String reference,
        String isAdmin,
        String paymentCode,
        String fundType,
        dynamic wishMessage,
        String nominalFormatted,
        String icon,
    }) => 
        Transaction(
            id: id ?? this.id,
            projectId: projectId ?? this.projectId,
            userId: userId ?? this.userId,
            nominal: nominal ?? this.nominal,
            paymentMethod: paymentMethod ?? this.paymentMethod,
            status: status ?? this.status,
            timeLimit: timeLimit ?? this.timeLimit,
            createdAt: createdAt ?? this.createdAt,
            updatedAt: updatedAt ?? this.updatedAt,
            pathProof: pathProof ?? this.pathProof,
            rejectReason: rejectReason ?? this.rejectReason,
            donatureName: donatureName ?? this.donatureName,
            donatureEmail: donatureEmail ?? this.donatureEmail,
            specialMessage: specialMessage ?? this.specialMessage,
            uniqueCode: uniqueCode ?? this.uniqueCode,
            isAnonymous: isAnonymous ?? this.isAnonymous,
            total: total ?? this.total,
            additionalFee: additionalFee ?? this.additionalFee,
            paymentType: paymentType ?? this.paymentType,
            donaturePhone: donaturePhone ?? this.donaturePhone,
            reference: reference ?? this.reference,
            isAdmin: isAdmin ?? this.isAdmin,
            paymentCode: paymentCode ?? this.paymentCode,
            fundType: fundType ?? this.fundType,
            wishMessage: wishMessage ?? this.wishMessage,
            nominalFormatted: nominalFormatted ?? this.nominalFormatted,
            icon: icon ?? this.icon,
        );

    factory Transaction.fromJson(Map<String, dynamic> json) => Transaction(
        id: json["id"] == null ? null : json["id"],
        projectId: json["project_id"] == null ? null : json["project_id"],
        userId: json["user_id"] == null ? null : json["user_id"],
        nominal: json["nominal"] == null ? null : json["nominal"],
        paymentMethod: json["payment_method"] == null ? null : json["payment_method"],
        status: json["status"] == null ? null : json["status"],
        timeLimit: json["time_limit"] == null ? null : json["time_limit"],
        createdAt: json["created_at"] == null ? null : DateTime.parse(json["created_at"]),
        updatedAt: json["updated_at"] == null ? null : DateTime.parse(json["updated_at"]),
        pathProof: json["path_proof"],
        rejectReason: json["reject_reason"],
        donatureName: json["donature_name"] == null ? null : json["donature_name"],
        donatureEmail: json["donature_email"] == null ? null : json["donature_email"],
        specialMessage: json["special_message"],
        uniqueCode: json["unique_code"] == null ? null : json["unique_code"],
        isAnonymous: json["is_anonymous"] == null ? null : json["is_anonymous"],
        total: json["total"] == null ? null : json["total"],
        additionalFee: json["additional_fee"] == null ? null : json["additional_fee"],
        paymentType: json["payment_type"] == null ? null : json["payment_type"],
        donaturePhone: json["donature_phone"] == null ? null : json["donature_phone"],
        reference: json["reference"] == null ? null : json["reference"],
        isAdmin: json["is_admin"] == null ? null : json["is_admin"],
        paymentCode: json["payment_code"] == null ? null : json["payment_code"],
        fundType: json["fund_type"] == null ? null : json["fund_type"],
        wishMessage: json["wish_message"],
        nominalFormatted: json["nominal_formatted"] == null ? null : json["nominal_formatted"],
        icon: json["icon"] == null ? null : json["icon"],
    );

    Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "project_id": projectId == null ? null : projectId,
        "user_id": userId == null ? null : userId,
        "nominal": nominal == null ? null : nominal,
        "payment_method": paymentMethod == null ? null : paymentMethod,
        "status": status == null ? null : status,
        "time_limit": timeLimit == null ? null : timeLimit,
        "created_at": createdAt == null ? null : createdAt.toIso8601String(),
        "updated_at": updatedAt == null ? null : updatedAt.toIso8601String(),
        "path_proof": pathProof,
        "reject_reason": rejectReason,
        "donature_name": donatureName == null ? null : donatureName,
        "donature_email": donatureEmail == null ? null : donatureEmail,
        "special_message": specialMessage,
        "unique_code": uniqueCode == null ? null : uniqueCode,
        "is_anonymous": isAnonymous == null ? null : isAnonymous,
        "total": total == null ? null : total,
        "additional_fee": additionalFee == null ? null : additionalFee,
        "payment_type": paymentType == null ? null : paymentType,
        "donature_phone": donaturePhone == null ? null : donaturePhone,
        "reference": reference == null ? null : reference,
        "is_admin": isAdmin == null ? null : isAdmin,
        "payment_code": paymentCode == null ? null : paymentCode,
        "fund_type": fundType == null ? null : fundType,
        "wish_message": wishMessage,
        "nominal_formatted": nominalFormatted == null ? null : nominalFormatted,
        "icon": icon == null ? null : icon,
    };
}
