// To parse this JSON data, do
//
//     final donationCategoryResponse = donationCategoryResponseFromJson(jsonString);

import 'dart:convert';

DonationCategoryResponse donationCategoryResponseFromJson(String str) => DonationCategoryResponse.fromJson(json.decode(str));

String donationCategoryResponseToJson(DonationCategoryResponse data) => json.encode(data.toJson());

class DonationCategoryResponse {
    DonationCategoryResponse({
        this.success,
        this.message,
        this.data,
    });

    bool success;
    String message;
    List<DonationCategory> data;

    DonationCategoryResponse copyWith({
        bool success,
        String message,
        List<DonationCategory> data,
    }) => 
        DonationCategoryResponse(
            success: success ?? this.success,
            message: message ?? this.message,
            data: data ?? this.data,
        );

    factory DonationCategoryResponse.fromJson(Map<String, dynamic> json) => DonationCategoryResponse(
        success: json["success"] == null ? null : json["success"],
        message: json["message"] == null ? null : json["message"],
        data: json["data"] == null ? null : List<DonationCategory>.from(json["data"].map((x) => DonationCategory.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "success": success == null ? null : success,
        "message": message == null ? null : message,
        "data": data == null ? null : List<dynamic>.from(data.map((x) => x.toJson())),
    };
}

class DonationCategory {
    DonationCategory({
        this.id,
        this.pathIcon,
        this.category,
        this.createdAt,
        this.updatedAt,
    });

    int id;
    String pathIcon;
    String category;
    DateTime createdAt;
    DateTime updatedAt;

    DonationCategory copyWith({
        int id,
        String pathIcon,
        String category,
        DateTime createdAt,
        DateTime updatedAt,
    }) => 
        DonationCategory(
            id: id ?? this.id,
            pathIcon: pathIcon ?? this.pathIcon,
            category: category ?? this.category,
            createdAt: createdAt ?? this.createdAt,
            updatedAt: updatedAt ?? this.updatedAt,
        );

    factory DonationCategory.fromJson(Map<String, dynamic> json) => DonationCategory(
        id: json["id"] == null ? null : json["id"],
        pathIcon: json["path_icon"] == null ? null : json["path_icon"],
        category: json["category"] == null ? null : json["category"],
        createdAt: json["created_at"] == null ? null : DateTime.parse(json["created_at"]),
        updatedAt: json["updated_at"] == null ? null : DateTime.parse(json["updated_at"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "path_icon": pathIcon == null ? null : pathIcon,
        "category": category == null ? null : category,
        "created_at": createdAt == null ? null : createdAt.toIso8601String(),
        "updated_at": updatedAt == null ? null : updatedAt.toIso8601String(),
    };
}
