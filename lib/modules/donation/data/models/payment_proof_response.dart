// To parse this JSON data, do
//
//     final paymentProofResponse = paymentProofResponseFromJson(jsonString);

import 'dart:convert';

PaymentProofResponse paymentProofResponseFromJson(String str) => PaymentProofResponse.fromJson(json.decode(str));

String paymentProofResponseToJson(PaymentProofResponse data) => json.encode(data.toJson());

class PaymentProofResponse {
    PaymentProofResponse({
        this.success,
        this.message,
    });

    bool success;
    String message;

    PaymentProofResponse copyWith({
        bool success,
        String message,
    }) => 
        PaymentProofResponse(
            success: success ?? this.success,
            message: message ?? this.message,
        );

    factory PaymentProofResponse.fromJson(Map<String, dynamic> json) => PaymentProofResponse(
        success: json["success"] == null ? null : json["success"],
        message: json["message"] == null ? null : json["message"],
    );

    Map<String, dynamic> toJson() => {
        "success": success == null ? null : success,
        "message": message == null ? null : message,
    };
}
