// To parse this JSON data, do
//
//     final paymentResponse = paymentResponseFromJson(jsonString);

import 'dart:convert';

PaymentResponse paymentResponseFromJson(String str) => PaymentResponse.fromJson(json.decode(str));

String paymentResponseToJson(PaymentResponse data) => json.encode(data.toJson());

class PaymentResponse {
    PaymentResponse({
        this.success,
        this.message,
        this.data,
    });

    bool success;
    String message;
    PaymentData data;

    PaymentResponse copyWith({
        bool success,
        String message,
        PaymentData data,
    }) => 
        PaymentResponse(
            success: success ?? this.success,
            message: message ?? this.message,
            data: data ?? this.data,
        );

    factory PaymentResponse.fromJson(Map<String, dynamic> json) => PaymentResponse(
        success: json["success"] == null ? null : json["success"],
        message: json["message"] == null ? null : json["message"],
        data: json["data"] == null ? null : PaymentData.fromJson(json["data"]),
    );

    Map<String, dynamic> toJson() => {
        "success": success == null ? null : success,
        "message": message == null ? null : message,
        "data": data == null ? null : data.toJson(),
    };
}

class PaymentData {
    PaymentData({
        this.projectId,
        this.userId,
        this.nominal,
        this.paymentMethod,
        this.donatureName,
        this.donatureEmail,
        this.donaturePhone,
        this.specialMessage,
        this.isAnonymous,
        this.uniqueCode,
        this.additionalFee,
        this.status,
        this.paymentType,
        this.paymentCode,
        this.total,
        this.timeLimit,
        this.reference,
        this.updatedAt,
        this.createdAt,
        this.id,
    });

    String projectId;
    int userId;
    String nominal;
    String paymentMethod;
    String donatureName;
    String donatureEmail;
    String donaturePhone;
    String specialMessage;
    String isAnonymous;
    int uniqueCode;
    int additionalFee;
    String status;
    String paymentType;
    String paymentCode;
    int total;
    DateTime timeLimit;
    String reference;
    DateTime updatedAt;
    DateTime createdAt;
    int id;

    PaymentData copyWith({
        String projectId,
        int userId,
        String nominal,
        String paymentMethod,
        String donatureName,
        String donatureEmail,
        String donaturePhone,
        String specialMessage,
        String isAnonymous,
        int uniqueCode,
        int additionalFee,
        String status,
        String paymentType,
        String paymentCode,
        int total,
        DateTime timeLimit,
        String reference,
        DateTime updatedAt,
        DateTime createdAt,
        int id,
    }) => 
        PaymentData(
            projectId: projectId ?? this.projectId,
            userId: userId ?? this.userId,
            nominal: nominal ?? this.nominal,
            paymentMethod: paymentMethod ?? this.paymentMethod,
            donatureName: donatureName ?? this.donatureName,
            donatureEmail: donatureEmail ?? this.donatureEmail,
            donaturePhone: donaturePhone ?? this.donaturePhone,
            specialMessage: specialMessage ?? this.specialMessage,
            isAnonymous: isAnonymous ?? this.isAnonymous,
            uniqueCode: uniqueCode ?? this.uniqueCode,
            additionalFee: additionalFee ?? this.additionalFee,
            status: status ?? this.status,
            paymentType: paymentType ?? this.paymentType,
            paymentCode: paymentCode ?? this.paymentCode,
            total: total ?? this.total,
            timeLimit: timeLimit ?? this.timeLimit,
            reference: reference ?? this.reference,
            updatedAt: updatedAt ?? this.updatedAt,
            createdAt: createdAt ?? this.createdAt,
            id: id ?? this.id,
        );

    factory PaymentData.fromJson(Map<String, dynamic> json) => PaymentData(
        projectId: json["project_id"] == null ? null : json["project_id"],
        userId: json["user_id"] == null ? null : json["user_id"],
        nominal: json["nominal"] == null ? null : json["nominal"],
        paymentMethod: json["payment_method"] == null ? null : json["payment_method"],
        donatureName: json["donature_name"] == null ? null : json["donature_name"],
        donatureEmail: json["donature_email"] == null ? null : json["donature_email"],
        donaturePhone: json["donature_phone"] == null ? null : json["donature_phone"],
        specialMessage: json["special_message"] == null ? null : json["special_message"],
        isAnonymous: json["is_anonymous"] == null ? null : json["is_anonymous"],
        uniqueCode: json["unique_code"] == null ? null : json["unique_code"],
        additionalFee: json["additional_fee"] == null ? null : json["additional_fee"],
        status: json["status"] == null ? null : json["status"],
        paymentType: json["payment_type"] == null ? null : json["payment_type"],
        paymentCode: json["payment_code"] == null ? null : json["payment_code"],
        total: json["total"] == null ? null : json["total"],
        timeLimit: json["time_limit"] == null ? null : DateTime.parse(json["time_limit"]),
        reference: json["reference"] == null ? null : json["reference"],
        updatedAt: json["updated_at"] == null ? null : DateTime.parse(json["updated_at"]),
        createdAt: json["created_at"] == null ? null : DateTime.parse(json["created_at"]),
        id: json["id"] == null ? null : json["id"],
    );

    Map<String, dynamic> toJson() => {
        "project_id": projectId == null ? null : projectId,
        "user_id": userId == null ? null : userId,
        "nominal": nominal == null ? null : nominal,
        "payment_method": paymentMethod == null ? null : paymentMethod,
        "donature_name": donatureName == null ? null : donatureName,
        "donature_email": donatureEmail == null ? null : donatureEmail,
        "donature_phone": donaturePhone == null ? null : donaturePhone,
        "special_message": specialMessage == null ? null : specialMessage,
        "is_anonymous": isAnonymous == null ? null : isAnonymous,
        "unique_code": uniqueCode == null ? null : uniqueCode,
        "additional_fee": additionalFee == null ? null : additionalFee,
        "status": status == null ? null : status,
        "payment_type": paymentType == null ? null : paymentType,
        "payment_code": paymentCode == null ? null : paymentCode,
        "total": total == null ? null : total,
        "time_limit": timeLimit == null ? null : timeLimit.toIso8601String(),
        "reference": reference == null ? null : reference,
        "updated_at": updatedAt == null ? null : updatedAt.toIso8601String(),
        "created_at": createdAt == null ? null : createdAt.toIso8601String(),
        "id": id == null ? null : id,
    };
}
