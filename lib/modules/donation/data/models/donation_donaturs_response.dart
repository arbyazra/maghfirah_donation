// To parse this JSON data, do
//
//     final donationDonatursResponse = donationDonatursResponseFromJson(jsonString);

import 'dart:convert';

DonationDonatursResponse donationDonatursResponseFromJson(String str) => DonationDonatursResponse.fromJson(json.decode(str));

String donationDonatursResponseToJson(DonationDonatursResponse data) => json.encode(data.toJson());

class DonationDonatursResponse {
    DonationDonatursResponse({
        this.success,
        this.message,
        this.data,
    });

    bool success;
    String message;
    List<Donatur> data;

    DonationDonatursResponse copyWith({
        bool success,
        String message,
        List<Donatur> data,
    }) => 
        DonationDonatursResponse(
            success: success ?? this.success,
            message: message ?? this.message,
            data: data ?? this.data,
        );

    factory DonationDonatursResponse.fromJson(Map<String, dynamic> json) => DonationDonatursResponse(
        success: json["success"] == null ? null : json["success"],
        message: json["message"] == null ? null : json["message"],
        data: json["data"] == null ? null : List<Donatur>.from(json["data"].map((x) => Donatur.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "success": success == null ? null : success,
        "message": message == null ? null : message,
        "data": data == null ? null : List<dynamic>.from(data.map((x) => x.toJson())),
    };
}

class Donatur {
    Donatur({
        this.id,
        this.projectId,
        this.userId,
        this.nominal,
        this.paymentMethod,
        this.status,
        this.timeLimit,
        this.createdAt,
        this.updatedAt,
        this.pathProof,
        this.rejectReason,
        this.donatureName,
        this.donatureEmail,
        this.specialMessage,
        this.uniqueCode,
        this.isAnonymous,
        this.total,
        this.additionalFee,
        this.paymentType,
        this.donaturePhone,
        this.reference,
        this.isAdmin,
        this.paymentCode,
        this.fundType,
        this.wishMessage,
        this.pathFoto,
    });

    int id;
    String projectId;
    String userId;
    String nominal;
    String paymentMethod;
    String status;
    DateTime timeLimit;
    DateTime createdAt;
    DateTime updatedAt;
    dynamic pathProof;
    dynamic rejectReason;
    String donatureName;
    String donatureEmail;
    dynamic specialMessage;
    String uniqueCode;
    String isAnonymous;
    String total;
    String additionalFee;
    String paymentType;
    String donaturePhone;
    dynamic reference;
    String isAdmin;
    String paymentCode;
    String fundType;
    dynamic wishMessage;
    dynamic pathFoto;

    Donatur copyWith({
        int id,
        String projectId,
        String userId,
        String nominal,
        String paymentMethod,
        String status,
        DateTime timeLimit,
        DateTime createdAt,
        DateTime updatedAt,
        dynamic pathProof,
        dynamic rejectReason,
        String donatureName,
        String donatureEmail,
        dynamic specialMessage,
        String uniqueCode,
        String isAnonymous,
        String total,
        String additionalFee,
        String paymentType,
        String donaturePhone,
        dynamic reference,
        String isAdmin,
        String paymentCode,
        String fundType,
        dynamic wishMessage,
        dynamic pathFoto,
    }) => 
        Donatur(
            id: id ?? this.id,
            projectId: projectId ?? this.projectId,
            userId: userId ?? this.userId,
            nominal: nominal ?? this.nominal,
            paymentMethod: paymentMethod ?? this.paymentMethod,
            status: status ?? this.status,
            timeLimit: timeLimit ?? this.timeLimit,
            createdAt: createdAt ?? this.createdAt,
            updatedAt: updatedAt ?? this.updatedAt,
            pathProof: pathProof ?? this.pathProof,
            rejectReason: rejectReason ?? this.rejectReason,
            donatureName: donatureName ?? this.donatureName,
            donatureEmail: donatureEmail ?? this.donatureEmail,
            specialMessage: specialMessage ?? this.specialMessage,
            uniqueCode: uniqueCode ?? this.uniqueCode,
            isAnonymous: isAnonymous ?? this.isAnonymous,
            total: total ?? this.total,
            additionalFee: additionalFee ?? this.additionalFee,
            paymentType: paymentType ?? this.paymentType,
            donaturePhone: donaturePhone ?? this.donaturePhone,
            reference: reference ?? this.reference,
            isAdmin: isAdmin ?? this.isAdmin,
            paymentCode: paymentCode ?? this.paymentCode,
            fundType: fundType ?? this.fundType,
            wishMessage: wishMessage ?? this.wishMessage,
            pathFoto: pathFoto ?? this.pathFoto,
        );

    factory Donatur.fromJson(Map<String, dynamic> json) => Donatur(
        id: json["id"] == null ? null : json["id"],
        projectId: json["project_id"] == null ? null : json["project_id"],
        userId: json["user_id"] == null ? null : json["user_id"],
        nominal: json["nominal"] == null ? null : json["nominal"],
        paymentMethod: json["payment_method"] == null ? null : json["payment_method"],
        status: json["status"] == null ? null : json["status"],
        timeLimit: json["time_limit"] == null ? null : DateTime.parse(json["time_limit"]),
        createdAt: json["created_at"] == null ? null : DateTime.parse(json["created_at"]),
        updatedAt: json["updated_at"] == null ? null : DateTime.parse(json["updated_at"]),
        pathProof: json["path_proof"],
        rejectReason: json["reject_reason"],
        donatureName: json["donature_name"] == null ? null : json["donature_name"],
        donatureEmail: json["donature_email"] == null ? null : json["donature_email"],
        specialMessage: json["special_message"],
        uniqueCode: json["unique_code"] == null ? null : json["unique_code"],
        isAnonymous: json["is_anonymous"] == null ? null : json["is_anonymous"],
        total: json["total"] == null ? null : json["total"],
        additionalFee: json["additional_fee"] == null ? null : json["additional_fee"],
        paymentType: json["payment_type"] == null ? null : json["payment_type"],
        donaturePhone: json["donature_phone"] == null ? null : json["donature_phone"],
        reference: json["reference"],
        isAdmin: json["is_admin"] == null ? null : json["is_admin"],
        paymentCode: json["payment_code"] == null ? null : json["payment_code"],
        fundType: json["fund_type"] == null ? null : json["fund_type"],
        wishMessage: json["wish_message"],
        pathFoto: json["path_foto"],
    );

    Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "project_id": projectId == null ? null : projectId,
        "user_id": userId == null ? null : userId,
        "nominal": nominal == null ? null : nominal,
        "payment_method": paymentMethod == null ? null : paymentMethod,
        "status": status == null ? null : status,
        "time_limit": timeLimit == null ? null : timeLimit.toIso8601String(),
        "created_at": createdAt == null ? null : createdAt.toIso8601String(),
        "updated_at": updatedAt == null ? null : updatedAt.toIso8601String(),
        "path_proof": pathProof,
        "reject_reason": rejectReason,
        "donature_name": donatureName == null ? null : donatureName,
        "donature_email": donatureEmail == null ? null : donatureEmail,
        "special_message": specialMessage,
        "unique_code": uniqueCode == null ? null : uniqueCode,
        "is_anonymous": isAnonymous == null ? null : isAnonymous,
        "total": total == null ? null : total,
        "additional_fee": additionalFee == null ? null : additionalFee,
        "payment_type": paymentType == null ? null : paymentType,
        "donature_phone": donaturePhone == null ? null : donaturePhone,
        "reference": reference,
        "is_admin": isAdmin == null ? null : isAdmin,
        "payment_code": paymentCode == null ? null : paymentCode,
        "fund_type": fundType == null ? null : fundType,
        "wish_message": wishMessage,
        "path_foto": pathFoto,
    };
}
