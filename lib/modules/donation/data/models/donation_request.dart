
class DonationRequest {
  String id;
  String paymentCode;
  int nominal;
  String donatureName;
  String donatureEmail;
  String donaturePhone;
  int isAnonymous;
  String specialMessage;
  String wishMessage;

  DonationRequest(
      {this.paymentCode,
      this.id,
      this.nominal,
      this.donatureName,
      this.donatureEmail,
      this.donaturePhone="",
      this.isAnonymous=0,
      this.specialMessage="",
      this.wishMessage=""});

  copyWith({
    id,
    paymentCode,
    nominal,
    donatureName,
    donatureEmail,
    donaturePhone,
    isAnonymous,
    specialMessage,
    wishMessage,
  }) =>
      DonationRequest(
        id: id ?? this.id,
        paymentCode: paymentCode ?? this.paymentCode,
        nominal: nominal ?? this.nominal,
        donatureName: donatureName ?? this.donatureName,
        donatureEmail: donatureEmail ?? this.donatureEmail,
        donaturePhone: donaturePhone ?? this.donaturePhone,
        isAnonymous: isAnonymous ?? this.isAnonymous,
        specialMessage: specialMessage ?? this.specialMessage,
        wishMessage: wishMessage ?? this.wishMessage,
      );

  toJson() => {
        "id": "$id",
        "payment_code": "$paymentCode",
        "nominal": "$nominal",
        "donature_name": donatureName,
        "donature_email": donatureEmail,
        "donature_phone": donaturePhone??'',
        "is_anonymous": "$isAnonymous",
        "special_message": specialMessage??'',
        "wish_message": wishMessage??'',
      };
}
