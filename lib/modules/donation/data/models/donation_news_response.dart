// To parse this JSON data, do
//
//     final donationNewsResponse = donationNewsResponseFromJson(jsonString);

import 'dart:convert';

DonationNewsResponse donationNewsResponseFromJson(String str) => DonationNewsResponse.fromJson(json.decode(str));

String donationNewsResponseToJson(DonationNewsResponse data) => json.encode(data.toJson());

class DonationNewsResponse {
    DonationNewsResponse({
        this.success,
        this.message,
        this.data,
    });

    bool success;
    String message;
    List<News> data;

    DonationNewsResponse copyWith({
        bool success,
        String message,
        List<News> data,
    }) => 
        DonationNewsResponse(
            success: success ?? this.success,
            message: message ?? this.message,
            data: data ?? this.data,
        );

    factory DonationNewsResponse.fromJson(Map<String, dynamic> json) => DonationNewsResponse(
        success: json["success"] == null ? null : json["success"],
        message: json["message"] == null ? null : json["message"],
        data: json["data"] == null ? null : List<News>.from(json["data"].map((x) => News.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "success": success == null ? null : success,
        "message": message == null ? null : message,
        "data": data == null ? null : List<dynamic>.from(data.map((x) => x.toJson())),
    };
}

class News {
    News({
        this.id,
        this.projectId,
        this.nominal,
        this.content,
        this.updateType,
        this.createdAt,
        this.updatedAt,
        this.userId,
        this.title,
        this.date,
    });

    int id;
    int projectId;
    int nominal;
    String content;
    int updateType;
    DateTime createdAt;
    DateTime updatedAt;
    int userId;
    String title;
    String date;

    News copyWith({
        int id,
        int projectId,
        int nominal,
        String content,
        int updateType,
        DateTime createdAt,
        DateTime updatedAt,
        int userId,
        String title,
        String date,
    }) => 
        News(
            id: id ?? this.id,
            projectId: projectId ?? this.projectId,
            nominal: nominal ?? this.nominal,
            content: content ?? this.content,
            updateType: updateType ?? this.updateType,
            createdAt: createdAt ?? this.createdAt,
            updatedAt: updatedAt ?? this.updatedAt,
            userId: userId ?? this.userId,
            title: title ?? this.title,
            date: date ?? this.date,
        );

    factory News.fromJson(Map<String, dynamic> json) => News(
        id: json["id"] == null ? null : json["id"],
        projectId: json["project_id"] == null ? null : json["project_id"],
        nominal: json["nominal"] == null ? null : json["nominal"],
        content: json["content"] == null ? null : json["content"],
        updateType: json["update_type"] == null ? null : json["update_type"],
        createdAt: json["created_at"] == null ? null : DateTime.parse(json["created_at"]),
        updatedAt: json["updated_at"] == null ? null : DateTime.parse(json["updated_at"]),
        userId: json["user_id"] == null ? null : json["user_id"],
        title: json["title"] == null ? null : json["title"],
        date: json["date"] == null ? null : json["date"],
    );

    Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "project_id": projectId == null ? null : projectId,
        "nominal": nominal == null ? null : nominal,
        "content": content == null ? null : content,
        "update_type": updateType == null ? null : updateType,
        "created_at": createdAt == null ? null : createdAt.toIso8601String(),
        "updated_at": updatedAt == null ? null : updatedAt.toIso8601String(),
        "user_id": userId == null ? null : userId,
        "title": title == null ? null : title,
        "date": date == null ? null : date,
    };
}
