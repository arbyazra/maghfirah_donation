// To parse this JSON data, do
//
//     final paymentMethodResponse = paymentMethodResponseFromJson(jsonString);

import 'dart:convert';

PaymentMethodResponse paymentMethodResponseFromJson(String str) => PaymentMethodResponse.fromJson(json.decode(str));

String paymentMethodResponseToJson(PaymentMethodResponse data) => json.encode(data.toJson());

class PaymentMethodResponse {
    PaymentMethodResponse({
        this.success,
        this.message,
        this.data,
    });

    bool success;
    String message;
    List<PaymentMethod> data;

    PaymentMethodResponse copyWith({
        bool success,
        String message,
        List<PaymentMethod> data,
    }) => 
        PaymentMethodResponse(
            success: success ?? this.success,
            message: message ?? this.message,
            data: data ?? this.data,
        );

    factory PaymentMethodResponse.fromJson(Map<String, dynamic> json) => PaymentMethodResponse(
        success: json["success"] == null ? null : json["success"],
        message: json["message"] == null ? null : json["message"],
        data: json["data"] == null ? null : List<PaymentMethod>.from(json["data"].map((x) => PaymentMethod.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "success": success == null ? null : success,
        "message": message == null ? null : message,
        "data": data == null ? null : List<dynamic>.from(data.map((x) => x.toJson())),
    };
}

class PaymentMethod {
    PaymentMethod({
        this.type,
        this.fulltype,
        this.items,
    });

    String type;
    String fulltype;
    List<Item> items;

    PaymentMethod copyWith({
        String type,
        String fulltype,
        List<Item> items,
    }) => 
        PaymentMethod(
            type: type ?? this.type,
            fulltype: fulltype ?? this.fulltype,
            items: items ?? this.items,
        );

    factory PaymentMethod.fromJson(Map<String, dynamic> json) => PaymentMethod(
        type: json["type"] == null ? null : json["type"],
        fulltype: json["fulltype"] == null ? null : json["fulltype"],
        items: json["items"] == null ? null : List<Item>.from(json["items"].map((x) => Item.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "type": type == null ? null : type,
        "fulltype": fulltype == null ? null : fulltype,
        "items": items == null ? null : List<dynamic>.from(items.map((x) => x.toJson())),
    };
}

class Item {
    Item({
        this.group,
        this.code,
        this.name,
        this.type,
        this.chargedTo,
        this.fee,
        this.active,
        this.icon,
        this.serviceName,
        this.requestCode,
        this.id,
        this.bankName,
        this.bankUsername,
        this.bankNumber,
        this.bankCode,
        this.pathIcon,
        this.status,
        this.createdAt,
        this.updatedAt,
    });

    Group group;
    String code;
    String name;
    Type type;
    ChargedTo chargedTo;
    dynamic fee;
    bool active;
    String icon;
    String serviceName;
    String requestCode;
    int id;
    String bankName;
    String bankUsername;
    String bankNumber;
    String bankCode;
    String pathIcon;
    String status;
    DateTime createdAt;
    DateTime updatedAt;

    Item copyWith({
        Group group,
        String code,
        String name,
        Type type,
        ChargedTo chargedTo,
        dynamic fee,
        bool active,
        String icon,
        String serviceName,
        String requestCode,
        int id,
        String bankName,
        String bankUsername,
        String bankNumber,
        String bankCode,
        String pathIcon,
        String status,
        DateTime createdAt,
        DateTime updatedAt,
    }) => 
        Item(
            group: group ?? this.group,
            code: code ?? this.code,
            name: name ?? this.name,
            type: type ?? this.type,
            chargedTo: chargedTo ?? this.chargedTo,
            fee: fee ?? this.fee,
            active: active ?? this.active,
            icon: icon ?? this.icon,
            serviceName: serviceName ?? this.serviceName,
            requestCode: requestCode ?? this.requestCode,
            id: id ?? this.id,
            bankName: bankName ?? this.bankName,
            bankUsername: bankUsername ?? this.bankUsername,
            bankNumber: bankNumber ?? this.bankNumber,
            bankCode: bankCode ?? this.bankCode,
            pathIcon: pathIcon ?? this.pathIcon,
            status: status ?? this.status,
            createdAt: createdAt ?? this.createdAt,
            updatedAt: updatedAt ?? this.updatedAt,
        );

    factory Item.fromJson(Map<String, dynamic> json) => Item(
        group: json["group"] == null ? null : groupValues.map[json["group"]],
        code: json["code"] == null ? null : json["code"],
        name: json["name"] == null ? null : json["name"],
        type: json["type"] == null ? null : typeValues.map[json["type"]],
        chargedTo: json["charged_to"] == null ? null : chargedToValues.map[json["charged_to"]],
        fee: json["fee"],
        active: json["active"] == null ? null : json["active"],
        icon: json["icon"] == null ? null : json["icon"],
        serviceName: json["service_name"] == null ? null : json["service_name"],
        requestCode: json["request_code"] == null ? null : json["request_code"],
        id: json["id"] == null ? null : json["id"],
        bankName: json["bank_name"] == null ? null : json["bank_name"],
        bankUsername: json["bank_username"] == null ? null : json["bank_username"],
        bankNumber: json["bank_number"] == null ? null : json["bank_number"],
        bankCode: json["bank_code"] == null ? null : json["bank_code"],
        pathIcon: json["path_icon"] == null ? null : json["path_icon"],
        status: json["status"] == null ? null : json["status"],
        createdAt: json["created_at"] == null ? null : DateTime.parse(json["created_at"]),
        updatedAt: json["updated_at"] == null ? null : DateTime.parse(json["updated_at"]),
    );

    Map<String, dynamic> toJson() => {
        "group": group == null ? null : groupValues.reverse[group],
        "code": code == null ? null : code,
        "name": name == null ? null : name,
        "type": type == null ? null : typeValues.reverse[type],
        "charged_to": chargedTo == null ? null : chargedToValues.reverse[chargedTo],
        "fee": fee,
        "active": active == null ? null : active,
        "icon": icon == null ? null : icon,
        "service_name": serviceName == null ? null : serviceName,
        "request_code": requestCode == null ? null : requestCode,
        "id": id == null ? null : id,
        "bank_name": bankName == null ? null : bankName,
        "bank_username": bankUsername == null ? null : bankUsername,
        "bank_number": bankNumber == null ? null : bankNumber,
        "bank_code": bankCode == null ? null : bankCode,
        "path_icon": pathIcon == null ? null : pathIcon,
        "status": status == null ? null : status,
        "created_at": createdAt == null ? null : createdAt.toIso8601String(),
        "updated_at": updatedAt == null ? null : updatedAt.toIso8601String(),
    };
}

enum ChargedTo { MERCHANT }

final chargedToValues = EnumValues({
    "merchant": ChargedTo.MERCHANT
});

class FeeClass {
    FeeClass({
        this.flat,
        this.percent,
    });

    int flat;
    String percent;

    FeeClass copyWith({
        int flat,
        String percent,
    }) => 
        FeeClass(
            flat: flat ?? this.flat,
            percent: percent ?? this.percent,
        );

    factory FeeClass.fromJson(Map<String, dynamic> json) => FeeClass(
        flat: json["flat"] == null ? null : json["flat"],
        percent: json["percent"] == null ? null : json["percent"],
    );

    Map<String, dynamic> toJson() => {
        "flat": flat == null ? null : flat,
        "percent": percent == null ? null : percent,
    };
}

enum Group { E_WALLET, VIRTUAL_ACCOUNT, CONVENIENCE_STORE }

final groupValues = EnumValues({
    "Convenience Store": Group.CONVENIENCE_STORE,
    "E-Wallet": Group.E_WALLET,
    "Virtual Account": Group.VIRTUAL_ACCOUNT
});

enum Type { DIRECT }

final typeValues = EnumValues({
    "direct": Type.DIRECT
});

class EnumValues<T> {
    Map<String, T> map;
    Map<T, String> reverseMap;

    EnumValues(this.map);

    Map<T, String> get reverse {
        if (reverseMap == null) {
            reverseMap = map.map((k, v) => new MapEntry(v, k));
        }
        return reverseMap;
    }
}
