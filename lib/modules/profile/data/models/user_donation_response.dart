// To parse this JSON data, do
//
//     final userDonationResponse = userDonationResponseFromJson(jsonString);

import 'dart:convert';

UserDonationResponse userDonationResponseFromJson(String str) => UserDonationResponse.fromJson(json.decode(str));

String userDonationResponseToJson(UserDonationResponse data) => json.encode(data.toJson());

class UserDonationResponse {
    UserDonationResponse({
        this.success,
        this.message,
        this.data,
    });

    bool success;
    String message;
    UserDonation data;

    UserDonationResponse copyWith({
        bool success,
        String message,
        UserDonation data,
    }) => 
        UserDonationResponse(
            success: success ?? this.success,
            message: message ?? this.message,
            data: data ?? this.data,
        );

    factory UserDonationResponse.fromJson(Map<String, dynamic> json) => UserDonationResponse(
        success: json["success"] == null ? null : json["success"],
        message: json["message"] == null ? null : json["message"],
        data: json["data"] == null ? null : UserDonation.fromJson(json["data"]),
    );

    Map<String, dynamic> toJson() => {
        "success": success == null ? null : success,
        "message": message == null ? null : message,
        "data": data == null ? null : data.toJson(),
    };
}

class UserDonation {
    UserDonation({
        this.donations,
        this.amount,
    });

    String donations;
    String amount;

    UserDonation copyWith({
        String donations,
        String amount,
    }) => 
        UserDonation(
            donations: donations ?? this.donations,
            amount: amount ?? this.amount,
        );

    factory UserDonation.fromJson(Map<String, dynamic> json) => UserDonation(
        donations: json["donations"] == null ? null : json["donations"].toString(),
        amount: json["amount"] == null ? null : json["amount"].toString()
    );

    Map<String, dynamic> toJson() => {
        "donations": donations == null ? null : donations,
        "amount": amount == null ? null : amount,
    };
}
