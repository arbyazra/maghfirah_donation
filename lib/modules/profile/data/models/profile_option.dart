

class ProfileOption{
  final String name;
  final dynamic target;
  final bool isLogout;
  final bool isOpeningAnotherLink;

  ProfileOption({this.name, this.target,this.isLogout=false,this.isOpeningAnotherLink=false, });
}