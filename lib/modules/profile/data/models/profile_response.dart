// To parse this JSON data, do
//
//     final profileResponse = profileResponseFromJson(jsonString);

import 'dart:convert';

ProfileResponse profileResponseFromJson(String str) => ProfileResponse.fromJson(json.decode(str));

String profileResponseToJson(ProfileResponse data) => json.encode(data.toJson());

class ProfileResponse {
    ProfileResponse({
        this.success,
        this.message,
        this.data,
    });

    bool success;
    String message;
    Profile data;

    ProfileResponse copyWith({
        bool success,
        String message,
        Profile data,
    }) => 
        ProfileResponse(
            success: success ?? this.success,
            message: message ?? this.message,
            data: data ?? this.data,
        );

    factory ProfileResponse.fromJson(Map<String, dynamic> json) => ProfileResponse(
        success: json["success"] == null ? null : json["success"],
        message: json["message"] == null ? null : json["message"],
        data: json["data"] == null ? null : Profile.fromJson(json["data"]),
    );

    Map<String, dynamic> toJson() => {
        "success": success == null ? null : success,
        "message": message == null ? null : message,
        "data": data == null ? null : data.toJson(),
    };
}

class Profile {
    Profile({
        this.id,
        this.name,
        this.email,
        this.emailVerifiedAt,
        this.level,
        this.pathFoto,
        this.createdAt,
        this.updatedAt,
        this.saldo,
        this.phone,
    });

    int id;
    String name;
    String email;
    dynamic emailVerifiedAt;
    String level;
    dynamic pathFoto;
    DateTime createdAt;
    DateTime updatedAt;
    String saldo;
    dynamic phone;

    Profile copyWith({
        int id,
        String name,
        String email,
        dynamic emailVerifiedAt,
        String level,
        dynamic pathFoto,
        DateTime createdAt,
        DateTime updatedAt,
        String saldo,
        dynamic phone,
    }) => 
        Profile(
            id: id ?? this.id,
            name: name ?? this.name,
            email: email ?? this.email,
            emailVerifiedAt: emailVerifiedAt ?? this.emailVerifiedAt,
            level: level ?? this.level,
            pathFoto: pathFoto ?? this.pathFoto,
            createdAt: createdAt ?? this.createdAt,
            updatedAt: updatedAt ?? this.updatedAt,
            saldo: saldo ?? this.saldo,
            phone: phone ?? this.phone,
        );

    factory Profile.fromJson(Map<String, dynamic> json) => Profile(
        id: json["id"] == null ? null : json["id"],
        name: json["name"] == null ? null : json["name"],
        email: json["email"] == null ? null : json["email"],
        emailVerifiedAt: json["email_verified_at"],
        level: json["level"] == null ? null : json["level"],
        pathFoto: json["path_foto"],
        createdAt: json["created_at"] == null ? null : DateTime.parse(json["created_at"]),
        updatedAt: json["updated_at"] == null ? null : DateTime.parse(json["updated_at"]),
        saldo: json["saldo"] == null ? null : json["saldo"],
        phone: json["phone"],
    );

    Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "name": name == null ? null : name,
        "email": email == null ? null : email,
        "email_verified_at": emailVerifiedAt,
        "level": level == null ? null : level,
        "path_foto": pathFoto,
        "created_at": createdAt == null ? null : createdAt.toIso8601String(),
        "updated_at": updatedAt == null ? null : updatedAt.toIso8601String(),
        "saldo": saldo == null ? null : saldo,
        "phone": phone,
    };
}
