import 'dart:io';

import 'package:http/http.dart';
import 'package:maghfirah_donation/modules/modules.dart';
import 'package:maghfirah_donation/shared/shared.dart';

class ProfileRepository {
  final Network network;

  ProfileRepository({this.network});

  Future<ProfileResponse> getProfile() async {
    var url = parseUrl("/profile");
    final response = await network.getRequest(url);
    if (response.statusCode >= 200 && response.statusCode < 300) {
      print(response.body);
      return profileResponseFromJson(response.body);
    } else {
      print(response.body);
      return profileResponseFromJson(response.body);
    }
  }

  Future<UpdateProfileResponse> updateProfile(
      {Map<String, String> request}) async {
    var url = parseUrl("/profile");
    final response = await network.postRequest(url, request);
    if (response.statusCode >= 200 && response.statusCode < 300) {
      print(response.body);
      return updateProfileResponseFromJson(response.body);
    } else {
      print(response.body);
      return updateProfileResponseFromJson(response.body);
    }
  }

  Future<bool> updatePhoto(File file) async {
    var url = parseUrl("/profile/photo");
    var request = MultipartRequest("POST", Uri.parse(url));
    var pic = await MultipartFile.fromPath("photo", file.path,
        filename: file.path);
    request.files.add(pic);
    return await network.postFiles(
      request,
    );
  }

  Future<UserDonationResponse> getUserDonation() async {
    var url = parseUrl("/profile/donation");
    final response = await network.getRequest(url);
    if (response.statusCode >= 200 && response.statusCode < 300) {
      print(response.body);
      return userDonationResponseFromJson(response.body);
    } else {
      print(response.body);
      return userDonationResponseFromJson(response.body);
    }
  }

  Future<UpdatePasswordResponse> updatePassword(
      {String oldPass, String newPass}) async {
    var url = parseUrl("/profile/password");
    final response = await network.postRequest(url, {
      "old_password": oldPass,
      "new_password": newPass,
    });
    if (response.statusCode >= 200 && response.statusCode < 300) {
      print(response.body);
      return updatePasswordResponseFromJson(response.body);
    } else {
      print(response.body);
      return updatePasswordResponseFromJson(response.body);
    }
  }
}
