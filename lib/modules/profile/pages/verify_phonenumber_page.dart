import 'package:flutter/material.dart';
import 'package:maghfirah_donation/components/components.dart';
import 'package:maghfirah_donation/shared/shared.dart';
import 'package:pin_code_fields/pin_code_fields.dart';

class VerifyPhoneNumberPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: DefaultAppBarAtm(
        title: "Verifikasi",
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.only(
            top: 30,
            left: 14,
            right: 14,
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              RichText(
                text: TextSpan(
                    text: Strings.verificationCodeSentMsg,
                    style: TextStyle(
                        fontWeight: FontWeight.w500,
                        color: AppColors.textColor,
                        fontFamily: helveticaNeue),
                    children: [
                      TextSpan(
                        text: "0000-0000-0000",
                        style: TextStyle(
                            fontWeight: FontWeight.w700,
                            color: AppColors.textColor,
                            fontFamily: helveticaNeue),
                      ),
                    ]),
              ),
              SizedBox(height: 32),
              Padding(
                padding: EdgeInsets.symmetric(
                    horizontal: Dimens.width(context) * .05),
                child: PinCodeTextField(
                  appContext: context,
                  length: 4,
                  obscureText: false,
                  animationType: AnimationType.fade,
                  pinTheme: PinTheme(
                    shape: PinCodeFieldShape.box,
                    borderRadius: BorderRadius.circular(0),
                    selectedColor: Colors.grey[300],
                    inactiveColor: Colors.grey[300],
                    activeColor: Colors.grey[300],
                    disabledColor: Colors.grey[300],
                    inactiveFillColor: Colors.grey[300],
                    selectedFillColor: Colors.grey[300],
                    activeFillColor: Colors.grey[300],
                    fieldHeight: 70,
                    fieldWidth: 70,
                  ),
                  enableActiveFill: true,
                  animationDuration: Duration(milliseconds: 300),
                  // enableActiveFill: true,
                  // errorAnimationController: errorController,
                  // controller: textEditingController,
                  onCompleted: (v) {
                    print("Completed");
                  },
                  onChanged: (value) {
                    print(value);
                  },
                  beforeTextPaste: (text) {
                    print("Allowing to paste $text");
                    //if you return true then it will show the paste confirmation dialog. Otherwise if false, then nothing will happen.
                    //but you can show anything you want here, like your pop up saying wrong paste format or etc
                    return true;
                  },
                ),
              ),
              SizedBox(height: 18),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  H3Atm(
                    "Tidak menerima SMS?",
                    style: TextStylesTheme.contentText,
                  ),
                  SizedBox(width: 4),
                  H3Atm(
                    "Resend",
                    style: TextStylesTheme.sectionTitleText
                        .copyWith(color: AppColors.primaryColor),
                  ),
                ],
              ),
              SizedBox(height: 40),
              DefaultButtonMol(
                color: AppColors.primaryColor,
                whiteMode: false,
                text: "Verifikasi",
                onClick: () {},
              )
            ],
          ),
        ),
      ),
    );
  }
}
