import 'package:flutter/material.dart';
import 'package:maghfirah_donation/components/components.dart';
import '../../modules.dart';

class UpdateProfilePage extends StatelessWidget {
  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldKey,
      appBar: DefaultAppBarAtm(
        title: "Data Diri",
      ),
      body: UpdateProfileBody(scaffoldKey: scaffoldKey,),
    );
  }
}
