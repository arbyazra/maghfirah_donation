import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:maghfirah_donation/components/components.dart';
import 'package:maghfirah_donation/shared/shared.dart';
import 'package:formz/formz.dart';
import '../../modules.dart';

class UpdatePhoneNumberPage extends StatefulWidget {
  @override
  _UpdatePhoneNumberPageState createState() => _UpdatePhoneNumberPageState();
}

class _UpdatePhoneNumberPageState extends State<UpdatePhoneNumberPage> {
  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
  @override
  void initState() { 
    super.initState();
    context.read<UpdateProfileCubit>().reset();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: scaffoldKey,
        appBar: DefaultAppBarAtm(
          title: "Ubah Nomor HP",
        ),
        body: BlocConsumer<UpdateProfileCubit, UpdateProfileState>(
          listener: (context, state) {
            if (state.status.isSubmissionInProgress) {
              scaffoldKey.currentState
                  .showSnackBar(Utils.showLoadingSnackBar());
            }
            if (state.status.isSubmissionFailure) {
              var msg = context.read<UpdateProfileCubit>().msg;
              scaffoldKey.currentState
                ..hideCurrentSnackBar()
                ..showSnackBar(SnackBar(
                  duration: Duration(seconds: 2),
                  content: H3Atm(
                    msg ?? "Suatu kesalahan telah terjadi, maaf",
                  ),
                ));
            }
            if (state.status.isSubmissionSuccess) {
              scaffoldKey.currentState
                ..hideCurrentSnackBar()
                ..showSnackBar(SnackBar(
                  content: H3Atm("Berhasil Mengubah Data!"),
                  duration: Duration(milliseconds: 1300),
                )).closed.then((value) {
                  Navigator.pop(context);
                });
            }
          },
          builder: (context, state) {
            return UpdatePhoneNumberBody(
              state: state,
            );
          },
        ));
  }
}

class UpdatePhoneNumberBody extends StatefulWidget {
  final UpdateProfileState state;

  const UpdatePhoneNumberBody({Key key, this.state}) : super(key: key);
  @override
  _UpdatePhoneNumberBodyState createState() => _UpdatePhoneNumberBodyState();
}

class _UpdatePhoneNumberBodyState extends State<UpdatePhoneNumberBody> {
  final phoneCon = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.only(
          top: 30,
          left: 14,
          right: 14,
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            H3Atm(
              Strings.modifyPhoneNumberWarning,
              style: TextStyle(
                fontWeight: FontWeight.w500,
              ),
            ),
            SizedBox(height: 18),
            DefaultTextFieldAtm(
              // errorText: emailErrorText(state),
              // onChanged: (v) => _bloc.onEmailChanged(v),
              controller: phoneCon,
              validator: (v) =>
                  v.length > 0 ? null : "Nomor HP tidak boleh kosong",
              keyboardType: TextInputType.phone,
              labelText: Strings.fieldPhoneNumber,
            ),
            SizedBox(height: 40),
            DefaultButtonMol(
              color: AppColors.primaryColor,
              whiteMode: false,
              text: "Simpan",
              onClick: () {
                if (widget.state.status.isSubmissionInProgress) {
                  return;
                }
                context.read<UpdateProfileCubit>().send(
                  {
                    "phone": phoneCon.text.trim(),
                  },
                );
              },
            )
          ],
        ),
      ),
    );
  }
}
