import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:maghfirah_donation/components/components.dart';
import 'package:maghfirah_donation/modules/profile/blocs/update_profile/update_profile_cubit.dart';
import 'package:maghfirah_donation/shared/shared.dart';
import 'package:formz/formz.dart';

import '../../modules.dart';

class UpdateNamePage extends StatefulWidget {
  @override
  _UpdateNamePageState createState() => _UpdateNamePageState();
}

class _UpdateNamePageState extends State<UpdateNamePage> {
  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  void initState() { 
    super.initState();
    context.read<UpdateProfileCubit>().reset();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldKey,
      appBar: DefaultAppBarAtm(
        title: "Ubah Nama Lengkap",
        
      ),
      body: BlocConsumer<UpdateProfileCubit, UpdateProfileState>(
        listener: (context, state) {
          if (state.status.isSubmissionInProgress) {
            scaffoldKey.currentState
                .showSnackBar(Utils.showLoadingSnackBar());
          }
          if (state.status.isSubmissionFailure) {
            var msg = context.read<UpdateProfileCubit>().msg;
            scaffoldKey.currentState
              ..hideCurrentSnackBar()
              ..showSnackBar(SnackBar(
                duration: Duration(seconds: 2),
                content: H3Atm(
                  msg ?? "Suatu kesalahan telah terjadi, maaf",
                ),
              ));
          }
          if (state.status.isSubmissionSuccess) {
            scaffoldKey.currentState
              ..hideCurrentSnackBar()
              ..showSnackBar(
                SnackBar(
                  content: H3Atm("Berhasil Mengubah Data!"),
                  duration: Duration(milliseconds: 1300),
                ),
              ).closed.then((value) {
                Navigator.pop(context);
              });
          }
        },
        builder: (context, state) {
          return UpdateNameBody(
            state: state,
          );
        },
      ),
    );
  }
}

class UpdateNameBody extends StatefulWidget {
  final UpdateProfileState state;

  const UpdateNameBody({Key key, this.state}) : super(key: key);
  @override
  _UpdateNameBodyState createState() => _UpdateNameBodyState();
}

class _UpdateNameBodyState extends State<UpdateNameBody> {
  final nameCon = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.only(
          top: 30,
          left: 14,
          right: 14,
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            H3Atm(
              Strings.modifyNameWarning,
              style: TextStyle(
                fontWeight: FontWeight.w500,
              ),
            ),
            SizedBox(height: 18),
            DefaultTextFieldAtm(
              // errorText: emailErrorText(state),
              // onChanged: (v) => _bloc.onEmailChanged(v),
              controller: nameCon,
              validator: (v) => v.length > 0 ? null : "Nama tidak boleh kosong",
              labelText: Strings.fieldName,
            ),
            SizedBox(height: 40),
            DefaultButtonMol(
              color: AppColors.primaryColor,
              whiteMode: false,
              text: "Simpan",
              onClick: () {
                if (widget.state.status.isSubmissionInProgress) {
                  return;
                }
                context.read<UpdateProfileCubit>().send({
                  "name": nameCon.text.trim(),
                });
              },
            )
          ],
        ),
      ),
    );
  }
}
