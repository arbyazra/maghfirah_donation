import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import '../../../components/components.dart';
// import 'package:maghfirah_donation/shared/resources/resources.dart';
import 'package:maghfirah_donation/shared/shared.dart';

import '../../modules.dart';

class ProfilePage extends StatefulWidget {
  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  @override
  void initState() { 
    super.initState();
    context.read<ProfileBloc>().add(GetProfile());
    context.read<UserDonationBloc>().add(GetUserDonation());
  }
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<AuthBloc, AuthState>(
      builder: (context, state) {
        if (state is Unauthenticated) {
          return Scaffold(
              body: Center(
            child: CircularProgressIndicator(),
          ));
        }
        return BlocBuilder<ProfileBloc, ProfileState>(
          builder: (context, state) {
            return Scaffold(
              appBar: DefaultAppBarAtm(
                title: "Akun Saya",
              ),
              body: _buildBody(state),
            );
          },
        );
      },
    );
  }

  Widget _buildBody(ProfileState state) {
    if (state is ProfileLoaded) {
      return SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              height: Dimens.dp12,
            ),
            ProfileNameCard(
              response: state.result,
            ),
            BlocBuilder<UserDonationBloc, UserDonationState>(
              builder: (context, state) {
                if (state is UserDonationError) {
                  return SizedBox();
                }
                if (state is UserDonationLoaded) {
                  return _buildUserDonation(state.result);
                }
                return SizedBox(
                    height: 100,
                    width: double.maxFinite,
                    child: Center(child: CircularProgressIndicator()));
              },
            ),
            SizedBox(
              height: Dimens.dp32,
            ),
            DefaultDividerAtm(),
            ProfileOptionsTile(),
            DefaultDividerAtm(),
            SizedBox(
              height: Dimens.dp20,
            ),
            Padding(
              padding: const EdgeInsets.only(left: Dimens.dp16),
              child: H3Atm(
                "Versi 1.0.0",
                style: TextStyle(
                    color: AppColors.greyDark, fontWeight: FontWeight.w300),
              ),
            ),
            SizedBox(
              height: 100,
            ),
          ],
        ),
      );
    }
    return Center(child: CircularProgressIndicator());
  }

  Widget _buildUserDonation(UserDonationResponse result) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(
          height: Dimens.dp14,
        ),
        MyDonationCard(
          onTap: () {},
          icon: MaghfirahIcon.money,
          title: "Sedekah",
          value: result.data.donations,
        ),
        SizedBox(
          height: Dimens.dp20,
        ),
        MyDonationCard(
          onTap: () {},
          icon: MaghfirahIcon.donation,
          title: "Sedekah disalurkan",
          value: NumberFormat.simpleCurrency(locale: 'id')
              .format(int.parse(result.data.amount)),
        ),
      ],
    );
  }
}
