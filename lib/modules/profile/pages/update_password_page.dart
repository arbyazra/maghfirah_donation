import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:maghfirah_donation/components/components.dart';
import 'package:maghfirah_donation/shared/shared.dart';
import 'package:formz/formz.dart';
import '../../modules.dart';

class UpdatePasswordPage extends StatelessWidget {
  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
  @override
  Widget build(BuildContext context) {
    final network = AppConfigProvider.of(context).network;
    return BlocProvider(
      create: (context) =>
          UpdatePasswordCubit(repository: ProfileRepository(network: network)),
      child: Scaffold(
        key: scaffoldKey,
        appBar: DefaultAppBarAtm(
          title: "Ubah Password",
        ),
        body: BlocConsumer<UpdatePasswordCubit, UpdatePasswordState>(
          listener: (context, state) {
            if (state.status.isSubmissionInProgress) {
              scaffoldKey.currentState
                  .showSnackBar(Utils.showLoadingSnackBar());
            }
            if (state.status.isSubmissionFailure) {
              var msg = context.read<UpdatePasswordCubit>().msg;
              scaffoldKey.currentState
                ..hideCurrentSnackBar()
                ..showSnackBar(SnackBar(
                  duration: Duration(seconds: 2),
                  content: H3Atm(
                    msg ?? "Suatu kesalahan telah terjadi, maaf",
                  ),
                ));
            }
            if (state.status.isSubmissionSuccess) {
              scaffoldKey.currentState
                ..hideCurrentSnackBar()
                ..showSnackBar(
                  SnackBar(
                    content: H3Atm("Berhasil Mengubah Password!"),
                    duration: Duration(milliseconds: 1300),
                  ),
                ).closed.then((value) {
                  Navigator.pop(context);
                });
            }
          },
          builder: (context, state) {
            return UpdatePasswordBody(
              scaffoldKey: scaffoldKey,
              state: state,
            );
          },
        ),
      ),
    );
  }
}

class UpdatePasswordBody extends StatefulWidget {
  final UpdatePasswordState state;
  final GlobalKey<ScaffoldState> scaffoldKey;
  const UpdatePasswordBody({Key key, this.state, this.scaffoldKey})
      : super(key: key);
  @override
  _UpdatePasswordBodyState createState() => _UpdatePasswordBodyState();
}

class _UpdatePasswordBodyState extends State<UpdatePasswordBody> {
  UpdatePasswordCubit _bloc;
  String _oldPasswordErrorText(UpdatePasswordState state) =>
      state.oldPassword.valid ? null : Strings.passwordErrorText;

  String _newPasswordErrorText(UpdatePasswordState state) =>
      state.newPassword.valid ? null : Strings.passwordErrorText;

  String _passwordVerificationErrorText(UpdatePasswordState state) =>
      state.newPasswordVerification.valid &&
              (state.newPassword.value == state.newPasswordVerification.value)
          ? null
          : "Password harus sama";
  @override
  void initState() {
    super.initState();
    _bloc = context.read<UpdatePasswordCubit>();
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.only(
          top: 30,
          left: 14,
          right: 14,
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            H3Atm(
              Strings.modifyPasswordWarning,
              style: TextStyle(
                fontWeight: FontWeight.w500,
              ),
            ),
            SizedBox(height: 18),
            _buildFields(),
            SizedBox(height: 40),
            DefaultButtonMol(
              color: AppColors.primaryColor,
              whiteMode: false,
              text: "Simpan",
              onClick: () {
                if (!widget.state.status.isValidated) {
                  widget.scaffoldKey.currentState.showSnackBar(SnackBar(
                    content: H3Atm("Cek kolom yang belum Anda isi"),
                  ));
                  return;
                }
                _bloc.save();
              },
            )
          ],
        ),
      ),
    );
  }

  Widget _buildFields() {
    return BlocBuilder<UpdatePasswordCubit, UpdatePasswordState>(
      builder: (context, state) {
        return Column(
          children: [
            DefaultTextFieldAtm(
              labelText: Strings.fieldOldPassword,
              onChanged: (v) => _bloc.onOldPasswordChanged(v),
              suffixIcon: IconButton(
                icon: Icon(!state.isOldPasswordShown
                    ? Icons.visibility_off
                    : Icons.visibility),
                onPressed: () => _bloc.toggleOldPasswordView(),
              ),
              isObscure: !state.isOldPasswordShown,
              errorText: _oldPasswordErrorText(state),
            ),
            SizedBox(
              height: Dimens.dp32,
            ),
            DefaultTextFieldAtm(
              labelText: Strings.fieldNewPassword,
              onChanged: (v) => _bloc.onNewPasswordChanged(v),
              suffixIcon: IconButton(
                icon: Icon(!state.isNewPasswordShown
                    ? Icons.visibility_off
                    : Icons.visibility),
                onPressed: () => _bloc.toggleNewPasswordView(),
              ),
              isObscure: !state.isNewPasswordShown,
              errorText: _newPasswordErrorText(state),
            ),
            SizedBox(
              height: Dimens.dp24,
            ),
            DefaultTextFieldAtm(
              labelText: Strings.fieldNewPasswordConf,
              onChanged: (v) => _bloc.onNewPasswordVerificationChanged(v),
              suffixIcon: IconButton(
                icon: Icon(!state.isNewPasswordShown
                    ? Icons.visibility_off
                    : Icons.visibility),
                onPressed: () => _bloc.toggleNewPasswordView(),
              ),
              isObscure: !state.isNewPasswordShown,
              errorText: _passwordVerificationErrorText(state),
            ),
          ],
        );
      },
    );
  }
}
