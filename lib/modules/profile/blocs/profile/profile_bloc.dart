import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:maghfirah_donation/modules/profile/data/repositories/profile_repository.dart';
import '../../../modules.dart';

part 'profile_event.dart';
part 'profile_state.dart';

class ProfileBloc extends Bloc<ProfileEvent, ProfileState> {
  ProfileBloc({this.repository}) : super(ProfileInitial());
  
  final ProfileRepository repository;
  @override
  Stream<ProfileState> mapEventToState(
    ProfileEvent event,
  ) async* {
    if(event is ResetProfile){
      yield ProfileInitial();
    }
    if (event is GetProfile) {
      yield ProfileLoading();
      try {
        var result = await repository.getProfile();
        if (result.success) {
          yield ProfileLoaded(result);
        } else {
          yield ProfileError();
        }
      } catch (e) {
        print(e);
        yield ProfileError();
      }
    }
  }
}
