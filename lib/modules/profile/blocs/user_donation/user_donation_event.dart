part of 'user_donation_bloc.dart';

abstract class UserDonationEvent extends Equatable {
  const UserDonationEvent();

  @override
  List<Object> get props => [];
}

class GetUserDonation extends UserDonationEvent{}