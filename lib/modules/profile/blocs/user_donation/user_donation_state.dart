part of 'user_donation_bloc.dart';

abstract class UserDonationState extends Equatable {
  const UserDonationState();
  
  @override
  List<Object> get props => [];
}

class UserDonationInitial extends UserDonationState {}
class UserDonationLoading extends UserDonationState {}
class UserDonationLoaded extends UserDonationState {
  final UserDonationResponse result;

  UserDonationLoaded(this.result);
}
class UserDonationError extends UserDonationState {}
