import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:maghfirah_donation/modules/modules.dart';

part 'user_donation_event.dart';
part 'user_donation_state.dart';

class UserDonationBloc extends Bloc<UserDonationEvent, UserDonationState> {
  UserDonationBloc({this.repository}) : super(UserDonationInitial());
  final ProfileRepository repository;
  String msg;
  @override
  Stream<UserDonationState> mapEventToState(
    UserDonationEvent event,
  ) async* {
    if (event is GetUserDonation) {
      yield UserDonationLoading();
      try {
        var result = await repository.getUserDonation();
        if (result.success) {
          yield UserDonationLoaded(result);
        } else {
          msg = result.message;
          yield UserDonationError();
        }
      } catch (e) {
        print(e);
        yield UserDonationError();
      }
    }
  }
}
