part of 'update_password_cubit.dart';

class UpdatePasswordState extends Equatable {
  final FormzStatus status;
  final Password oldPassword;
  final Password newPassword;
  final Password newPasswordVerification;
  final bool isOldPasswordShown;
  final bool isNewPasswordShown;

  UpdatePasswordState({
    this.status = FormzStatus.pure,
    this.oldPassword = const Password.pure(),
    this.newPassword = const Password.pure(),
    this.newPasswordVerification = const Password.pure(),
    this.isOldPasswordShown=false,
    this.isNewPasswordShown=false,
  });
  copyWith({
    status,
    oldPassword,
    newPassword,
    newPasswordVerification,
    isOldPasswordShown,
    isNewPasswordShown
  }) =>
      UpdatePasswordState(
        status: status ?? this.status,
        isOldPasswordShown: isOldPasswordShown ?? this.isOldPasswordShown,
        isNewPasswordShown: isNewPasswordShown ?? this.isNewPasswordShown,
        oldPassword: oldPassword ?? this.oldPassword,
        newPassword: newPassword ?? this.newPassword,
        newPasswordVerification:
            newPasswordVerification ?? this.newPasswordVerification,
      );
  @override
  List<Object> get props => [
        status,
        oldPassword,
        newPassword,
        newPasswordVerification,
        isOldPasswordShown,
        isNewPasswordShown,
      ];
}
