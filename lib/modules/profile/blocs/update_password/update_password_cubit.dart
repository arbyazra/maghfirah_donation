import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:formz/formz.dart';
import 'package:maghfirah_donation/modules/modules.dart';

part 'update_password_state.dart';

class UpdatePasswordCubit extends Cubit<UpdatePasswordState> {
  UpdatePasswordCubit({this.repository}) : super(UpdatePasswordState());
  final ProfileRepository repository;
  var msg;
  void onOldPasswordChanged(String v) {
    var password = Password.dirty(v);
    emit(state.copyWith(
        oldPassword: password,
        status: Formz.validate([
          password,
          state.newPassword,
          state.newPasswordVerification,
        ])));
  }

  void toggleOldPasswordView() {
    emit(state.copyWith(
        status: Formz.validate([
          state.oldPassword,
          state.newPassword,
          state.newPasswordVerification,
        ]),
        isOldPasswordShown: !state.isOldPasswordShown));
  }

  void toggleNewPasswordView() {
    emit(state.copyWith(
        status: Formz.validate([
          state.oldPassword,
          state.newPassword,
          state.newPasswordVerification,
        ]),
        isNewPasswordShown: !state.isNewPasswordShown));
  }

  void onNewPasswordChanged(String v) {
    var password = Password.dirty(v);
    emit(state.copyWith(
        newPassword: password,
        status: Formz.validate([
          state.oldPassword,
          password,
          state.newPasswordVerification,
        ])));
  }

  void onNewPasswordVerificationChanged(String v) {
    var password = Password.dirty(v);
    emit(state.copyWith(
        newPasswordVerification: password,
        status: Formz.validate([
          state.oldPassword,
          state.newPassword,
          password,
        ])));
  }

  void save() async {
    emit(state.copyWith(status: FormzStatus.submissionInProgress));
    try {
      var result = await repository.updatePassword(
        oldPass: state.oldPassword.value,
        newPass: state.newPasswordVerification.value,
      );
      if (result.success) {
        emit(state.copyWith(status: FormzStatus.submissionSuccess));
      } else {
        msg = result.message;
        emit(state.copyWith(status: FormzStatus.submissionFailure));
      }
    } catch (e) {
      print(e);
      emit(state.copyWith(status: FormzStatus.submissionFailure));
    }
  }
}
