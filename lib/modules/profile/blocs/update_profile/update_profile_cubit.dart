import 'dart:io';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:formz/formz.dart';
import 'package:image_picker/image_picker.dart';

import '../../../modules.dart';

part 'update_profile_state.dart';

class UpdateProfileCubit extends Cubit<UpdateProfileState> {
  UpdateProfileCubit({this.repository}) : super(UpdateProfileState());
  final ProfileRepository repository;
  PickedFile file;
  var msg;

  void reset() {
    emit(state.copyWith(status: FormzStatus.pure));
    file=null;
  }

  void uploadImage() async {
    emit(state.copyWith(status: FormzStatus.submissionInProgress));
    try {
      var result = await repository.updatePhoto(File(file.path));
      if (result) {
        emit(state.copyWith(status: FormzStatus.submissionSuccess));
        emit(state.copyWith(status: FormzStatus.pure));
      } else {
        emit(state.copyWith(status: FormzStatus.submissionFailure));
        emit(state.copyWith(status: FormzStatus.pure));
      }
    } catch (e) {
      print(e);
      emit(state.copyWith(status: FormzStatus.submissionFailure));
      emit(state.copyWith(status: FormzStatus.pure));
    }
  }

  void send(Map<String, String> request) async {
    emit(state.copyWith(status: FormzStatus.submissionInProgress));
    try {
      var result = await repository.updateProfile(request: request);
      if (result.success) {
        emit(state.copyWith(status: FormzStatus.submissionSuccess));
        emit(state.copyWith(status: FormzStatus.valid));
      } else {
        msg = result.message;
        emit(state.copyWith(status: FormzStatus.submissionFailure));
        emit(state.copyWith(status: FormzStatus.valid));
      }
    } catch (e) {
      print(e);
      emit(state.copyWith(status: FormzStatus.submissionFailure));
      emit(state.copyWith(status: FormzStatus.valid));
    }
  }
}
