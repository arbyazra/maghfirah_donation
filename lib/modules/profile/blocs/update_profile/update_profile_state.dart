part of 'update_profile_cubit.dart';

class UpdateProfileState extends Equatable {
  final FormzStatus status;

  UpdateProfileState({
    this.status = FormzStatus.pure,
  });
  copyWith({status}) => UpdateProfileState(status: status ?? this.status);
  @override
  List<Object> get props => [status];
}
