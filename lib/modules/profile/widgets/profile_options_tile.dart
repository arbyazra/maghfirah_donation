import 'package:flutter/material.dart';
import 'package:maghfirah_donation/components/components.dart';
import 'package:maghfirah_donation/components/webviews/webview_screen.dart';
import 'package:maghfirah_donation/modules/modules.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:url_launcher/url_launcher.dart';

class ProfileOptionsTile extends StatelessWidget {
  final x = [
    ProfileOption(
      name: "Data Diri",
      target: UpdateProfilePage(),
    ),
    ProfileOption(
      name: "Ubah Password",
      target: UpdatePasswordPage(),
    ),
    ProfileOption(
      name: "Tentang Maghfirah",
      target: WebViewScreen(
        title: "Tentang Maghfirah",
        url: "https://maghfirah.digyta.net/about-us",
      ),
    ),
    ProfileOption(
      name: "Syarat dan Ketentuan",
      target: WebViewScreen(
        title: "Syarat dan Ketentuan",
        url: "https://maghfirah.digyta.net/term-condition",
      ),
    ),
    ProfileOption(
        name: "Review Aplikasi",
        isOpeningAnotherLink: true,
        target:
            "https://play.google.com/store/search?q=amal%20kampung%20maghfirah&c=apps"),
    ProfileOption(
      name: "Bantuan",
      target: WebViewScreen(
        title: "Bantuan",
        url: "https://maghfirah.digyta.net/help",
      ),
    ),
    ProfileOption(name: "Logout", isLogout: true),
  ];
  
  @override
  Widget build(BuildContext context) {
    return Container(
        child: ListView.separated(
      physics: NeverScrollableScrollPhysics(),
      shrinkWrap: true,
      separatorBuilder: (context, index) => DefaultDividerAtm(),
      itemCount: x.length,
      itemBuilder: (context, index) {
        final data = x[index];
        return ListTile(
          title: H3Atm(
            data.name,
            style: TextStyle(
              fontWeight: !data.isLogout ? FontWeight.w300 : FontWeight.w700,
              color: data.isLogout ? Colors.red[600] : Colors.black,
            ),
          ),
          onTap: () async {
            if (data.isOpeningAnotherLink) {
              if (await canLaunch(data.target)) {
                await launch(data.target);
              } else {
                throw 'tidak dapat menjalankan url';
              }
              return;
            }
            if (data.isLogout) {
              context.read<AuthBloc>().add(LoggedOut());
              return;
            }
            if (data.target != null) {
              await Navigator.push(
                  context, MaterialPageRoute(builder: (_) => data.target));
            }
          },
          trailing: data.isLogout
              ? Icon(
                  Icons.logout,
                  color: Colors.red[600],
                )
              : Icon(Icons.chevron_right),
        );
      },
    ));
  }
}
