import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:image_picker/image_picker.dart';
import 'package:maghfirah_donation/components/components.dart';
import 'package:maghfirah_donation/shared/shared.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:formz/formz.dart';
import '../../modules.dart';

class UpdateProfileBody extends StatefulWidget {
  final GlobalKey<ScaffoldState> scaffoldKey;

  const UpdateProfileBody({Key key, this.scaffoldKey}) : super(key: key);
  @override
  _UpdateProfileBodyState createState() => _UpdateProfileBodyState();
}

class _UpdateProfileBodyState extends State<UpdateProfileBody> {
  final RefreshController _refreshController =
      RefreshController(initialRefresh: false);

  @override
  void initState() {
    super.initState();
    context.read<UpdateProfileCubit>().reset();
    context.read<ProfileBloc>().add(GetProfile());
  }

  @override
  Widget build(BuildContext context) {
    return SmartRefresher(
        controller: _refreshController,
        onRefresh: () {
          context.read<ProfileBloc>().add(ResetProfile());
          _refreshController.refreshCompleted();
        },
        child: BlocListener<UpdateProfileCubit, UpdateProfileState>(
          listener: (context, state) {
            if (state.status.isSubmissionInProgress) {
              widget.scaffoldKey.currentState
                  .showSnackBar(Utils.showLoadingSnackBar(duration: Duration(minutes: 1)),);
            }
            if (state.status.isSubmissionFailure) {
              var msg = context.read<UpdateProfileCubit>().msg;
              widget.scaffoldKey.currentState
                ..hideCurrentSnackBar()
                ..showSnackBar(SnackBar(
                  duration: Duration(seconds: 2),
                  content: H3Atm(
                    msg ?? "Suatu kesalahan telah terjadi, maaf",
                  ),
                ));
            }
            if (state.status.isSubmissionSuccess) {
              widget.scaffoldKey.currentState
                ..hideCurrentSnackBar()
                ..showSnackBar(
                  SnackBar(
                    content: H3Atm("Berhasil Mengunggah!"),
                    duration: Duration(milliseconds: 1300),
                  ),
                ).closed.then((value) {
                });
            }
          },
          child: SingleChildScrollView(
            child: Padding(
              padding: const EdgeInsets.only(
                top: Dimens.dp32,
                bottom: 100,
                left: Dimens.dp14,
                right: Dimens.dp14,
              ),
              child: BlocConsumer<ProfileBloc, ProfileState>(
                listener: (context, state) {
                  if (state is ProfileInitial) {
                    context.read<ProfileBloc>().add(GetProfile());
                  }
                },
                builder: (context, state) {
                  if (state is ProfileError) {
                    return Center(
                      child: Text("Ups, Terjadi suatu kesalahan"),
                    );
                  }
                  if (state is ProfileLoaded) {
                    return Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        _buildProfilePhoto(state),
                        SizedBox(
                          height: Dimens.dp32,
                        ),
                        _buildForm(context, state),
                      ],
                    );
                  }
                  return Center(child: CircularProgressIndicator());
                },
              ),
            ),
          ),
        ));
  }

  Widget _buildProfilePhoto(ProfileLoaded state) {
    print(state.result.data.pathFoto);
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          SizedBox(
            width: 80,
            height: 80,
            child: ImageURLAtm(
              imageUrl: "${Config.endpoint}/storage/${state.result.data.pathFoto}",
              fileImg: context.read<UpdateProfileCubit>().file != null
                  ? File(context.read<UpdateProfileCubit>().file.path)
                  : null,
              radius: 1000,
            ),
          ),
          SizedBox(
            height: Dimens.dp20,
          ),
          InkWell(
            onTap: _showImagePicker,
            child: H3Atm(
              "Ubah Avatar",
              style: TextStyle(
                  fontWeight: FontWeight.w500, color: AppColors.primaryColor),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildForm(BuildContext context, ProfileLoaded state) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        DefaultTextFieldAtm(
          initialValue: state.result.data.name,
          labelText: Strings.fieldName,
          onTap: () => Navigator.push(context, UpdateNamePage().route()),
          suffixIcon: SizedBox(
            width: 68,
            child: IconButton(
              onPressed: () {},
              padding: const EdgeInsets.symmetric(horizontal: Dimens.dp14),
              icon: H3Atm(
                "Ubah",
                style: TextStyle(
                    fontWeight: FontWeight.w500, color: AppColors.primaryColor),
              ),
            ),
          ),
        ),
        SizedBox(
          height: Dimens.dp18,
        ),
        DefaultTextFieldAtm(
          initialValue: state.result.data.email,
          labelText: Strings.fieldEmailAddress,
        ),
        SizedBox(
          height: Dimens.dp18,
        ),
        DefaultTextFieldAtm(
          initialValue: state.result.data.phone,
          labelText: Strings.fieldPhoneNumber,
          onTap: () => Navigator.push(context, UpdatePhoneNumberPage().route()),
          suffixIcon: SizedBox(
            width: 68,
            child: IconButton(
              onPressed: () {},
              padding: const EdgeInsets.symmetric(horizontal: Dimens.dp14),
              icon: H3Atm(
                "Ubah",
                style: TextStyle(
                    fontWeight: FontWeight.w500, color: AppColors.primaryColor),
              ),
            ),
          ),
        ),
      ],
    );
  }

  void _showImagePicker() async {
    var isFromCamera = await Utils.showSourcesCamera(context);

    try {
      final pickedFile = await ImagePicker().getImage(
        source: isFromCamera ? ImageSource.camera : ImageSource.gallery,
      );

      context.read<UpdateProfileCubit>().file = pickedFile;

      setState(() {});
      context.read<UpdateProfileCubit>().uploadImage();
    } catch (e) {
      print(e);
    }
  }
}
