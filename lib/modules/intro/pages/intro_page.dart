import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:maghfirah_donation/components/components.dart';

import '../../../shared/shared.dart';
import '../../modules.dart';

class IntroPage extends StatefulWidget {
  @override
  _IntroPageState createState() => _IntroPageState();
}

class _IntroPageState extends State<IntroPage> {
  int _currentPage = 0;
  PageController _controller;

  @override
  void initState() {
    _controller = PageController(
      initialPage: _currentPage,
    );
    super.initState();
  }

  void onNextClick() async {
    if (_currentPage != IntroRepository.intros.length - 1) {
      _controller.nextPage(
        duration: Duration(milliseconds: 300),
        curve: Curves.easeInOutSine,
      );
      _currentPage += 1;
      setState(() {});
    } else {
      BlocProvider.of<AuthBloc>(context).add(HasPassedIntro());
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
            gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: [
              Color(0xFF009F3F),
              Color(0xFF0A6C31),
            ])),
        padding: EdgeInsets.symmetric(
            horizontal: Dimens.dp20, vertical: Dimens.dp32),
        child: Stack(
          children: [
            _buildPageViews(),
            _buildButtons(),
          ],
        ),
      ),
    );
  }

  Widget _buildPageViews() {
    return PageView(
        onPageChanged: (value) {
          setState(() {
            _currentPage = value;
          });
        },
        controller: _controller,
        children: IntroRepository.intros.map((e) => _buildIntro(e)).toList());
  }

  Widget _buildButtons() {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: IntroRepository.intros.map((e) {
              var isCurrentPage =
                  IntroRepository.intros.indexOf(e) == _currentPage;
              return Container(
                width: isCurrentPage ? 12 : 8,
                height: isCurrentPage ? 12 : 8,
                margin: EdgeInsets.only(
                  right: Dimens.dp4,
                  left: Dimens.dp4,
                ),
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: isCurrentPage
                      ? Colors.white
                      : Colors.white.withOpacity(0.5),
                ),
              );
            }).toList(),
          ),
          SizedBox(height: 50),
          DefaultButtonMol(
            text: "Mulai Berdonasi",
            onClick: onNextClick,
          ),
        ],
      ),
    );
  }

  Widget _buildIntro(
    Intro intro,
  ) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 60),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Expanded(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Image.asset(
                  intro.image,
                  height: 200,
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: Dimens.dp14),
                  child: H3Atm(
                    intro.description,
                    align: TextAlign.center,
                    style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.w700,
                        letterSpacing: 0.4,
                        height: 1.4),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
