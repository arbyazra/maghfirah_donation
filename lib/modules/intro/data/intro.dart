
class Intro {
  final String image;
  final String description;

  Intro({this.image, this.description});
}