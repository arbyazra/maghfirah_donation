import 'package:maghfirah_donation/shared/shared.dart';

import 'data.dart';

class IntroRepository{
  static List<Intro> intros = [
    Intro(
        description: "Harta tidak berkurang sama sekali karena sedekah",
        image: AssetPaths.intro_one),
    Intro(
        description: "Orang cerdas pasti berwakaf",
        image: AssetPaths.intro_two),
    Intro(
        description: "Zakat bersihkan dan tumbuh-kembangkan harta",
        image: AssetPaths.intro_three),
  ];
}