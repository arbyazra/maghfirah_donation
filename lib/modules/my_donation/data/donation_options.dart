class DonationOptions{
  static final statuses = [
    "Semua Status Transaksi",
    "Selesai",
    "Menunggu Pembayaran",
    "Menunggu Konfirmasi"
  ];

  static final categories = [
    ["Semua Kategori"],
    "Sedekah",
    "Wakaf",
    "Zakat",
    "Tabungan",
    "Bersih - Bersih"
  ];

  static final dates = [
    "Semua Tanggal Transaksi",
    "30 Hari Terakhir",
    "60 Hari Terakhir",
    "90 Hari Terakhir",
    "Pilih Tanggal Sendiri"
  ];
}