// To parse this JSON data, do
//
//     final myDonationResponse = myDonationResponseFromJson(jsonString);

import 'dart:convert';

import '../../../modules.dart';

MyDonationResponse myDonationResponseFromJson(String str) => MyDonationResponse.fromJson(json.decode(str));

String myDonationResponseToJson(MyDonationResponse data) => json.encode(data.toJson());

class MyDonationResponse {
    MyDonationResponse({
        this.success,
        this.message,
        this.data,
    });

    bool success;
    String message;
    List<MyDonation> data;

    MyDonationResponse copyWith({
        bool success,
        String message,
        List<MyDonation> data,
    }) => 
        MyDonationResponse(
            success: success ?? this.success,
            message: message ?? this.message,
            data: data ?? this.data,
        );

    factory MyDonationResponse.fromJson(Map<String, dynamic> json) => MyDonationResponse(
        success: json["success"] == null ? null : json["success"],
        message: json["message"] == null ? null : json["message"],
        data: json["data"] == null ? null : List<MyDonation>.from(json["data"].map((x) => MyDonation.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "success": success == null ? null : success,
        "message": message == null ? null : message,
        "data": data == null ? null : List<dynamic>.from(data.map((x) => x.toJson())),
    };
}

class MyDonation {
    MyDonation({
        this.id,
        this.projectId,
        this.userId,
        this.nominal,
        this.paymentMethod,
        this.status,
        this.timeLimit,
        this.createdAt,
        this.updatedAt,
        this.pathProof,
        this.rejectReason,
        this.donatureName,
        this.donatureEmail,
        this.specialMessage,
        this.uniqueCode,
        this.isAnonymous,
        this.total,
        this.additionalFee,
        this.paymentType,
        this.donaturePhone,
        this.reference,
        this.isAdmin,
        this.paymentCode,
        this.fundType,
        this.wishMessage,
        this.project,
        this.category,
    });

    int id;
    String projectId;
    String userId;
    String nominal;
    String paymentMethod;
    String status;
    DateTime timeLimit;
    DateTime createdAt;
    DateTime updatedAt;
    dynamic pathProof;
    dynamic rejectReason;
    String donatureName;
    String donatureEmail;
    String specialMessage;
    String uniqueCode;
    String isAnonymous;
    String total;
    String additionalFee;
    String paymentType;
    String donaturePhone;
    String reference;
    String isAdmin;
    String paymentCode;
    String fundType;
    String wishMessage;
    Project project;
    Category category;

    MyDonation copyWith({
        int id,
        String projectId,
        String userId,
        String nominal,
        String paymentMethod,
        String status,
        DateTime timeLimit,
        DateTime createdAt,
        DateTime updatedAt,
        dynamic pathProof,
        dynamic rejectReason,
        String donatureName,
        String donatureEmail,
        String specialMessage,
        String uniqueCode,
        String isAnonymous,
        String total,
        String additionalFee,
        String paymentType,
        String donaturePhone,
        String reference,
        String isAdmin,
        String paymentCode,
        String fundType,
        String wishMessage,
        Project project,
        Category category,
    }) => 
        MyDonation(
            id: id ?? this.id,
            projectId: projectId ?? this.projectId,
            userId: userId ?? this.userId,
            nominal: nominal ?? this.nominal,
            paymentMethod: paymentMethod ?? this.paymentMethod,
            status: status ?? this.status,
            timeLimit: timeLimit ?? this.timeLimit,
            createdAt: createdAt ?? this.createdAt,
            updatedAt: updatedAt ?? this.updatedAt,
            pathProof: pathProof ?? this.pathProof,
            rejectReason: rejectReason ?? this.rejectReason,
            donatureName: donatureName ?? this.donatureName,
            donatureEmail: donatureEmail ?? this.donatureEmail,
            specialMessage: specialMessage ?? this.specialMessage,
            uniqueCode: uniqueCode ?? this.uniqueCode,
            isAnonymous: isAnonymous ?? this.isAnonymous,
            total: total ?? this.total,
            additionalFee: additionalFee ?? this.additionalFee,
            paymentType: paymentType ?? this.paymentType,
            donaturePhone: donaturePhone ?? this.donaturePhone,
            reference: reference ?? this.reference,
            isAdmin: isAdmin ?? this.isAdmin,
            paymentCode: paymentCode ?? this.paymentCode,
            fundType: fundType ?? this.fundType,
            wishMessage: wishMessage ?? this.wishMessage,
            project: project ?? this.project,
            category: category ?? this.category,
        );

    factory MyDonation.fromJson(Map<String, dynamic> json) => MyDonation(
        id: json["id"] == null ? null : json["id"],
        projectId: json["project_id"] == null ? null : json["project_id"],
        userId: json["user_id"] == null ? null : json["user_id"],
        nominal: json["nominal"] == null ? null : json["nominal"],
        paymentMethod: json["payment_method"] == null ? null : json["payment_method"],
        status: json["status"] == null ? null : json["status"],
        timeLimit: json["time_limit"] == null ? null : DateTime.parse(json["time_limit"]),
        createdAt: json["created_at"] == null ? null : DateTime.parse(json["created_at"]),
        updatedAt: json["updated_at"] == null ? null : DateTime.parse(json["updated_at"]),
        pathProof: json["path_proof"],
        rejectReason: json["reject_reason"],
        donatureName: json["donature_name"] == null ? null : json["donature_name"],
        donatureEmail: json["donature_email"] == null ? null : json["donature_email"],
        specialMessage: json["special_message"] == null ? null : json["special_message"],
        uniqueCode: json["unique_code"] == null ? null : json["unique_code"],
        isAnonymous: json["is_anonymous"] == null ? null : json["is_anonymous"],
        total: json["total"] == null ? null : json["total"],
        additionalFee: json["additional_fee"] == null ? null : json["additional_fee"],
        paymentType: json["payment_type"] == null ? null : json["payment_type"],
        donaturePhone: json["donature_phone"] == null ? null : json["donature_phone"],
        reference: json["reference"] == null ? null : json["reference"],
        isAdmin: json["is_admin"] == null ? null : json["is_admin"],
        paymentCode: json["payment_code"] == null ? null : json["payment_code"],
        fundType: json["fund_type"] == null ? null : json["fund_type"],
        wishMessage: json["wish_message"] == null ? null : json["wish_message"],
        project: json["project"] == null ? null : Project.fromJson(json["project"]),
        category: json["category"] == null ? null : Category.fromJson(json["category"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "project_id": projectId == null ? null : projectId,
        "user_id": userId == null ? null : userId,
        "nominal": nominal == null ? null : nominal,
        "payment_method": paymentMethod == null ? null : paymentMethod,
        "status": status == null ? null : status,
        "time_limit": timeLimit == null ? null : timeLimit.toIso8601String(),
        "created_at": createdAt == null ? null : createdAt.toIso8601String(),
        "updated_at": updatedAt == null ? null : updatedAt.toIso8601String(),
        "path_proof": pathProof,
        "reject_reason": rejectReason,
        "donature_name": donatureName == null ? null : donatureName,
        "donature_email": donatureEmail == null ? null : donatureEmail,
        "special_message": specialMessage == null ? null : specialMessage,
        "unique_code": uniqueCode == null ? null : uniqueCode,
        "is_anonymous": isAnonymous == null ? null : isAnonymous,
        "total": total == null ? null : total,
        "additional_fee": additionalFee == null ? null : additionalFee,
        "payment_type": paymentType == null ? null : paymentType,
        "donature_phone": donaturePhone == null ? null : donaturePhone,
        "reference": reference == null ? null : reference,
        "is_admin": isAdmin == null ? null : isAdmin,
        "payment_code": paymentCode == null ? null : paymentCode,
        "fund_type": fundType == null ? null : fundType,
        "wish_message": wishMessage == null ? null : wishMessage,
        "project": project == null ? null : project.toJson(),
        "category": category == null ? null : category.toJson(),
    };
}

class Project {
    Project({
        this.id,
        this.title,
        this.content,
        this.pathFeatured,
        this.nominalTarget,
        this.dateTarget,
        this.categoryId,
        this.userId,
        this.isFixed,
        this.status,
        this.createdAt,
        this.updatedAt,
        this.slug,
        this.type,
        this.wakafPrice,
        this.wakafUnit,
    });

    int id;
    String title;
    String content;
    String pathFeatured;
    dynamic nominalTarget;
    dynamic dateTarget;
    String categoryId;
    String userId;
    String isFixed;
    String status;
    DateTime createdAt;
    DateTime updatedAt;
    String slug;
    String type;
    String wakafPrice;
    dynamic wakafUnit;

    Project copyWith({
        int id,
        String title,
        String content,
        String pathFeatured,
        dynamic nominalTarget,
        dynamic dateTarget,
        String categoryId,
        String userId,
        String isFixed,
        String status,
        DateTime createdAt,
        DateTime updatedAt,
        String slug,
        String type,
        String wakafPrice,
        dynamic wakafUnit,
    }) => 
        Project(
            id: id ?? this.id,
            title: title ?? this.title,
            content: content ?? this.content,
            pathFeatured: pathFeatured ?? this.pathFeatured,
            nominalTarget: nominalTarget ?? this.nominalTarget,
            dateTarget: dateTarget ?? this.dateTarget,
            categoryId: categoryId ?? this.categoryId,
            userId: userId ?? this.userId,
            isFixed: isFixed ?? this.isFixed,
            status: status ?? this.status,
            createdAt: createdAt ?? this.createdAt,
            updatedAt: updatedAt ?? this.updatedAt,
            slug: slug ?? this.slug,
            type: type ?? this.type,
            wakafPrice: wakafPrice ?? this.wakafPrice,
            wakafUnit: wakafUnit ?? this.wakafUnit,
        );

    factory Project.fromJson(Map<String, dynamic> json) => Project(
        id: json["id"] == null ? null : json["id"],
        title: json["title"] == null ? null : json["title"],
        content: json["content"] == null ? null : json["content"],
        pathFeatured: json["path_featured"] == null ? null : json["path_featured"],
        nominalTarget: json["nominal_target"],
        dateTarget: json["date_target"],
        categoryId: json["category_id"] == null ? null : json["category_id"],
        userId: json["user_id"] == null ? null : json["user_id"],
        isFixed: json["is_fixed"] == null ? null : json["is_fixed"],
        status: json["status"] == null ? null : json["status"],
        createdAt: json["created_at"] == null ? null : DateTime.parse(json["created_at"]),
        updatedAt: json["updated_at"] == null ? null : DateTime.parse(json["updated_at"]),
        slug: json["slug"] == null ? null : json["slug"],
        type: json["type"] == null ? null : json["type"],
        wakafPrice: json["wakaf_price"] == null ? null : json["wakaf_price"],
        wakafUnit: json["wakaf_unit"],
    );

    Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "title": title == null ? null : title,
        "content": content == null ? null : content,
        "path_featured": pathFeatured == null ? null : pathFeatured,
        "nominal_target": nominalTarget,
        "date_target": dateTarget,
        "category_id": categoryId == null ? null : categoryId,
        "user_id": userId == null ? null : userId,
        "is_fixed": isFixed == null ? null : isFixed,
        "status": status == null ? null : status,
        "created_at": createdAt == null ? null : createdAt.toIso8601String(),
        "updated_at": updatedAt == null ? null : updatedAt.toIso8601String(),
        "slug": slug == null ? null : slug,
        "type": type == null ? null : type,
        "wakaf_price": wakafPrice == null ? null : wakafPrice,
        "wakaf_unit": wakafUnit,
    };
}
