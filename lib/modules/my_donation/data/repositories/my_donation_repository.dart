import 'package:maghfirah_donation/shared/shared.dart';

import '../../../modules.dart';

class MyDonationRepository {
  final Network network;
  MyDonationRepository({this.network});

  Future<MyDonationResponse> getMyDonations(
      {int days,
      int categoryId,
      String paymentStatus,
      String dateRange}) async {
    var url = parseUrl("/transaction?page=1&limit=9999");
    var queryParams = {
      'days': '1000',
      'page':'1',
      'limit':'99999'
    };
    if (days != null) {
      queryParams['days'] = "$days";
    }
    if (categoryId != null) {
      queryParams['category_id'] = "$categoryId";
    }
    if (paymentStatus != null) {
      queryParams['status'] = paymentStatus;
    }
    if (dateRange != null) {
      queryParams['dateRange'] = dateRange;
    }
    print(Uri.parse(url).replace(queryParameters: queryParams).toString());
    final response = await network.getRequest(
      Uri.parse(url).replace(queryParameters: queryParams).toString(),
    );
    if (response.statusCode >= 200 && response.statusCode < 300) {
      return myDonationResponseFromJson(response.body);
    } else {
      print(response.body);
      return myDonationResponseFromJson(response.body);
    }
  }
}
