part of 'my_donation_bloc.dart';

abstract class MyDonationEvent extends Equatable {
  const MyDonationEvent();

  @override
  List<Object> get props => [];
}
class ResetMyDonation extends MyDonationEvent{}
class GetMyDonation extends MyDonationEvent{
  final int days;
  final int categoryId;
  final String dateRange;
  final String paymentStatus;

  GetMyDonation({this.paymentStatus, this.days, this.categoryId,this.dateRange, });
}