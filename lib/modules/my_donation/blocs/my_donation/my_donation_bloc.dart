import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:maghfirah_donation/modules/modules.dart';

part 'my_donation_event.dart';
part 'my_donation_state.dart';

class MyDonationBloc extends Bloc<MyDonationEvent, MyDonationState> {
  MyDonationBloc({this.repository}) : super(MyDonationInitial());
  final MyDonationRepository repository;
  var msg;
  @override
  Stream<MyDonationState> mapEventToState(
    MyDonationEvent event,
  ) async* {
    if (event is MyDonationEvent) {
      yield MyDonationInitial();
    }
    if (event is GetMyDonation) {
      print(event.days);
      try {
        var result = await repository.getMyDonations(
            categoryId: event.categoryId,
            days: event.days,
            dateRange: event.dateRange,
            paymentStatus: event.paymentStatus,);
        if (result.success) {
          yield MyDonationLoaded(result);
        } else {
          msg = result.message;
          yield MyDonationError();
        }
      } catch (e) {
        print(e);
        yield MyDonationError();
      }
    }
  }
}
