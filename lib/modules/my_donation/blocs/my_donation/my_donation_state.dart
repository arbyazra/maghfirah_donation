part of 'my_donation_bloc.dart';

abstract class MyDonationState extends Equatable {
  const MyDonationState();
  
  @override
  List<Object> get props => [];
}

class MyDonationInitial extends MyDonationState {}
class MyDonationLoading extends MyDonationState {}
class MyDonationLoaded extends MyDonationState {
  final MyDonationResponse myDonation;

  MyDonationLoaded(this.myDonation);
}
class MyDonationError extends MyDonationState {}
