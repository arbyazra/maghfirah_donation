import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:formz/formz.dart';
import 'package:maghfirah_donation/modules/my_donation/widgets/donation_filter_dialog.dart';

part 'donation_filter_state.dart';

class DonationFilterCubit extends Cubit<DonationFilterState> {
  DonationFilterCubit() : super(DonationFilterState());

  void reset() {
    emit(DonationFilterState());
  }

  void change({
    DonationFilterCategory value,
    int index,
    int selectedCategoryId,
    DateTime startTime,
    DateTime untilTime,
    bool autoSearch = true,
    String searchQuery,
  }) {
    emit(state.copyWith(filterStatus: FormzStatus.submissionInProgress));
    switch (value) {
      case DonationFilterCategory.BY_SEARCH_QUERY:
        emit(state.copyWith(
          status: value,
          selectedStatus: index,
          filterStatus: FormzStatus.submissionSuccess,
          autoSearch: autoSearch,
          searchQuery: searchQuery,
        ));
        break;
      case DonationFilterCategory.BY_PAYMENT_STATUS:
        emit(state.copyWith(
            status: value,
            selectedStatus: index,
            filterStatus: FormzStatus.submissionSuccess,
            autoSearch: autoSearch));
        break;
      case DonationFilterCategory.BY_DONATION_TYPE:
        emit(state.copyWith(
            category: value,
            selectedCategory: index,
            selectedCategoryId: selectedCategoryId,
            filterStatus: FormzStatus.submissionSuccess,
            autoSearch: autoSearch));
        break;
      case DonationFilterCategory.BY_DATE:
        emit(state.copyWith(
            date: value,
            selectedDate: index,
            filterStatus: FormzStatus.submissionSuccess,
            startTime: startTime,
            untilTime: untilTime,
            autoSearch: autoSearch));
        break;
      default:
        print("undefined category");
    }
    // emit(state.copyWith());
  }
}
