part of 'donation_filter_cubit.dart';

class DonationFilterState extends Equatable {
  final DonationFilterCategory category;
  final DonationFilterCategory status;
  final DonationFilterCategory date;
  final DateTime startTime;
  final DateTime untilTime;
  final int selectedCategory;
  final int selectedCategoryId;
  final int selectedStatus;
  final int selectedDate;
  final FormzStatus filterStatus;
  final bool autoSearch;
  final String searchQuery;

  DonationFilterState({
    this.category,
    this.startTime,
    this.searchQuery="", 
    this.autoSearch=false, 
    this.untilTime,
    this.filterStatus = FormzStatus.pure,
    this.status,
    this.selectedCategoryId,
    this.date,
    this.selectedCategory,
    this.selectedStatus,
    this.selectedDate,
  });
  copyWith({
    category,
    status,
    date,
    selectedCategory,
    selectedCategoryId,
    selectedStatus,
    filterStatus,
    selectedDate,
    autoSearch,
    startTime,
    searchQuery,
    untilTime,
  }) =>
      DonationFilterState(
        category: category ?? this.category,
        status: status ?? this.status,
        date: date ?? this.date,
        selectedCategory: selectedCategory ?? this.selectedCategory,
        selectedCategoryId: selectedCategoryId ?? this.selectedCategoryId,
        selectedStatus: selectedStatus ?? this.selectedStatus,
        selectedDate: selectedDate ?? this.selectedDate,
        filterStatus: filterStatus ?? this.filterStatus,
        startTime: startTime ?? this.startTime,
        untilTime: untilTime ?? this.untilTime,
        autoSearch: autoSearch ?? this.autoSearch,
        searchQuery: searchQuery ?? this.searchQuery,
      );
  @override
  List<Object> get props => [
        category,
        status,
        date,
        selectedCategory,
        selectedStatus,
        selectedDate,
        selectedCategoryId,
        filterStatus,
        untilTime,
        startTime,
        autoSearch,
        searchQuery,
      ];
}
