import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:maghfirah_donation/components/components.dart';
import 'package:maghfirah_donation/modules/modules.dart';
import 'package:maghfirah_donation/shared/providers/providers.dart';

class MyDonationPage extends StatelessWidget {
  final tabs = ["Semua Status", "Semua Kategori", "Semua Tanggal"];
  @override
  Widget build(BuildContext context) {
    final network = AppConfigProvider.of(context).network;
    return MultiBlocProvider(
      providers: [
        BlocProvider(
          create: (context) =>
          MyDonationBloc(repository: MyDonationRepository(network: network)),
        ),
        BlocProvider(
          create: (context) =>
          DonationTypeBloc(repository: DonationRepository(network: network)),
        )
      ],
      child: BlocBuilder<AuthBloc, AuthState>(
        builder: (context, state) {
          if (state is Unauthenticated) {
            return Scaffold(
                body: Center(
              child: CircularProgressIndicator(),
            ));
          }
          return DefaultTabController(
            length: tabs.length,
            child: Scaffold(
                appBar: DefaultAppBarAtm(
                  title: "Sedekah Saya",
                ),
                body: MyDonationBody()),
          );
        },
      ),
    );
  }
}
