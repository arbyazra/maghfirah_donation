import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:maghfirah_donation/components/components.dart';
import 'package:maghfirah_donation/shared/shared.dart';

import '../../modules.dart';

class MyDonationDataView extends StatefulWidget {
  final MyDonationResponse myDonations;

  const MyDonationDataView({Key key, this.myDonations}) : super(key: key);
  @override
  _MyDonationDataViewState createState() => _MyDonationDataViewState();
}

class _MyDonationDataViewState extends State<MyDonationDataView> {
  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: BlocBuilder<DonationFilterCubit, DonationFilterState>(
        builder: (context, state) {
          return ListView.separated(
            separatorBuilder: (context, index) {
              var data = widget.myDonations.data[index];
              var query = "${data?.category?.category}"
                  " ${data?.project?.title}"
                  " ${data?.status}";
              return state.searchQuery != null && state.searchQuery.length > 0
                  ? query
                          .toLowerCase()
                          .contains(state.searchQuery.toLowerCase())
                      ? SizedBox(
                          height: Dimens.dp20,
                        )
                      : SizedBox()
                  : SizedBox(
                      height: Dimens.dp20,
                    );
            },
            padding: EdgeInsets.only(
                left: Dimens.dp18,
                right: Dimens.dp18,
                top: Dimens.dp14,
                bottom: 100),
            itemCount: widget.myDonations.data.length,
            itemBuilder: (context, index) {
              var data = widget.myDonations.data[index];
              var query = "${data?.category?.category}"
                  " ${data?.project?.title}"
                  " ${data?.status}";
              return state.searchQuery != null && state.searchQuery.length > 0
                  ? query
                          .toLowerCase()
                          .contains(state.searchQuery.toLowerCase())
                      ? MyDonationItemCard(
                          myDonation: data,
                        )
                      : SizedBox()
                  : MyDonationItemCard(
                      myDonation: data,
                    );
            },
          );
        },
      ),
    );
  }
}
