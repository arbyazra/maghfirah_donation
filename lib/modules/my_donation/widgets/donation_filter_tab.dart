import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:maghfirah_donation/components/components.dart';
import 'package:maghfirah_donation/shared/shared.dart';

import '../../modules.dart';
import '../my_donation.dart';

class DonationFilterTab extends StatefulWidget {
  const DonationFilterTab({
    Key key,
  }) : super(key: key);
  @override
  _DonationFilterTabState createState() => _DonationFilterTabState();
}

class _DonationFilterTabState extends State<DonationFilterTab> {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<DonationTypeBloc, DonationTypeState>(
      builder: (context, typeState) {
        if (typeState is DonationTypeLoaded) {
          return BlocBuilder<DonationFilterCubit, DonationFilterState>(
            builder: (context, state) {
              return Padding(
                padding: const EdgeInsets.only(top: Dimens.dp16),
                child: _buildTabs(state,typeState),
              );
            },
          );
        }
        return Center(child: CircularProgressIndicator(),);
      },
    );
  }

  String _getTitleDialog(DonationFilterCategory category, int value,DonationTypeLoaded typeState) {
    switch (category) {
      case DonationFilterCategory.BY_PAYMENT_STATUS:
        return DonationOptions.statuses[value];
        break;
      case DonationFilterCategory.BY_DONATION_TYPE:
        return typeState.result.data[value].category.capitalize();
        break;
      case DonationFilterCategory.BY_DATE:
        return DonationOptions.dates[value];
        break;
      default:
        return "";
    }
  }

  Widget _buildTabs(DonationFilterState state,DonationTypeLoaded typeState) {
    return Container(
      height: 40,
      padding: EdgeInsets.only(left: Dimens.dp14, right: Dimens.dp14),
      child: TabBar(
        isScrollable: true,
        onTap: (value) {
          switch (value) {
            case 0:
              showDonationFilterDialog(
                  DonationFilterCategory.BY_PAYMENT_STATUS);
              break;
            case 1:
              showDonationFilterDialog(DonationFilterCategory.BY_DONATION_TYPE,types: typeState.result.data);
              break;
            case 2:
              showDonationFilterDialog(DonationFilterCategory.BY_DATE);
              break;
          }
          // _bloc.change(value);
        },
        indicator: BoxDecoration(),
        labelColor: AppColors.textColor,
        unselectedLabelColor: AppColors.textColor,
        indicatorPadding: EdgeInsets.zero,
        labelPadding: EdgeInsets.zero,
        tabs: [
          _buildTabItem(
              state.status == null || state.selectedStatus == null
                  ? DonationOptions.statuses[0]
                  : _getTitleDialog(state.status, state.selectedStatus,typeState),
              isEdged: true),
          _buildTabItem(state.category == null || state.selectedCategory == null
              ? typeState.result.data[0].category.capitalize()
              : _getTitleDialog(state.category, state.selectedCategory,typeState)),
          _buildTabItem(
              state.date == null || state.selectedDate == null
                  ? DonationOptions.dates[0]
                  : _getTitleDialog(state.date, state.selectedDate,typeState),
              isEdged: true),
        ],
      ),
    );
  }

  Widget _buildTabItem(String e, {bool isEdged = false}) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: !isEdged ? Dimens.dp10 : 0),
      padding: EdgeInsets.symmetric(horizontal: Dimens.dp10),
      decoration: BoxDecoration(
        // color: state.curIdx == widget.tabs.indexOf(e)
        //     ? AppColors.primaryColor
        //     : Colors.white,
        border: Border.fromBorderSide(BorderSide(color: AppColors.grey2)),
        borderRadius: BorderRadius.circular(6),
      ),
      child: Tab(
        child: Row(
          children: [
            H3Atm(
              e,
              style: TextStyle(fontWeight: FontWeight.w300),
            ),
            SizedBox(
              width: Dimens.dp4,
            ),
            Icon(Icons.keyboard_arrow_down_outlined)
          ],
        ),
      ),
    );
  }

  void showDonationFilterDialog(DonationFilterCategory category,{List<DonationCategory> types}) {
    showModalBottomSheet(
      isScrollControlled: true,
      context: context,
      builder: (context) {
        return DonationFilterDialog(
          category: category,
          types: types,
        );
      },
    );
  }
}
