import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:maghfirah_donation/components/components.dart';
import 'package:maghfirah_donation/modules/donation/data/models/donation_category_response.dart';
import 'package:maghfirah_donation/shared/shared.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../my_donation.dart';

enum DonationFilterCategory { BY_PAYMENT_STATUS, BY_DONATION_TYPE, BY_DATE,BY_SEARCH_QUERY }

class DonationFilterDialog extends StatelessWidget {
  final DonationFilterCategory category;
  final List<DonationCategory> types;
  DonationFilterDialog({Key key, this.category, this.types}) : super(key: key);

  List<String> _getFilterOptions() {
    switch (category) {
      case DonationFilterCategory.BY_PAYMENT_STATUS:
        return DonationOptions.statuses;
        break;
      case DonationFilterCategory.BY_DONATION_TYPE:
        return types.map((element) => element.category.capitalize()).toList();
        break;
      case DonationFilterCategory.BY_DATE:
        return DonationOptions.dates;
        break;
      default:
        return DonationOptions.dates;
    }
  }

  String _getTitleDialog() {
    switch (category) {
      case DonationFilterCategory.BY_PAYMENT_STATUS:
        return "Pilih Status Pembayaran";
        break;
      case DonationFilterCategory.BY_DONATION_TYPE:
        return "Pilih Kategori";
        break;
      case DonationFilterCategory.BY_DATE:
        return "Pilih Tanggal";
        break;
      default:
        return "";
    }
  }

  bool isSelected(DonationFilterState state, int index) {
    switch (category) {
      case DonationFilterCategory.BY_PAYMENT_STATUS:
        if (state.selectedStatus == null && index == 0)
          return true;
        else
          return state.selectedStatus == index;
        break;
      case DonationFilterCategory.BY_DONATION_TYPE:
        if (state.selectedCategory == null && index == 0)
          return true;
        else
          return state.selectedCategory == index;
        break;
      case DonationFilterCategory.BY_DATE:
        if (state.selectedDate == null && index == 0)
          return true;
        else
          return state.selectedDate == index;
        break;
      default:
        print("undefined category");
        return false;
    }
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<DonationFilterCubit, DonationFilterState>(
      builder: (context, state) {
        return Container(
          height: Dimens.height(context) * _getFilterOptions().length * .14,
          child: buildFilterContent(state, context),
        );
      },
    );
  }

  Widget buildFilterContent(DonationFilterState state, BuildContext context) {
    return Column(
      children: [
        Row(
          children: [
            IconButton(
                icon: Icon(Icons.close),
                onPressed: () => Navigator.pop(context)),
            Expanded(
              child: H2Atm(
                _getTitleDialog(),
                // activity.title,
                maxLine: 1,
                overflow: TextOverflow.ellipsis,
                style: TextStyle(
                  fontWeight: FontWeight.w700,
                ),
              ),
            ),
          ],
        ),
        Expanded(
          child: ListView.separated(
            itemCount: _getFilterOptions().length,
            separatorBuilder: (context, index) => DefaultDividerAtm(),
            itemBuilder: (context, index) {
              final data = _getFilterOptions()[index];
              return ListTile(
                leading: H2Atm(
                  data,
                  // activity.title,
                  maxLine: 1,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                    fontWeight: FontWeight.w500,
                  ),
                ),
                onTap: () {
                  if (index == 4 &&
                      category == DonationFilterCategory.BY_DATE) {
                    context.read<DonationFilterCubit>().change(
                        value: category, index: index, selectedCategoryId: -1);
                    return;
                  }

                  context.read<DonationFilterCubit>().change(
                      value: category,
                      index: index,
                      selectedCategoryId:
                          category == DonationFilterCategory.BY_DONATION_TYPE
                              ? types
                                  .firstWhere((element) =>
                                      element.category.toLowerCase() ==
                                      data.toLowerCase())
                                  .id
                              : -1);
                  Navigator.pop(context);
                },
                trailing: Container(
                  width: 18,
                  height: 18,
                  decoration: BoxDecoration(
                      color: isSelected(state, index)
                          ? AppColors.primaryColor
                          : Colors.white,
                      border: Border.fromBorderSide(BorderSide(
                          color: !isSelected(state, index)
                              ? AppColors.greyDark
                              : Colors.transparent)),
                      shape: BoxShape.circle),
                ),
              );
            },
          ),
        ),
        state.selectedDate == 4 && category==DonationFilterCategory.BY_DATE ? _buildCustomDate(context, state) : SizedBox(),
      ],
    );
  }

  Widget _buildCustomDate(BuildContext context, DonationFilterState state) {
    print(state.startTime);
    return Padding(
      padding: const EdgeInsets.fromLTRB(20, 0, 20, 12),
      child: Column(
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Expanded(
                child: DefaultTextFieldAtm(
                  hasOutlineBorder: true,
                  hintText:state.startTime!=null?  DateFormat('dd MM yyyy')
                      .format(state.startTime):"Pilih Tanggal",
                  onTap: () => _selectDate(context, 4, state),
                ),
              ),
              SizedBox(width: 10),
              Icon(Icons.remove, color: Colors.grey),
              SizedBox(width: 10),
              Expanded(
                child: DefaultTextFieldAtm(
                  hasOutlineBorder: true,
                  hintText:state.untilTime!=null? DateFormat('dd MM yyyy')
                      .format(state.untilTime):"Pilih Tanggal",
                  onTap: () => _selectDate(context, 4, state, lastDate: true),
                ),
              ),
            ],
          ),
          SizedBox(height: 10),
          DefaultButtonMol(
            whiteMode: false,
            text: "Cari",
            onClick: () {
              context.read<DonationFilterCubit>().change(
                  value: category,
                  index: 4,
                  selectedCategoryId: -1,
                  autoSearch: true);
              Navigator.pop(context);
            },
          )
        ],
      ),
    );
  }

  Future<void> _selectDate(
      BuildContext context, int index, DonationFilterState state,
      {lastDate = false}) async {
    DateTime selectedDate = DateTime.now();
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: lastDate
            ? state.untilTime ?? DateTime.now()
            : state.startTime ?? DateTime.now(),
        firstDate: DateTime(2015, 8),
        lastDate: DateTime(2101));
    if (picked != null && picked != selectedDate) {
      if (lastDate) {
        context.read<DonationFilterCubit>().change(
            value: category,
            index: index,
            selectedCategoryId: -1,
            untilTime: picked,
            autoSearch: false);
      } else {
        context.read<DonationFilterCubit>().change(
            value: category,
            index: index,
            selectedCategoryId: -1,
            startTime: picked,
            autoSearch: false);
      }
    }
  }
}
