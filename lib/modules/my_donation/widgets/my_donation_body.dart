import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:maghfirah_donation/components/components.dart';
import 'package:maghfirah_donation/modules/donation/donation.dart';
import 'package:maghfirah_donation/shared/shared.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../my_donation.dart';
import 'package:formz/formz.dart';

class MyDonationBody extends StatefulWidget {
  @override
  _MyDonationBodyState createState() => _MyDonationBodyState();
}

class _MyDonationBodyState extends State<MyDonationBody> {
  @override
  void initState() {
    context.read<DonationTypeBloc>().add(GetDonationType(isMyDonation: true));
    context.read<DonationFilterCubit>().reset();
    context.read<MyDonationBloc>().add(GetMyDonation());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          color: Colors.white,
          child: Column(
            children: [
              ColorizedTextFieldAtm(
                verticalPadding: 10,
                hintText: "Cari transaksi",
                onChanged: (value) => context
                    .read<DonationFilterCubit>()
                    .change(
                        searchQuery: value,
                        value: DonationFilterCategory.BY_SEARCH_QUERY),
                prefixIcon: Icon(
                  Icons.search,
                  color: Colors.black,
                ),
                // suffixIcon: Icon(Icons.search,color: Colors.black,),
              ),
              DonationFilterTab(),
              SizedBox(
                height: Dimens.dp10,
              ),
              DefaultDividerAtm(),
            ],
          ),
        ),
        BlocListener<DonationFilterCubit, DonationFilterState>(
          listener: (context, state) {
            if (state.filterStatus.isSubmissionSuccess) {
              if ((state.selectedCategory != 0 ||
                      state.selectedDate != 0 ||
                      state.selectedStatus != 0) &&
                  state.autoSearch) {
                context.read<MyDonationBloc>().add(ResetMyDonation());
                context.read<MyDonationBloc>().add(
                      GetMyDonation(
                        dateRange: state.selectedDate != 4
                            ? null
                            : state.startTime != null && state.untilTime != null
                                ? "${DateFormat('yyyy-MM-dd').format(state.startTime)},${DateFormat('yyyy-MM-dd').format(state.untilTime)}"
                                : null,
                        days: state.selectedDate == null
                            ? null
                            : state.selectedDate == 1
                                ? 30
                                : state.selectedDate == 2
                                    ? 60
                                    : state.selectedDate == 3
                                        ? 90
                                        : null,
                        paymentStatus: state.selectedStatus == null
                            ? null
                            : state.selectedStatus == 1
                                ? 'success'
                                : state.selectedStatus == 2
                                    ? 'pending'
                                    : state.selectedStatus == 3
                                        ? 'waiting'
                                        : null,
                        categoryId: state.selectedCategoryId == null
                            ? null
                            : state.selectedCategoryId < 0
                                ? null
                                : state.selectedCategoryId,
                      ),
                    );
              }
            }
          },
          child: BlocBuilder<MyDonationBloc, MyDonationState>(
            builder: (context, state) {
              if (state is MyDonationLoaded) {
                if (state.myDonation.data.isEmpty) {
                  return _buildEmptyState();
                }
                return MyDonationDataView(
                  myDonations: state.myDonation,
                );
              }
              if (state is MyDonationError) {
                return _buildErrorState();
              }
              return Expanded(
                  child: Center(
                child: CircularProgressIndicator(),
              ));
            },
          ),
        ),
      ],
    );
  }

  Widget _buildErrorState() {
    return Container(
      child: Center(
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                height: 100,
                child: Image.asset(AssetPaths.messageEmpty),
              ),
              SizedBox(
                height: 12,
              ),
              H2Atm(
                "Ups, ada sedikit kendala di server kami",
                style: TextStyle(fontWeight: FontWeight.w500),
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildEmptyState() {
    return Container(
      child: Center(
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                height: 100,
                child: Image.asset(AssetPaths.delivered),
              ),
              SizedBox(
                height: 12,
              ),
              H2Atm(
                "Anda belum bersedekah",
                style: TextStyle(fontWeight: FontWeight.w500),
              )
            ],
          ),
        ),
      ),
    );
  }
}
