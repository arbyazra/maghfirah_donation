part of 'slider_bloc.dart';

abstract class SliderState extends Equatable {
  const SliderState();
  
  @override
  List<Object> get props => [];
}

class SliderInitial extends SliderState {}
class SliderLoading extends SliderState {}
class SliderLoaded extends SliderState {
  final SliderResponse resultTop;
  final SliderResponse resultBottom;

  SliderLoaded(this.resultTop,this.resultBottom);
}
class SliderError extends SliderState {}
