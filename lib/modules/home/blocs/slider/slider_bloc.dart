import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:maghfirah_donation/modules/home/home.dart';

part 'slider_event.dart';
part 'slider_state.dart';

class SliderBloc extends Bloc<SliderEvent, SliderState> {
  SliderBloc({this.repository}) : super(SliderInitial());
  final HomeRepository repository;
  String msg;
  @override
  Stream<SliderState> mapEventToState(
    SliderEvent event,
  ) async* {
    if (event is GetSliders) {
      yield SliderLoading();
      try {
        var resultTop = await repository.getSliders();
        var resultBottom = await repository.getSliders(pos: "bottom");
        if (resultTop.success && resultBottom.success) {
          yield SliderLoaded(
            resultTop,
            resultBottom,
          );
        } else {
          msg = resultTop.message;
          msg = resultBottom.message;
          yield SliderError();
        }
      } catch (e) {
        yield SliderError();
      }
    }
  }
}
