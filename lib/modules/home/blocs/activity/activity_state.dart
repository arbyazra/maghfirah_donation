part of 'activity_bloc.dart';

abstract class ActivityState extends Equatable {
  const ActivityState();
  
  @override
  List<Object> get props => [];
}

class ActivityInitial extends ActivityState {}
class ActivityLoading extends ActivityState {}
class ActivityLoaded extends ActivityState {
  final ActivityResponse result;

  ActivityLoaded(this.result);
}
class ActivityError extends ActivityState {}
