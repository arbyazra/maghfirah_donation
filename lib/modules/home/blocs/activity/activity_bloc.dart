import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

import '../../../modules.dart';

part 'activity_event.dart';
part 'activity_state.dart';

class ActivityBloc extends Bloc<ActivityEvent, ActivityState> {
  ActivityBloc({this.repository}) : super(ActivityInitial());
  final HomeRepository repository;
  @override
  Stream<ActivityState> mapEventToState(
    ActivityEvent event,
  ) async* {
    if(event is ResetActivity){
      yield ActivityInitial();
    }
    if (event is GetActivity) {
      yield ActivityLoading();
      try {
        var result = await repository.getActivity();
        if (result.success) {
          yield ActivityLoaded(result);
        } else {
          yield ActivityError();
        }
      } catch (e) {
        print(e);
        yield ActivityError();
      }
    }
  }
}
