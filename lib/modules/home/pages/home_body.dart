import 'package:flutter/material.dart';
import 'package:maghfirah_donation/components/components.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../modules.dart';

class HomeBody extends StatefulWidget {
  @override
  _HomeBodyState createState() => _HomeBodyState();
}

class _HomeBodyState extends State<HomeBody> {
  @override
  void initState() {
    context.read<ActivityBloc>().add(GetActivity());
    context.read<SliderBloc>().add(GetSliders());
    context.read<DonationBloc>().add(GetDonation(null, null));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 100),
      child: Column(
        children: [
          DefaultCarousels(
            height: 170,
          ),
          SizedBox(
            height: 6,
          ),
          ActivityListView(),
          Padding(
            padding: const EdgeInsets.only(
              top: 20,
            ),
            child: DefaultCarousels(
              height: 170,
              isTop: false,
              isHeader: false,
            ),
          ),
          SizedBox(
            height: 12,
          ),
          DonationListView(),
        ],
      ),
    );
  }
}
