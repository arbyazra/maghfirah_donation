import 'package:flutter/material.dart';
import 'package:maghfirah_donation/components/components.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import '../../modules.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class HomePage extends StatelessWidget {
  final RefreshController _refreshController =
      RefreshController(initialRefresh: false);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        primary: true,
        appBar: SearchableAppBar(
          isHome: true,
        ),
        body: BlocListener<ActivityBloc, ActivityState>(
            listener: (context, state) {
              if (state is ActivityInitial) {
                context.read<ActivityBloc>().add(
                      GetActivity(),
                    );
              }
            },
            child: BlocListener<DonationBloc, DonationState>(
              listener: (context, state) {
                if (state is DonationInitial) {
                  context.read<DonationBloc>().add(
                        GetDonation(null, null),
                      );
                }
              },
              child: SmartRefresher(
                controller: _refreshController,
                onRefresh: () {
                  context.read<ActivityBloc>().add(
                        ResetActivity(),
                      );
                  context.read<DonationBloc>().add(
                        ResetDonation(),
                      );
                  _refreshController.refreshCompleted();
                },
                child: SingleChildScrollView(
                  child: HomeBody(),
                ),
              ),
            )
            // body: CustomScrollView(
            //   slivers: [
            //     CollapsableAppBar(),
            //     SliverList(
            //       delegate: SliverChildListDelegate.fixed([
            //         _buildBody(),
            //       ]),
            //     )
            //   ],
            // ),
            ));
  }
}
