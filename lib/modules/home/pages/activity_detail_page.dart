import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:maghfirah_donation/components/components.dart';
import 'package:maghfirah_donation/shared/shared.dart';
import 'package:pinch_zoom/pinch_zoom.dart';
import 'package:share/share.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../modules.dart';

class ActivityDetailPage extends StatelessWidget {
  final Activity activity;

  const ActivityDetailPage({Key key, this.activity}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: SearchableAppBar(
        onForceBack: () {
          Navigator.pop(context);
        },
        actions: [
          Padding(
            padding: const EdgeInsets.only(top: Dimens.dp10, right: Dimens.dp6),
            child: IconButton(
                icon: Icon(
                  Icons.share,
                  size: 28,
                ),
                onPressed: () {
                  Share.share(Config.endpoint + "/activity/${activity.id}");
                }),
          )
        ],
      ),
      bottomSheet: Padding(
        padding: const EdgeInsets.all(Dimens.dp14),
        child: DefaultButtonMol(
          onClick: () async {
            if(activity.link==null)return;
            if (await canLaunch(activity.link)) {
              await launch(activity.link);
            } else {
              print("error");
            }
          },
          color: Colors.blue,
          hasBorderSide: false,
          text: "Join a Meeting",
          whiteMode: false,
        ),
      ),
      body: ListView(
        children: [
          InkWell(
            onTap: () {
              showDialog(
                  context: context,
                  barrierColor: Colors.black.withOpacity(0.8),
                  builder: (context) => Dialog(
                        insetPadding: EdgeInsets.zero,
                        backgroundColor: Colors.black,
                        child: Container(
                          color: Colors.black,
                          height: Dimens.height(context) * .5,
                          width: Dimens.width(context) * .95,
                          child: PinchZoom(
                            image: ImageURLAtm(
                              radius: 0,
                              hasBorder: false,
                              width: double.maxFinite,
                              height: null,
                              fit: BoxFit.fitWidth,
                              imageUrl: activity.path,
                            ),
                            zoomedBackgroundColor:
                                Colors.black.withOpacity(0.8),
                            resetDuration: const Duration(milliseconds: 100),
                            maxScale: 2.5,
                          ),
                        ),
                      ));
            },
            child: ImageURLAtm(
              radius: 0,
              hasBorder: false,
              width: double.maxFinite,
              height: null,
              imageUrl: activity.path,
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(
                left: Dimens.dp14,
                top: Dimens.dp20,
                bottom: Dimens.dp4,
                right: Dimens.dp14),
            child: H1Atm(
              activity.title,
              maxLine: 4,
              overflow: TextOverflow.ellipsis,
              style: TextStyle(
                  fontWeight: FontWeight.w700,
                  height: 1.1,
                  letterSpacing: 0.8,
                  color: Color(0xFF353535)),
            ),
          ),
          DefaultDividerAtm(
            verticalPadding: 20,
            thickness: 6,
          ),
          Padding(
            padding:
                const EdgeInsets.only(left: Dimens.dp14, bottom: Dimens.dp4),
            child: H2Atm(
              "Informasi Kegiatan",
              style: TextStyle(
                  fontWeight: FontWeight.w700,
                  letterSpacing: 0.8,
                  color: Color(0xFF353535)),
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(
                horizontal: Dimens.dp16, vertical: Dimens.dp12),
            child: Column(
              children: [
                Utils.buildRow(
                  "Tanggal",
                  DateFormat("EEEE, dd MMMM yyyy").format(
                    activity.activityDate,
                  ),
                ),
                SizedBox(
                  height: Dimens.dp12,
                ),
                Utils.buildRow(
                  "Jam",
                  activity.activityTime,
                ),
                SizedBox(
                  height: Dimens.dp12,
                ),
                Utils.buildRow(
                  "Lokasi",
                  activity.place,
                ),
              ],
            ),
          ),
          SizedBox(
            height: 150,
          )
        ],
      ),
    );
  }
}
