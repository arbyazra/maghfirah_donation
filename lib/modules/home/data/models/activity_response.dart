// To parse this JSON data, do
//
//     final activityResponse = activityResponseFromJson(jsonString);

import 'dart:convert';

ActivityResponse activityResponseFromJson(String str) => ActivityResponse.fromJson(json.decode(str));

String activityResponseToJson(ActivityResponse data) => json.encode(data.toJson());

class ActivityResponse {
    ActivityResponse({
        this.success,
        this.message,
        this.data,
    });

    bool success;
    String message;
    List<Activity> data;

    ActivityResponse copyWith({
        bool success,
        String message,
        List<Activity> data,
    }) => 
        ActivityResponse(
            success: success ?? this.success,
            message: message ?? this.message,
            data: data ?? this.data,
        );

    factory ActivityResponse.fromJson(Map<String, dynamic> json) => ActivityResponse(
        success: json["success"] == null ? null : json["success"],
        message: json["message"] == null ? null : json["message"],
        data: json["data"] == null ? null : List<Activity>.from(json["data"].map((x) => Activity.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "success": success == null ? null : success,
        "message": message == null ? null : message,
        "data": data == null ? null : List<dynamic>.from(data.map((x) => x.toJson())),
    };
}

class Activity {
    Activity({
        this.id,
        this.title,
        this.path,
        this.activityDate,
        this.activityTime,
        this.content,
        this.place,
        this.link,
        this.createdAt,
        this.updatedAt,
    });

    int id;
    String title;
    String path;
    DateTime activityDate;
    String activityTime;
    String content;
    String place;
    dynamic link;
    DateTime createdAt;
    DateTime updatedAt;

    Activity copyWith({
        int id,
        String title,
        String path,
        DateTime activityDate,
        String activityTime,
        String content,
        String place,
        dynamic link,
        DateTime createdAt,
        DateTime updatedAt,
    }) => 
        Activity(
            id: id ?? this.id,
            title: title ?? this.title,
            path: path ?? this.path,
            activityDate: activityDate ?? this.activityDate,
            activityTime: activityTime ?? this.activityTime,
            content: content ?? this.content,
            place: place ?? this.place,
            link: link ?? this.link,
            createdAt: createdAt ?? this.createdAt,
            updatedAt: updatedAt ?? this.updatedAt,
        );

    factory Activity.fromJson(Map<String, dynamic> json) => Activity(
        id: json["id"] == null ? null : json["id"],
        title: json["title"] == null ? null : json["title"],
        path: json["path"] == null ? null : json["path"],
        activityDate: json["activity_date"] == null ? null : DateTime.parse(json["activity_date"]),
        activityTime: json["activity_time"] == null ? null : json["activity_time"],
        content: json["content"] == null ? null : json["content"],
        place: json["place"] == null ? null : json["place"],
        link: json["link"],
        createdAt: json["created_at"] == null ? null : DateTime.parse(json["created_at"]),
        updatedAt: json["updated_at"] == null ? null : DateTime.parse(json["updated_at"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "title": title == null ? null : title,
        "path": path == null ? null : path,
        "activity_date": activityDate == null ? null : "${activityDate.year.toString().padLeft(4, '0')}-${activityDate.month.toString().padLeft(2, '0')}-${activityDate.day.toString().padLeft(2, '0')}",
        "activity_time": activityTime == null ? null : activityTime,
        "content": content == null ? null : content,
        "place": place == null ? null : place,
        "link": link,
        "created_at": createdAt == null ? null : createdAt.toIso8601String(),
        "updated_at": updatedAt == null ? null : updatedAt.toIso8601String(),
    };
}
