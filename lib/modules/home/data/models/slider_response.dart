// To parse this JSON data, do
//
//     final sliderResponse = sliderResponseFromJson(jsonString);

import 'dart:convert';

SliderResponse sliderResponseFromJson(String str) => SliderResponse.fromJson(json.decode(str));

String sliderResponseToJson(SliderResponse data) => json.encode(data.toJson());

class SliderResponse {
    SliderResponse({
        this.success,
        this.message,
        this.data,
    });

    bool success;
    String message;
    List<Slider> data;

    SliderResponse copyWith({
        bool success,
        String message,
        List<Slider> data,
    }) => 
        SliderResponse(
            success: success ?? this.success,
            message: message ?? this.message,
            data: data ?? this.data,
        );

    factory SliderResponse.fromJson(Map<String, dynamic> json) => SliderResponse(
        success: json["success"] == null ? null : json["success"],
        message: json["message"] == null ? null : json["message"],
        data: json["data"] == null ? null : List<Slider>.from(json["data"].map((x) => Slider.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "success": success == null ? null : success,
        "message": message == null ? null : message,
        "data": data == null ? null : List<dynamic>.from(data.map((x) => x.toJson())),
    };
}

class Slider {
    Slider({
        this.id,
        this.pathSlider,
        this.linkTarget,
        this.createdAt,
        this.updatedAt,
        this.position,
    });

    int id;
    String pathSlider;
    String linkTarget;
    DateTime createdAt;
    DateTime updatedAt;
    String position;

    Slider copyWith({
        int id,
        String pathSlider,
        String linkTarget,
        DateTime createdAt,
        DateTime updatedAt,
        String position,
    }) => 
        Slider(
            id: id ?? this.id,
            pathSlider: pathSlider ?? this.pathSlider,
            linkTarget: linkTarget ?? this.linkTarget,
            createdAt: createdAt ?? this.createdAt,
            updatedAt: updatedAt ?? this.updatedAt,
            position: position ?? this.position,
        );

    factory Slider.fromJson(Map<String, dynamic> json) => Slider(
        id: json["id"] == null ? null : json["id"],
        pathSlider: json["path_slider"] == null ? null : json["path_slider"],
        linkTarget: json["link_target"] == null ? null : json["link_target"],
        createdAt: json["created_at"] == null ? null : DateTime.parse(json["created_at"]),
        updatedAt: json["updated_at"] == null ? null : DateTime.parse(json["updated_at"]),
        position: json["position"] == null ? null : json["position"],
    );

    Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "path_slider": pathSlider == null ? null : pathSlider,
        "link_target": linkTarget == null ? null : linkTarget,
        "created_at": createdAt == null ? null : createdAt.toIso8601String(),
        "updated_at": updatedAt == null ? null : updatedAt.toIso8601String(),
        "position": position == null ? null : position,
    };
}
