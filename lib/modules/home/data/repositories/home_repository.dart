import 'package:maghfirah_donation/shared/shared.dart';

import '../../../modules.dart';

class HomeRepository {
  final Network network;

  HomeRepository({this.network});

  Future<ActivityResponse> getActivity() async {
    var url = parseUrl("/activities");
    final response = await network.getRequest(url,);
    if (response.statusCode >= 200 && response.statusCode < 300) {
      return activityResponseFromJson(response.body);
    } else {
      print(response.body);
      return activityResponseFromJson(response.body);
    }
  }

  Future<SliderResponse> getSliders({pos='top'}) async {
    var url = parseUrl("/sliders?position=$pos");
    final response = await network.getRequest(url,);
    if (response.statusCode >= 200 && response.statusCode < 300) {
      print(response.body);
      return sliderResponseFromJson(response.body);
    } else {
      print(response.body);
      return sliderResponseFromJson(response.body);
    }
  }


}
