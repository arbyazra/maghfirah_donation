import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:formz/formz.dart';
import '../../../modules.dart';

part 'register_state.dart';

class RegisterCubit extends Cubit<RegisterState> {
  RegisterCubit({this.repository}) : super(RegisterState());

  final AuthRepository repository;
  RegisterRequest registerRequest = RegisterRequest();
  String msg;
  List<FormzInput> fieldz(FormzInput xs) => xs == null
      ? [
          state.email,
          state.name,
          state.password,
          state.passwordVerification,
          state.phoneNumber,
        ].toList()
      : [
          state.email,
          state.name,
          state.password,
          state.passwordVerification,
          state.phoneNumber,
        ].where((element) {
          if (element.hashCode == xs.hashCode) {
            element = xs;
            return true;
          } else {
            return true;
          }
        }).toList();

  void onNameChanged(String v) {
    final value = Name.dirty(v);
    registerRequest.name = value.value;
    emit(state.copyWith(name: value, status: Formz.validate(fieldz(value))));
  }

  void onPhoneNumberChanged(String v) {
    final value = PhoneNumber.dirty(v);

    emit(state.copyWith(
        phoneNumber: value, status: Formz.validate(fieldz(value))));
  }

  void onEmailChanged(String v) {
    final value = Email.dirty(v);
    registerRequest.email = value.value;
    emit(state.copyWith(email: value, status: Formz.validate(fieldz(value))));
  }

  void togglePasswordView() {
    emit(state.copyWith(
        isPasswordShown: !state.isPasswordShown,
        status: Formz.validate(fieldz(null))));
  }

  void onPasswordChanged(String v) {
    final value = Password.dirty(v);
    registerRequest.password = value.value;
    emit(
        state.copyWith(password: value, status: Formz.validate(fieldz(value))));
  }

  void onPasswordVerificationChanged(String v) {
    final value = Password.dirty(v);
    emit(state.copyWith(
        passwordVerification: value, status: Formz.validate(fieldz(value))));
  }

  void onRegisterTap() async {
    emit(state.copyWith(status: FormzStatus.submissionInProgress));
    try {
      var result = await repository.register(registerRequest);
      if (result.success) {
        print("success");
        emit(state.copyWith(status: FormzStatus.submissionSuccess));
        emit(state.copyWith(status: FormzStatus.valid));
      } else {
        msg=result.message;
        print("ERROR " + result.message);
        emit(state.copyWith(status: FormzStatus.submissionFailure));
        emit(state.copyWith(status: FormzStatus.valid));
      }
    } catch (e) {
      print('error');
      print(e);
      emit(state.copyWith(status: FormzStatus.submissionFailure));
    }
  }
}
