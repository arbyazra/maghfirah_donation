part of 'register_cubit.dart';

class RegisterState extends Equatable {
   RegisterState({
    this.name = const Name.pure(),
    this.email = const Email.pure(),
    this.phoneNumber = const PhoneNumber.pure(),
    this.password = const Password.pure(),
    this.passwordVerification = const Password.pure(),
    this.status = FormzStatus.pure,
    this.isPasswordShown = false, 
  });

  final Name name;
  final Email email;
  final PhoneNumber phoneNumber;
  final Password password;
  final Password passwordVerification;
  final FormzStatus status;
  final bool isPasswordShown;

  @override
  List<Object> get props =>
      [name, email, phoneNumber, password, passwordVerification, status,isPasswordShown];

  RegisterState copyWith({
    Name name,
    Email email,
    PhoneNumber phoneNumber,
    Password password,
    Password passwordVerification,
    FormzStatus status,
    bool isPasswordShown,
  }) {
    return RegisterState(
      name: name ?? this.name,
      email: email ?? this.email,
      phoneNumber: phoneNumber ?? this.phoneNumber,
      password: password ?? this.password,
      passwordVerification: passwordVerification ?? this.passwordVerification,
      status: status ?? this.status,
      isPasswordShown: isPasswordShown ?? this.isPasswordShown,
    );
  }
}
