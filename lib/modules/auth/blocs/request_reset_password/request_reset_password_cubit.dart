import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:formz/formz.dart';
import 'package:maghfirah_donation/modules/modules.dart';
import 'package:maghfirah_donation/shared/network/http_request.dart';

part 'request_reset_password_state.dart';

class RequestResetPasswordCubit extends Cubit<RequestResetPasswordState> {
  RequestResetPasswordCubit()
      : super(RequestResetPasswordState());
  String msg;
  void onEmailChanged(String v) {
    var email = Email.dirty(v);
    emit(state.copyWith(email: email, status: Formz.validate([email])));
  }

  void requestResetPassword() async {
    emit(state.copyWith(status: FormzStatus.submissionInProgress));
    try {
      var result = await AuthRepository(network: Network())
          .requestResetPassword(state.email.value);
      // print(result.toJson());
      if (result.success) {
        print("success");
        emit(state.copyWith(status: FormzStatus.submissionSuccess));
      } else {
        msg = result.message;
        print(result.message);
        emit(RequestResetPasswordState(status: FormzStatus.submissionFailure));
      }
    } catch (e) {
      print(e);
      msg = e.toString();
      emit(state.copyWith(status: FormzStatus.submissionFailure));
    }
  }
}
