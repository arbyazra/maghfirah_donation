part of 'request_reset_password_cubit.dart';

class RequestResetPasswordState extends Equatable {
  final Email email;
  final FormzStatus status;

  RequestResetPasswordState({
    this.email = const Email.pure(),
    this.status = FormzStatus.pure,
  });

  copyWith({
    email,
    status,
  }) =>
      RequestResetPasswordState(
          email: email ?? this.email, status: status ?? this.status);

  @override
  List<Object> get props => [email, status];
}
