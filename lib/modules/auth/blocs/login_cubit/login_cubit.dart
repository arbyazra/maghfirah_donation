import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:formz/formz.dart';

import '../../../modules.dart';

part 'login_state.dart';

class LoginCubit extends Cubit<LoginState> {
  LoginCubit({this.repository}) : super(LoginState());
  final AuthRepository repository;
  String emailReq;
  String passwordReq;
  String msg;
  void onEmailChanged(String v) {
    final email = Email.dirty(v);
    emailReq= email.value;
    emit(state.copyWith(
        email: email, status: Formz.validate([email, state.password])));
  }

  void togglePasswordView() {
    emit(state.copyWith(
        status: Formz.validate([state.email, state.password]),
        isPasswordShown: !state.isPasswordShown));
  }

  void onPasswordChanged(String v) {
    final password = Password.dirty(v);
    passwordReq=password.value;
    emit(state.copyWith(
        password: password, status: Formz.validate([state.email, password])));
  }

  void login() async {
    emit(state.copyWith(status: FormzStatus.submissionInProgress));
    try {
      var result = await repository.login(
        name: emailReq,
        password: passwordReq,
      );
      // print(result.toJson());
      if (result.success) {
        print("success");
        await repository.setUser(result);
        emit(state.copyWith(status: FormzStatus.submissionSuccess));
        emit(state.copyWith(status: FormzStatus.valid));
      } else {
        msg=result.message;
        print(result.message);
        emit(LoginState(status: FormzStatus.submissionFailure));
        emit(LoginState(status: FormzStatus.valid));
      }
    } catch (e) {
      print(e);
      emit(state.copyWith(status: FormzStatus.submissionFailure));
      emit(LoginState(status: FormzStatus.valid));
    }
  }
}
