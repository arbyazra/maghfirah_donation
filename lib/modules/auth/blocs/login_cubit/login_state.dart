part of 'login_cubit.dart';


class LoginState extends Equatable {
  const LoginState({
    this.email = const Email.pure(),
    this.password = const Password.pure(),
    this.status = FormzStatus.pure,
    this.isPasswordShown = false, 
  });

  final Email email;
  final Password password;
  final bool isPasswordShown;
  final FormzStatus status;

  @override
  List<Object> get props => [email, password, status,isPasswordShown];

  LoginState copyWith({
    Email email,
    Password password,
    FormzStatus status,
    bool isPasswordShown,
  }) {
    return LoginState(
      email: email ?? this.email,
      password: password ?? this.password,
      status: status ?? this.status,
      isPasswordShown: isPasswordShown ?? this.isPasswordShown,
    );
  }
}