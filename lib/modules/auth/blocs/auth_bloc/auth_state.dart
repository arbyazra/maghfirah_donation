part of 'auth_bloc.dart';

abstract class AuthState extends Equatable {
  const AuthState();
  
  @override
  List<Object> get props => [];
}

class AuthInitial extends AuthState {}
class Authenticated extends AuthState {}
class Unauthenticated extends AuthState {
  final bool hasIntroPassed;

  Unauthenticated(this.hasIntroPassed);
  @override
  List<Object> get props => [hasIntroPassed];
}
class ErrorAuth extends AuthState {}
