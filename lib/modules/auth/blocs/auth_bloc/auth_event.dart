part of 'auth_bloc.dart';

abstract class AuthEvent extends Equatable {
  const AuthEvent();

  @override
  List<Object> get props => [];
}

class AppStarted extends AuthEvent{}
class LoggedIn extends AuthEvent{}
class HasPassedIntro extends AuthEvent{}
class LoggedOut extends AuthEvent{
  final bool reInitToken;

  LoggedOut({this.reInitToken=true});
}
class IsLoggedIn extends AuthEvent{}