import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:maghfirah_donation/modules/modules.dart';
import 'package:maghfirah_donation/shared/shared.dart';

part 'auth_event.dart';
part 'auth_state.dart';

class AuthBloc extends Bloc<AuthEvent, AuthState> {
  AuthBloc({this.repository}) : super(AuthInitial());
  final AuthRepository repository;
  @override
  Stream<AuthState> mapEventToState(AuthEvent event) async* {
    if (event is AppStarted) {
      yield* _mapAppStartedToState();
    } else if (event is LoggedIn) {
      yield* _mapLoggedInToState();
    } else if (event is LoggedOut) {
      yield* _mapLoggedOutToState(event);
    } else if (event is HasPassedIntro) {
      yield* _mapHasPassedIntroToState();
    }
  }

  Stream<AuthState> _mapAppStartedToState() async* {
    var hasPassedIntro = await repository.getUserPassedIntro();
    try {
      yield Authenticated();
      final isSignedIn = await repository.isSignedIn();
      if (isSignedIn) {
        yield Authenticated();
      } else {
        yield Unauthenticated(hasPassedIntro);
      }
    } catch (_) {
      yield Unauthenticated(hasPassedIntro);
    }
  }

  Stream<AuthState> _mapLoggedInToState() async* {
    try {
      await repository.setUserLogin();
      yield Authenticated();
    } catch (e) {
      yield ErrorAuth();
    }
  }

  Stream<AuthState> _mapLoggedOutToState(LoggedOut event) async* {
    var hasPassedIntro = await repository.getUserPassedIntro();
    try {
      // await repository.logout();
      await repository.signOut();
      yield Unauthenticated(hasPassedIntro);
    } catch (e) {
      yield ErrorAuth();
    }
  }

  Stream<AuthState> _mapHasPassedIntroToState() async* {
    await Cache.setCache(key: CACHE_INTRO, data: "true");
    yield Unauthenticated(true);
  }
}
