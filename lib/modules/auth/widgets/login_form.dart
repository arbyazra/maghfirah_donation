import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:maghfirah_donation/components/components.dart';
import 'package:maghfirah_donation/modules/modules.dart';
import 'package:formz/formz.dart';
import '../../../shared/shared.dart';

class LoginForm extends StatefulWidget {
  final GlobalKey<ScaffoldState> scaffoldKey;

  const LoginForm({
    Key key,
    this.scaffoldKey,
  }) : super(key: key);

  @override
  _LoginTempState createState() => _LoginTempState();
}

class _LoginTempState extends State<LoginForm> {
  LoginCubit _bloc;
  LoginState _state;

  @override
  void initState() {
    super.initState();
    _bloc = context.read<LoginCubit>();
  }

  String emailErrorText(LoginState state) =>
      state.email.valid ? null : Strings.emailErrorText;

  String passwordErrorText(LoginState state) =>
      state.password.valid ? null : Strings.passwordErrorText;

  void _onLoginTap() async {
    if (!_state.status.isValid) {
      widget.scaffoldKey.currentState.showSnackBar(SnackBar(
        content: H3Atm("Cek kolom yang belum Anda isi"),
      ));
      return;
    }
    _bloc.login();
  }

  void _navigateToRegisterPage() {
    Navigator.push(context, RegisterPage().route());
  }

  void _navigateToResetPasswordPage() {
    Navigator.push(context, ResetPasswordPage().route());
  }

  @override
  Widget build(BuildContext context) {
    return ListView(
      padding: EdgeInsets.only(
          top: Dimens.dp32,
          left: Dimens.dp18,
          right: Dimens.dp18,
          bottom: Dimens.dp40),
      children: [
        SizedBox(
          height: 76,
          child: Image.asset(
            AssetPaths.logoWithName,
          ),
        ),
        SizedBox(height: Dimens.dp10,),
        H1Atm(
          Strings.loginSlogan,
          align: TextAlign.center,
          style: TextStyle(
            fontWeight: FontWeight.w500,
          ),
        ),
        SizedBox(
          height: Dimens.dp32,
        ),
        _buildFields(),
        SizedBox(
          height: Dimens.dp32,
        ),
        Align(
          alignment: Alignment.centerRight,
          child: InkWell(
            child: H3Atm(
              "Lupa password?",
              style: TextStyle(color: Colors.grey[400]),
            ),
            onTap: _navigateToResetPasswordPage,
          ),
        ),
        SizedBox(
          height: Dimens.dp40,
        ),
        AuthBottomButtons(
          question: Strings.loginAskForRegister,
          answer: Strings.register,
          primaryButtonText: Strings.login,
          onPrimaryButtonTap: _onLoginTap,
          onAnswerTap: _navigateToRegisterPage,
        )
      ],
    );
  }

  Widget _buildFields() {
    return BlocBuilder<LoginCubit, LoginState>(
      // buildWhen: (previous, current) => previous.email != current.email,
      builder: (context, state) {
        _state = state;
        return Column(
          children: [
            DefaultTextFieldAtm(
              errorText: emailErrorText(state),
              onChanged: (v) => _bloc.onEmailChanged(v),
              labelText: Strings.fieldEmailOrPhoneNumber,
            ),
            SizedBox(
              height: Dimens.dp24,
            ),
            DefaultTextFieldAtm(
              errorText: passwordErrorText(state),
              onChanged: (v) => _bloc.onPasswordChanged(v),
              suffixIcon: IconButton(
                icon: Icon(!state.isPasswordShown
                    ? Icons.visibility_off
                    : Icons.visibility),
                onPressed: () => _bloc.togglePasswordView(),
              ),
              isObscure: !state.isPasswordShown,
              labelText: Strings.fieldPassword,
            ),
          ],
        );
      },
    );
  }
}
