import 'package:flutter/material.dart';
import 'package:maghfirah_donation/components/components.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../../shared/shared.dart';
import '../../modules.dart';
import 'package:formz/formz.dart';

class RequestResetPasswordForm extends StatefulWidget {
  final GlobalKey<ScaffoldState> scaffoldKey;

  const RequestResetPasswordForm({Key key, this.scaffoldKey}) : super(key: key);
  @override
  _ResetPasswordTempState createState() => _ResetPasswordTempState();
}

class _ResetPasswordTempState extends State<RequestResetPasswordForm> {
  void _onResetPasswordSent(RequestResetPasswordState state) async {
    if (!state.status.isValidated) {
      widget.scaffoldKey.currentState.showSnackBar(SnackBar(
        content: H3Atm("Cek kolom yang belum Anda isi"),
      ));
      return;
    }
    context.read<RequestResetPasswordCubit>().requestResetPassword();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<RequestResetPasswordCubit, RequestResetPasswordState>(
      builder: (context, state) => ListView(
        padding: EdgeInsets.only(
            top: Dimens.dp32,
            left: Dimens.dp18,
            right: Dimens.dp18,
            bottom: Dimens.dp40),
        children: [
          H2Atm(
            Strings.resetPasswordSlogan,
            style: TextStyle(
                fontWeight: FontWeight.w500, letterSpacing: 0.1, height: 1.1),
          ),
          SizedBox(
            height: Dimens.dp40,
          ),
          DefaultTextFieldAtm(
            labelText: Strings.fieldEmailAddress,
            onChanged: (value) =>
                context.read<RequestResetPasswordCubit>().onEmailChanged(value),
            errorText: !state.email.invalid
                ? null
                : "Email tidak valid, cth : john@gmail.com",
          ),
          SizedBox(
            height: Dimens.dp40,
          ),
          AuthBottomButtons(
            question: Strings.resetPasswordAskForLogin,
            answer: Strings.login,
            primaryButtonText: Strings.sendResetPasswordLink,
            onPrimaryButtonTap: ()=>_onResetPasswordSent(state),
            onAnswerTap: () => Navigator.pop(context),
          )
        ],
      ),
    );
  }
}
