import 'package:flutter/material.dart';
import 'package:maghfirah_donation/components/components.dart';
import 'package:maghfirah_donation/shared/shared.dart';


class AuthBottomButtons extends StatelessWidget {
  final String primaryButtonText;
  final VoidCallback onAnswerTap;
  final VoidCallback onPrimaryButtonTap;
  final String question;
  final String answer;
  const AuthBottomButtons(
      {Key key,
      this.onAnswerTap,
      this.primaryButtonText,
      this.onPrimaryButtonTap,
      this.question, this.answer})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: Dimens.dp16),
          child: DefaultButtonMol(
            whiteMode: false,
            text: primaryButtonText,
            onClick:onPrimaryButtonTap,
          ),
        ),
        SizedBox(
          height: Dimens.dp32,
        ),
        Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            H3Atm(
              question,
              style: TextStyle(
                  fontWeight: FontWeight.w400, color: Colors.grey[600]),
            ),
            SizedBox(
              width: Dimens.dp4,
            ),
            InkWell(
              onTap: onAnswerTap,
              child: H3Atm(
                answer,
                style: TextStyle(
                  color: AppColors.primaryColor,
                  fontWeight: FontWeight.w400,
                ),
              ),
            )
          ],
        )
      ],
    );
  }
}
