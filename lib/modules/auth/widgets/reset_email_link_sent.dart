import 'package:flutter/material.dart';
import 'package:maghfirah_donation/components/components.dart';
import 'package:maghfirah_donation/shared/resources/resources.dart';
import 'package:maghfirah_donation/shared/shared.dart';

class ResetEmailLinkSent extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Center(
            child: Container(
              width: 100,
              height: 100,
              decoration: BoxDecoration(
                  color: Color(0xFFECEBF8),
                  borderRadius: BorderRadius.circular(Dimens.dp8)),
              padding: EdgeInsets.all(Dimens.dp10),
              child: Icon(
                MaghfirahIcon.email_sent,
                size: 50,
                color: AppColors.primaryColor,
              ),
            ),
          ),
          SizedBox(
            height: Dimens.dp32,
          ),
          H1Atm(
            Strings.checkYourEmail,
            style: TextStyle(
              fontWeight: FontWeight.w500,
            ),
          ),
          SizedBox(
            height: Dimens.dp10,
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: Dimens.dp10),
            child: H3Atm(
              Strings.resetPasswordTutorial,
              align: TextAlign.center,
              style: TextStyle(
                fontWeight: FontWeight.w400,
                color: AppColors.primaryColor,
              ),
            ),
          ),
          SizedBox(
            height: Dimens.dp32,
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: Dimens.dp20),
            child: DefaultButtonMol(
              whiteMode: false,
              onClick: () {},
              text: Strings.openWithEmailApp,
            ),
          )
        ],
      ),
    );
  }
}
