import 'package:flutter/material.dart';
import 'package:maghfirah_donation/components/components.dart';
import 'package:maghfirah_donation/shared/shared.dart';

class ResetPasswordForm extends StatefulWidget {
  @override
  _UpdatePasswordTempState createState() => _UpdatePasswordTempState();
}

class _UpdatePasswordTempState extends State<ResetPasswordForm> {
  @override
  Widget build(BuildContext context) {
    return ListView(
      padding: EdgeInsets.only(
          top: Dimens.dp32,
          left: Dimens.dp18,
          right: Dimens.dp18,
          bottom: Dimens.dp40),
      children: [
        H1Atm(
          Strings.resetPasswordWarning,
          align: TextAlign.center,
          style: TextStyle(
            fontWeight: FontWeight.w500,
          ),
        ),
        SizedBox(
          height: Dimens.dp40,
        ),
        _buildFields(),
        SizedBox(
          height: Dimens.dp40,
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: Dimens.dp16),
          child: DefaultButtonMol(
            whiteMode: false,
            text: Strings.resetPassword,
            onClick: () {},
          ),
        ),
      ],
    );
  }

  Widget _buildFields() {
    return Column(
      children: [
        DefaultTextFieldAtm(
          isObscure: true,
          labelText: Strings.fieldNewPassword,
        ),
        SizedBox(
          height: Dimens.dp24,
        ),
        DefaultTextFieldAtm(
          isObscure: true,
          labelText: Strings.fieldNewPasswordConf,
        ),
      ],
    );
  }
}
