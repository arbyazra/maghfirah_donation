import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:maghfirah_donation/components/components.dart';
import 'package:maghfirah_donation/modules/modules.dart';
import '../../../shared/shared.dart';
import 'package:formz/formz.dart';

class RegisterForm extends StatefulWidget {
  final GlobalKey<ScaffoldState> scaffoldKey;
  const RegisterForm({
    Key key,
    this.scaffoldKey,
  }) : super(key: key);

  @override
  _RegisterFormState createState() => _RegisterFormState();
}

class _RegisterFormState extends State<RegisterForm> {
  RegisterCubit _bloc;
  RegisterState _state;

  @override
  void initState() {
    super.initState();
    _bloc = context.read<RegisterCubit>();
  }

  @override
  Widget build(BuildContext context) {
    return _buildBody();
  }

  String _emailErrorText(RegisterState state) =>
      state.email.valid ? null : Strings.emailErrorText;

  String _nameErrorText(RegisterState state) =>
      state.name.valid ? null : Strings.nameErrorText;

  String _phoneNumberErrorText(RegisterState state) =>
      state.phoneNumber.valid ? null : Strings.phoneNumberErrorText;

  String _passwordErrorText(RegisterState state) =>
      state.password.valid ? null : Strings.passwordErrorText;

  String _passwordVerificationErrorText(RegisterState state) =>
      state.passwordVerification.valid &&
              (state.password.value == state.passwordVerification.value)
          ? null
          : "Password harus sama";

  void _navigateToLoginPage() {
    Navigator.pop(context);
  }

  void _onRegisterTap() {
    // print(_bloc.isFormValid);
    if (!_state.status.isValidated) {
      widget.scaffoldKey.currentState.showSnackBar(SnackBar(
        content: H3Atm("Cek kolom yang belum Anda isi"),
      ));
      return;
    }
    _bloc.onRegisterTap();
  }

  Widget _buildBody() {
    return ListView(
      padding: EdgeInsets.only(
          top: Dimens.dp32,
          left: Dimens.dp18,
          right: Dimens.dp18,
          bottom: Dimens.dp40),
      children: [
        SizedBox(
          height: 76,
          child: Image.asset(
            AssetPaths.logoWithName,
          ),
        ),
        SizedBox(height: Dimens.dp10,),
        H1Atm(
          Strings.registerSlogan,
          align: TextAlign.center,
          style: TextStyle(
            fontWeight: FontWeight.w500,
          ),
        ),
        SizedBox(
          height: Dimens.dp32,
        ),
        _buildFields(),
        SizedBox(
          height: Dimens.dp40,
        ),
        AuthBottomButtons(
          question: Strings.registerAskForLogin,
          answer: Strings.login,
          primaryButtonText: Strings.register,
          onPrimaryButtonTap: _onRegisterTap,
          onAnswerTap: _navigateToLoginPage,
        )
      ],
    );
  }

  Widget _buildFields() {
    return BlocBuilder<RegisterCubit, RegisterState>(
      builder: (context, state) {
        _state=state;
        return Column(
          children: [
            DefaultTextFieldAtm(
              labelText: Strings.fieldName,
              errorText: _nameErrorText(state),
              onChanged: (v) => _bloc.onNameChanged(v),
            ),
            SizedBox(
              height: Dimens.dp24,
            ),
            DefaultTextFieldAtm(
              keyboardType: TextInputType.phone,
              labelText: Strings.fieldPhoneNumber,
              onChanged: (v) => _bloc.onPhoneNumberChanged(v),
              errorText: _phoneNumberErrorText(state),
            ),
            SizedBox(
              height: Dimens.dp24,
            ),
            DefaultTextFieldAtm(
              keyboardType: TextInputType.emailAddress,
              labelText: Strings.fieldEmailAddress,
              onChanged: (v) => _bloc.onEmailChanged(v),
              errorText: _emailErrorText(state),
            ),
            SizedBox(
              height: Dimens.dp24,
            ),
            DefaultTextFieldAtm(
              labelText: Strings.fieldPassword,
              onChanged: (v) => _bloc.onPasswordChanged(v),
              suffixIcon: IconButton(
                icon: Icon(!state.isPasswordShown? Icons.visibility_off:Icons.visibility),
                onPressed: () => _bloc.togglePasswordView(),
              ),
              isObscure: !state.isPasswordShown,
              errorText: _passwordErrorText(state),
            ),
            SizedBox(
              height: Dimens.dp24,
            ),
            DefaultTextFieldAtm(
              labelText: Strings.fieldPasswordConf,
              onChanged: (v) => _bloc.onPasswordVerificationChanged(v),
              errorText: _passwordVerificationErrorText(state),
              suffixIcon: IconButton(
                icon: Icon(!state.isPasswordShown? Icons.visibility_off:Icons.visibility),
                onPressed: () => _bloc.togglePasswordView(),
              ),
              isObscure: !state.isPasswordShown,
            ),
          ],
        );
      },
    );
  }
}
