// To parse this JSON data, do
//
//     final registerRequest = registerRequestFromJson(jsonString);

import 'dart:convert';

RegisterRequest registerRequestFromJson(String str) => RegisterRequest.fromJson(json.decode(str));

String registerRequestToJson(RegisterRequest data) => json.encode(data.toJson());

class RegisterRequest {
    RegisterRequest({
        this.name,
        this.email,
        this.password,
    });

    String name;
    String email;
    String password;

    RegisterRequest copyWith({
        String name,
        String email,
        String password,
    }) => 
        RegisterRequest(
            name: name ?? this.name,
            email: email ?? this.email,
            password: password ?? this.password,
        );

    factory RegisterRequest.fromJson(Map<String, dynamic> json) => RegisterRequest(
        name: json["name"] == null ? null : json["name"],
        email: json["email"] == null ? null : json["email"],
        password: json["password"] == null ? null : json["password"],
    );

    Map<String, String> toJson() => {
        "name": name == null ? null : name,
        "email": email == null ? null : email,
        "password": password == null ? null : password,
    };
}
