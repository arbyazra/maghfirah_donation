// To parse this JSON data, do
//
//     final renewedTokenResponse = renewedTokenResponseFromJson(jsonString);

import 'dart:convert';

RenewedTokenResponse renewedTokenResponseFromJson(String str) => RenewedTokenResponse.fromJson(json.decode(str));

String renewedTokenResponseToJson(RenewedTokenResponse data) => json.encode(data.toJson());

class RenewedTokenResponse {
    RenewedTokenResponse({
        this.success,
        this.message,
        this.data,
    });

    bool success;
    String message;
    String data;

    RenewedTokenResponse copyWith({
        bool success,
        String message,
        String data,
    }) => 
        RenewedTokenResponse(
            success: success ?? this.success,
            message: message ?? this.message,
            data: data ?? this.data,
        );

    factory RenewedTokenResponse.fromJson(Map<String, dynamic> json) => RenewedTokenResponse(
        success: json["success"] == null ? null : json["success"],
        message: json["message"] == null ? null : json["message"],
        data: json["data"] == null ? null : json["data"],
    );

    Map<String, dynamic> toJson() => {
        "success": success == null ? null : success,
        "message": message == null ? null : message,
        "data": data == null ? null : data,
    };
}
