// To parse this JSON data, do
//
//     final requestResetPasswordResponse = requestResetPasswordResponseFromJson(jsonString);

import 'dart:convert';

RequestResetPasswordResponse requestResetPasswordResponseFromJson(String str) => RequestResetPasswordResponse.fromJson(json.decode(str));

String requestResetPasswordResponseToJson(RequestResetPasswordResponse data) => json.encode(data.toJson());

class RequestResetPasswordResponse {
    RequestResetPasswordResponse({
        this.success,
        this.message,
        this.data,
    });

    bool success;
    String message;
    String data;

    RequestResetPasswordResponse copyWith({
        bool success,
        String message,
        String data,
    }) => 
        RequestResetPasswordResponse(
            success: success ?? this.success,
            message: message ?? this.message,
            data: data ?? this.data,
        );

    factory RequestResetPasswordResponse.fromJson(Map<String, dynamic> json) => RequestResetPasswordResponse(
        success: json["success"] == null ? null : json["success"],
        message: json["message"] == null ? null : json["message"],
        data: json["data"] == null ? null : json["data"],
    );

    Map<String, dynamic> toJson() => {
        "success": success == null ? null : success,
        "message": message == null ? null : message,
        "data": data == null ? null : data,
    };
}
