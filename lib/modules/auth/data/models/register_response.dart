// To parse this JSON data, do
//
//     final registerResponse = registerResponseFromJson(jsonString);

import 'dart:convert';

RegisterResponse registerResponseFromJson(String str) => RegisterResponse.fromJson(json.decode(str));

String registerResponseToJson(RegisterResponse data) => json.encode(data.toJson());

class RegisterResponse {
    RegisterResponse({
        this.success,
        this.message,
        this.data,
    });

    bool success;
    String message;
    RegisteredUser data;

    RegisterResponse copyWith({
        bool success,
        String message,
        RegisteredUser data,
    }) => 
        RegisterResponse(
            success: success ?? this.success,
            message: message ?? this.message,
            data: data ?? this.data,
        );

    factory RegisterResponse.fromJson(Map<String, dynamic> json) => RegisterResponse(
        success: json["success"] == null ? null : json["success"],
        message: json["message"] == null ? null : json["message"],
        data: json["data"] == null ? null : RegisteredUser.fromJson(json["data"]),
    );

    Map<String, dynamic> toJson() => {
        "success": success == null ? null : success,
        "message": message == null ? null : message,
        "data": data == null ? null : data.toJson(),
    };
}

class RegisteredUser {
    RegisteredUser({
        this.name,
        this.email,
        this.level,
        this.updatedAt,
        this.createdAt,
        this.id,
    });

    String name;
    String email;
    String level;
    DateTime updatedAt;
    DateTime createdAt;
    int id;

    RegisteredUser copyWith({
        String name,
        String email,
        String level,
        DateTime updatedAt,
        DateTime createdAt,
        int id,
    }) => 
        RegisteredUser(
            name: name ?? this.name,
            email: email ?? this.email,
            level: level ?? this.level,
            updatedAt: updatedAt ?? this.updatedAt,
            createdAt: createdAt ?? this.createdAt,
            id: id ?? this.id,
        );

    factory RegisteredUser.fromJson(Map<String, dynamic> json) => RegisteredUser(
        name: json["name"] == null ? null : json["name"],
        email: json["email"] == null ? null : json["email"],
        level: json["level"] == null ? null : json["level"],
        updatedAt: json["updated_at"] == null ? null : DateTime.parse(json["updated_at"]),
        createdAt: json["created_at"] == null ? null : DateTime.parse(json["created_at"]),
        id: json["id"] == null ? null : json["id"],
    );

    Map<String, dynamic> toJson() => {
        "name": name == null ? null : name,
        "email": email == null ? null : email,
        "level": level == null ? null : level,
        "updated_at": updatedAt == null ? null : updatedAt.toIso8601String(),
        "created_at": createdAt == null ? null : createdAt.toIso8601String(),
        "id": id == null ? null : id,
    };
}
