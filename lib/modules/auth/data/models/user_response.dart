// To parse this JSON data, do
//
//     final userResponse = userResponseFromJson(jsonString);

import 'dart:convert';

UserResponse userResponseFromJson(String str) => UserResponse.fromJson(json.decode(str));

String userResponseToJson(UserResponse data) => json.encode(data.toJson());

class UserResponse {
    UserResponse({
        this.success,
        this.message,
        this.data,
    });

    bool success;
    String message;
    User data;

    UserResponse copyWith({
        bool success,
        String message,
        User data,
    }) => 
        UserResponse(
            success: success ?? this.success,
            message: message ?? this.message,
            data: data ?? this.data,
        );

    factory UserResponse.fromJson(Map<String, dynamic> json) => UserResponse(
        success: json["success"] == null ? null : json["success"],
        message: json["message"] == null ? null : json["message"],
        data: json["data"] == null ? null : User.fromJson(json["data"]),
    );

    Map<String, dynamic> toJson() => {
        "success": success == null ? null : success,
        "message": message == null ? null : message,
        "data": data == null ? null : data.toJson(),
    };
}

class User {
    User({
        this.id,
        this.name,
        this.email,
        this.emailVerifiedAt,
        this.level,
        this.pathFoto,
        this.createdAt,
        this.updatedAt,
        this.saldo,
        this.token,
    });

    int id;
    String name;
    String email;
    dynamic emailVerifiedAt;
    String level;
    dynamic pathFoto;
    DateTime createdAt;
    DateTime updatedAt;
    String saldo;
    String token;

    User copyWith({
        int id,
        String name,
        String email,
        dynamic emailVerifiedAt,
        String level,
        dynamic pathFoto,
        DateTime createdAt,
        DateTime updatedAt,
        int saldo,
        String token,
    }) => 
        User(
            id: id ?? this.id,
            name: name ?? this.name,
            email: email ?? this.email,
            emailVerifiedAt: emailVerifiedAt ?? this.emailVerifiedAt,
            level: level ?? this.level,
            pathFoto: pathFoto ?? this.pathFoto,
            createdAt: createdAt ?? this.createdAt,
            updatedAt: updatedAt ?? this.updatedAt,
            saldo: saldo ?? this.saldo,
            token: token ?? this.token,
        );

    factory User.fromJson(Map<String, dynamic> json) => User(
        id: json["id"] == null ? null : json["id"],
        name: json["name"] == null ? null : json["name"],
        email: json["email"] == null ? null : json["email"],
        emailVerifiedAt: json["email_verified_at"],
        level: json["level"] == null ? null : json["level"],
        pathFoto: json["path_foto"],
        createdAt: json["created_at"] == null ? null : DateTime.parse(json["created_at"]),
        updatedAt: json["updated_at"] == null ? null : DateTime.parse(json["updated_at"]),
        saldo: json["saldo"] == null ? null : json["saldo"],
        token: json["token"] == null ? null : json["token"],
    );

    Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "name": name == null ? null : name,
        "email": email == null ? null : email,
        "email_verified_at": emailVerifiedAt,
        "level": level == null ? null : level,
        "path_foto": pathFoto,
        "created_at": createdAt == null ? null : createdAt.toIso8601String(),
        "updated_at": updatedAt == null ? null : updatedAt.toIso8601String(),
        "saldo": saldo == null ? null : saldo,
        "token": token == null ? null : token,
    };
}
