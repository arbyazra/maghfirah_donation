import 'dart:convert';

import 'package:maghfirah_donation/modules/auth/data/models/renewed_token_response.dart';
import 'package:maghfirah_donation/modules/modules.dart';
import 'package:maghfirah_donation/shared/shared.dart';

class AuthRepository {
  final Network network;
  AuthRepository({this.network});

  Future<UserResponse> login({String name, String password}) async {
    var url = parseUrl("/login");
    var body = {
      "name": name,
      "password": password,
    };
    final response = await network.postRequest(url, body);
    if (response.statusCode >= 200 && response.statusCode < 300) {
      return userResponseFromJson(response.body);
    } else {
      print(response.body);
      return userResponseFromJson(response.body);
    }
  }

  Future<RegisterResponse> register(RegisterRequest data) async {
    var url = parseUrl("/register");
    final response = await network.postRequest(
      url,
      data.toJson(),
    );
    if (response.statusCode >= 200 && response.statusCode < 300) {
      return registerResponseFromJson(response.body);
    } else {
      print(response.body);
      return registerResponseFromJson(response.body);
    }
  }

  Future<RequestResetPasswordResponse> requestResetPassword(String data) async {
    var url = parseUrl("/forgot-password");
    final response = await network.postRequest(
      url,
      {"email": data},
    );
    if (response.statusCode >= 404) {
      return RequestResetPasswordResponse(
          success: false, message: "api not found");
    }
    if (response.statusCode >= 200 && response.statusCode < 300) {
      return requestResetPasswordResponseFromJson(response.body);
    } else {
      print(response.body);
      return requestResetPasswordResponseFromJson(response.body);
    }
  }

  Future<void> setUser(UserResponse user) async {
    await Cache.setCache(key: CACHE_TOKEN_ID, data: user.data.token);
    await Cache.setCache(key: CACHE_USER, data: jsonEncode(user.toJson()));
  }

  Future<bool> getUserPassedIntro() async {
    var result = await Cache.getCache(key: CACHE_INTRO);
    if (result == null) return false;
    if (result == "true")
      return true;
    else
      return false;
  }

  Future<bool> isSignedIn() async {
    // return (await Cache.getCache(key: LOGIN_STATUS)) != null;
    var user = userResponseFromJson(await Cache.getCache(key: CACHE_USER));
    return (await Cache.getCache(key: CACHE_TOKEN_ID) != null && user.data.email!=null);
  }

  Future<void> setUserPassedIntro() async {
    return await Cache.setCache(key: CACHE_INTRO, data: "true");
  }

  Future<void> setUserLogin() async {
    return await Cache.setCache(key: LOGIN_STATUS, data: "true");
  }

  Future<UserResponse> getUser() async {
    return userResponseFromJson(await Cache.getCache(key: CACHE_USER));
  }

  Future<void> signOut() async {
    return await Cache.removeCache(key: CACHE_TOKEN_ID);
    // return await Cache.removeCache(key: LOGIN_STATUS);
  }

  static Future<RenewedTokenResponse> renewToken(String name) async {
    var url = parseUrl("/token");
    final response = await Network().postRequest(url, {"credential": name});
    if (response.statusCode >= 200 && response.statusCode < 300) {
      print(response.body);
      return renewedTokenResponseFromJson(response.body);
    } else {
      print(response.body);
      return renewedTokenResponseFromJson(response.body);
    }
  }
}
