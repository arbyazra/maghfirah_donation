import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:maghfirah_donation/components/components.dart';
import 'package:maghfirah_donation/shared/shared.dart';
import 'package:formz/formz.dart';
import '../../modules.dart';

class RegisterPage extends StatelessWidget {
  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => RegisterCubit(
        repository: AuthRepository(
            network: Network(
        )),
      ),
      child: Scaffold(
        key: scaffoldKey,
        appBar: DefaultAppBarAtm(
          title: Strings.register,
        ),
        body: BlocListener<RegisterCubit, RegisterState>(
          listener: (context, state) {
            if (state.status.isSubmissionInProgress) {
              scaffoldKey.currentState
                  .showSnackBar(Utils.showLoadingSnackBar());
            }
            if (state.status.isSubmissionFailure) {
                  var msg = context.read<RegisterCubit>().msg;
                  scaffoldKey.currentState
                    ..hideCurrentSnackBar()
                    ..showSnackBar(SnackBar(
                      duration: Duration(seconds: 3),
                      content: H3Atm(
                        msg ?? "Suatu kesalahan telah terjadi, maaf",
                      ),
                    ));
                }
            if (state.status.isSubmissionSuccess) {
              scaffoldKey.currentState
                ..hideCurrentSnackBar()
                ..showSnackBar(SnackBar(
                  content: H3Atm(
                      "Berhasil Mendaftar!, mengalihkan ke halaman login..."),
                )).closed.then((value) {
                  Navigator.pop(context);
                });
            }
          },
          child: RegisterForm(
            scaffoldKey: scaffoldKey,
          ),
        ),
      ),
    );
  }
}
