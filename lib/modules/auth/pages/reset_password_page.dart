import 'package:flutter/material.dart';
import 'package:maghfirah_donation/components/components.dart';
import 'package:maghfirah_donation/modules/auth/widgets/reset_password_form.dart';
import 'package:maghfirah_donation/shared/shared.dart';

class ResetPasswordPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: DefaultAppBarAtm(
        title: Strings.newPassword,
      ),
      body: ResetPasswordForm(),
    );
  }
}
