import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:maghfirah_donation/components/components.dart';
import 'package:maghfirah_donation/modules/modules.dart';
import 'package:maghfirah_donation/shared/shared.dart';
import 'package:formz/formz.dart';

class ResetPasswordPage extends StatelessWidget {
  final GlobalKey<ScaffoldState> _scaffoldState = GlobalKey<ScaffoldState>();
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => RequestResetPasswordCubit(),
      child: Scaffold(
        key: _scaffoldState,
        appBar: DefaultAppBarAtm(
          title: Strings.register,
        ),
        body:
            BlocListener<RequestResetPasswordCubit, RequestResetPasswordState>(
          listener: (context, state) {
            if (state.status.isSubmissionInProgress) {
              _scaffoldState.currentState
                  .showSnackBar(Utils.showLoadingSnackBar());
            }
            if (state.status.isSubmissionFailure) {
              var msg = context.read<RequestResetPasswordCubit>().msg;
              _scaffoldState.currentState
                ..hideCurrentSnackBar()
                ..showSnackBar(SnackBar(
                  duration: Duration(seconds: 2),
                  content: H3Atm(
                    msg ?? "Suatu kesalahan telah terjadi, maaf",
                  ),
                ));
            }
            if (state.status.isSubmissionSuccess) {
              _scaffoldState.currentState
                ..hideCurrentSnackBar()
                ..showSnackBar(SnackBar(
                  content: H3Atm("Berhasil Mengirim Email!"),
                )).closed.then((value) {
                  Navigator.pop(context);
                });
            }
          },
          child: RequestResetPasswordForm(
            scaffoldKey: _scaffoldState,
          ),
        ),
      ),
    );
  }
}
