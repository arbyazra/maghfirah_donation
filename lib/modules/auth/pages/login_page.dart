import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:maghfirah_donation/components/components.dart';
import 'package:maghfirah_donation/shared/shared.dart';
import 'package:formz/formz.dart';
import '../../modules.dart';

class LoginPage extends StatelessWidget {
  final bool isFromHome;
  final bool isFromDonation;
  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();

  LoginPage({Key key, this.isFromHome = false, this.isFromDonation = false})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return BlocListener<AuthBloc, AuthState>(
      listener: (context, state) {
        if (state is Authenticated) {
          Navigator.pop(context);
          context.read<ProfileBloc>().add(GetProfile());
        }
      },
      child: BlocProvider(
        create: (context) => LoginCubit(
          repository: AuthRepository(network: Network()),
        ),
        child: WillPopScope(
          onWillPop: () async => _onBackPressed(context),
          child: Scaffold(
            key: scaffoldKey,
            appBar: DefaultAppBarAtm(
              title: Strings.login,
            ),
            body: BlocListener<LoginCubit, LoginState>(
              listener: (context, state) {
                if (state.status.isSubmissionInProgress) {
                  scaffoldKey.currentState
                      .showSnackBar(Utils.showLoadingSnackBar());
                }
                if (state.status.isSubmissionFailure) {
                  var msg = context.read<LoginCubit>().msg;
                  scaffoldKey.currentState
                    ..hideCurrentSnackBar()
                    ..showSnackBar(SnackBar(
                      duration: Duration(seconds: 2),
                      content: H3Atm(
                        msg ?? "Suatu kesalahan telah terjadi, maaf",
                      ),
                    ));
                }
                if (state.status.isSubmissionSuccess) {
                  scaffoldKey.currentState
                    ..hideCurrentSnackBar()
                    ..showSnackBar(SnackBar(
                      content: H3Atm("Berhasil Masuk!"),
                    )).closed.then((value) {
                      context.read<AuthBloc>().add(LoggedIn());
                    });
                }
              },
              child: LoginForm(
                scaffoldKey: scaffoldKey,
              ),
            ),
          ),
        ),
      ),
    );
  }

  Future<bool> _onBackPressed(BuildContext context,
      {bool forceBack = false}) async {
    if (!isFromDonation && !isFromHome) {
      Utils.removeStacksAndRenewMainPage(context);
      Navigator.pop(context);
      return false;
    } else {
      if (!forceBack || isFromHome) {
        Navigator.pop(context);
        Navigator.pop(context);
      } else {
        Navigator.pop(context);
      }
    }
    return false;
  }
}
