import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../modules.dart';
import '../screen_navigator.dart';

class MainPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ScreenNavigatorTemp(
      body: _buildBody(),
    );
  }

  Widget _buildBody() {
    return BlocBuilder<ScreenNavigationBloc, ScreenNavigationState>(
      builder: (context, state) {
        if (state is HomeScreen) {
          return HomePage();
        } else if (state is ZakatScreen) {
          return ZakatPage();
        } else if (state is DonationScreen) {
          return MyDonationPage();
        } else if (state is ProfileScreen) {
          return ProfilePage();
        } else {
          return Center(child: Text('Undefine Page'));
        }
      },
    );
  }
}
