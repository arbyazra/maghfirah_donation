import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:maghfirah_donation/components/components.dart';
import 'package:maghfirah_donation/shared/resources/maghfirah_icon_icons.dart';
import '../../../shared/shared.dart';
import '../../modules.dart';

class ScreenNavigatorTemp extends StatefulWidget {
  final Widget body;

  const ScreenNavigatorTemp({Key key, @required this.body}) : super(key: key);

  @override
  _ScreenNavigatorTempState createState() => _ScreenNavigatorTempState();
}

class _ScreenNavigatorTempState extends State<ScreenNavigatorTemp> {
  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  int _currentIndex = 0;
  ScreenNavigationBloc _bloc;
  @override
  void initState() {
    _bloc = BlocProvider.of<ScreenNavigationBloc>(context);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ScreenNavigationBloc, ScreenNavigationState>(
      builder: (context, state) {
        _setCurrentStateBottomNav(state);
        return Scaffold(
          key: _scaffoldKey,
          bottomNavigationBar: _buildBottomNav(),
          body: widget.body,
        );
      },
    );
  }

  Widget _buildBottomNav() {
    return BottomNavigationBar(
      items: [
        BottomNavigationBarItem(
          icon: Icon(MaghfirahIcon.home,size: 22),
          activeIcon: GradientIconAtm(icon: Icon(MaghfirahIcon.home,size: 22,)),
          label: Strings.home,
        ),
        BottomNavigationBarItem(
          icon: Icon(MaghfirahIcon.calculator,size: 20,),
          activeIcon: GradientIconAtm(icon: Icon(MaghfirahIcon.calculator,size: 20)),
          label: Strings.zakat,
        ),
        BottomNavigationBarItem(
          icon: Icon(MaghfirahIcon.gift,size: 20),
          activeIcon: GradientIconAtm(icon: Icon(MaghfirahIcon.gift,size: 20,)),
          label: Strings.donation,
        ),
        BottomNavigationBarItem(
          icon: Icon(MaghfirahIcon.account_circle,size: 22),
          activeIcon: GradientIconAtm(icon: Icon(MaghfirahIcon.account_circle,size: 22,)),
          label: Strings.me,

        ),
      ],
      type: BottomNavigationBarType.fixed,
      elevation: 12,
      showUnselectedLabels: true,
      backgroundColor: AppColors.appWhite,
      onTap: _onTapItem,
      currentIndex: _currentIndex,
      selectedItemColor: AppColors.primaryColor,
      unselectedItemColor: AppColors.greyDark,
      unselectedIconTheme: IconThemeData(size: Dimens.dp24),
    );
  }

  void _setCurrentStateBottomNav(ScreenNavigationState state) {
    if (state is HomeScreen) {
      _currentIndex = 0;
    } else if (state is ZakatScreen) {
      _currentIndex = 1;
    } else if (state is DonationScreen) {
      _currentIndex = 2;
    } else if (state is ProfileScreen) {
      _currentIndex = 3;
    }
  }

  void _onTapItem(int index) async {
    switch (index) {
      case 0:
        Utils.addNewStack(context, NavigateToHome());
        _bloc.add(NavigateToHome());
        break;
      case 1:
        Utils.addNewStack(context, NavigateToZakat());
        _bloc.add(NavigateToZakat());
        break;
      case 2:
        Utils.addNewStack(context, NavigateToDonation());
        _bloc.add(NavigateToDonation());
        break;
      case 3:
        Utils.addNewStack(context, NavigateToProfile());
        _bloc.add(NavigateToProfile());
        break;
      default:
    }
  }
}
