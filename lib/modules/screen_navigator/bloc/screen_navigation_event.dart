part of 'screen_navigation_bloc.dart';

@immutable
abstract class ScreenNavigationEvent {}

class NavigateToHome extends ScreenNavigationEvent{}
class NavigateToZakat extends ScreenNavigationEvent{}
class NavigateToDonation extends ScreenNavigationEvent{}
class NavigateToHelp extends ScreenNavigationEvent{}
class NavigateToProfile extends ScreenNavigationEvent{}