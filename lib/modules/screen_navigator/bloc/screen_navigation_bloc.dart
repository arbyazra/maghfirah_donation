import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';


part 'screen_navigation_event.dart';
part 'screen_navigation_state.dart';

class ScreenNavigationBloc
    extends Bloc<ScreenNavigationEvent, ScreenNavigationState> {
  ScreenNavigationBloc() : super(ScreenNavigationInitial());
  @override
  Stream<ScreenNavigationState> mapEventToState(
    ScreenNavigationEvent event,
  ) async* {
    
    if (event is NavigateToHome) {
      yield HomeScreen();
    } else if (event is NavigateToZakat) {
      yield ZakatScreen();
    } else if (event is NavigateToDonation) {
      yield DonationScreen();
    } else if (event is NavigateToProfile) {
      yield ProfileScreen();
    } else {
      yield ScreenNavigationHelp();
    }
  }
}
