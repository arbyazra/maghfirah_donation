part of 'screen_navigation_bloc.dart';

@immutable
abstract class ScreenNavigationState extends Equatable {
  @override
  List<Object> get props => [];
}

class ScreenNavigationInitial extends ScreenNavigationState{}
class HomeScreen extends ScreenNavigationState{}
class DonationScreen extends ScreenNavigationState{}
class ZakatScreen extends ScreenNavigationState{}
class ScreenNavigationHelp extends ScreenNavigationState{}
class ProfileScreen extends ScreenNavigationState{}
