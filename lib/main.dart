import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:jiffy/jiffy.dart';
import 'modules/modules.dart';
import 'setup.dart';
import 'shared/shared.dart';

void main() async {
  await AppSetup.setup();
  SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark.copyWith(
      statusBarColor: AppColors.primaryColor,
      statusBarIconBrightness: Brightness.dark,
      statusBarBrightness: Brightness.dark));
  await Jiffy.locale('id');
  Set<ScreenNavigationEvent> stacks = {};
  final Network network = Network();
  SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
    DeviceOrientation.portraitDown,
  ]).then(
    (value) => runApp(
      AppConfigProvider(
        stacks: stacks,
        network: network,
        child: MultiBlocProvider(
          providers: BlocProviders().getProviders(network),
          child: _App(),
        ),
      ),
    ),
  );
}

class _App extends StatelessWidget {
  _App({
    Key key,
  }) : super(key: key);
  final _navigatorKey = GlobalKey<NavigatorState>();
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "Amal Kampung Maghfirah",
      navigatorKey: _navigatorKey,
      theme: LightTheme.themeData(context),
      debugShowCheckedModeBanner: false,
      home: AppNavigator(),
    );
  }
}

class AppNavigator extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<AuthBloc, AuthState>(
      builder: (context, authState) =>
          BlocBuilder<ScreenNavigationBloc, ScreenNavigationState>(
        builder: (context, navState) {
          if (authState is Authenticated) {
            return MainPage();
          }
          if ((navState is DonationScreen || navState is ProfileScreen) &&
              (authState is Unauthenticated)) {
            Future.microtask(() => _navigateToLoginPage(context));
          }
          if (authState is Unauthenticated) {
            if (authState.hasIntroPassed) {
              return MainPage();
            } else {
              return IntroPage();
            }
          }

          return Scaffold(
            body: Center(
              child: CircularProgressIndicator(),
            ),
          );
        },
      ),
    );
  }

  void _navigateToLoginPage(BuildContext context) {
    Navigator.of(context).push(MaterialPageRoute(builder: (_) => LoginPage()));
  }
}
