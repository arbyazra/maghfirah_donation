import 'package:flutter/material.dart';

import 'shared.dart';

class LightTheme {
  static ThemeData themeData(BuildContext context) {
    return ThemeData(
      fontFamily: helveticaNeue,
    ).copyWith(
        primaryColor: AppColors.primaryColor,
        scaffoldBackgroundColor: AppColors.appWhite,
        accentColor: AppColors.primaryColor,
        toggleableActiveColor: AppColors.primaryColor,
        dividerColor: AppColors.appWhite,
        textTheme: Theme.of(context)
            .textTheme
            .apply(fontFamily: helveticaNeue, bodyColor: AppColors.textColor),
        bottomSheetTheme: BottomSheetThemeData(
            backgroundColor: AppColors.appWhite,
            elevation: 5,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.vertical(top: Radius.circular(5)))));

/*  static BottomNavigationBarThemeData bottomNavTheme =
      BottomNavigationBarThemeData(
    backgroundColor: AppColors.appWhite,
    type: BottomNavigationBarType.fixed,
    elevation: 12,
    unselectedItemColor: AppColors.blackD8Color,
    unselectedIconTheme: IconThemeData(size: Dimens.dp24),
  );*/
  }
}

class TextStylesTheme {
  static TextStyle appbarWhiteText = TextStyle(
    fontWeight: FontWeight.w700,
    color: AppColors.appWhite,
    fontSize: Dimens.dp16,
  );
  static TextStyle appbarBlackText = TextStyle(
    fontWeight: FontWeight.w500,
    color: AppColors.greyDark,
    fontSize: Dimens.dp14,
  );

  static TextStyle primaryText = TextStyle(
    fontWeight: FontWeight.w500,
    color: AppColors.secondaryColor,
    fontSize: Dimens.dp14,
  );

  static TextStyle titleText = TextStyle(
      fontWeight: FontWeight.w700,
      height: 1.1,
      letterSpacing: 0.8,);

  static TextStyle sectionTitleText = TextStyle(
      fontWeight: FontWeight.w700,
      height: 1.1,
      letterSpacing: 0.4);

  static TextStyle contentText = TextStyle(
    fontWeight: FontWeight.w300,
    height: 1.4,
    letterSpacing: 0.2,
  );
}
