
import 'package:http_interceptor/http_interceptor.dart';
import 'package:maghfirah_donation/modules/modules.dart';

import '../shared.dart';

class ExpiredTokenRetryPolicy extends RetryPolicy {
  @override
  Future<bool> shouldAttemptRetryOnResponse(ResponseData response) async {
    if (response.statusCode == 401) {
      print("REQUEST NEW TOKEN");
      try {
        final user =
            userResponseFromJson(await Cache.getCache(key: CACHE_USER));
        var res = await AuthRepository.renewToken(user.data.email);
        await Cache.setCache(key: CACHE_TOKEN_ID, data: res.data);
        print("Token renewed");
      } catch (e) {
        print(e);
        print("Failed to renew token");
      }
      return true;
    }

    return false;
  }
}
