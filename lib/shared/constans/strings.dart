class Strings {
  static String home = "Home";
  static String zakat = "Zakat";
  static String donation = "Sedekah Saya";
  static String me = "Saya";

  static String login = "Masuk";
  static String loginNow = "Masuk Sekarang";
  static String loginSlogan =
      "Masuk untuk nikmati mudahnya layanan Kampung Maghfirah";
  static String loginAskForRegister = "Belum punya akun Maghfirah?";

  static String register = "Daftar";
  static String registerSlogan = "Perjalanan kebaikan anda dimulai disini";
  static String registerAskForLogin = "Sudah punya akun?";

  static String fieldName = "Nama";
  static String fieldEmailOrPhoneNumber = "Email atau Nomor Ponsel";
  static String fieldPhoneNumber = "Nomor Ponsel";
  static String fieldEmailAddress = "Alamat Email";
  static String fieldPassword = "Password";
  static String fieldPasswordConf = "Konfirmasi Password";
  static String fieldNewPassword = "Password Baru";
  static String fieldOldPassword = "Password Lama";
  static String fieldNewPasswordConf = "Ulangi Password Baru";
  static String fieldZakatDeposit = "Deposito / Tabungan / Giro";
  static String fieldZakatGold = "Emas, perak, permata, atau sejenisnya";
  static String fieldZakatAsset =
      "Nilai properti & kendaraan (bukan yang digunakan sehari - hari)";
  static String fieldZakatStock =
      "Lainnya (saham, piutang, dan surat - surat berharga lainnya)";
  static String fieldZakatDebt =
      "Hutang pribadi yang jatuh tempo tahun ini";
  static String fieldDonationAmount = "Nominal";
  static String fieldPaymentMethod = "Metode Pembayaran";
  static String fieldDonorPrays = "Tulis doa untuk penggalang dana atau dirimu sendiri disini.";

  static String nominalErrorText = "Nominal wajib diisi!";
  static String paymentMethodErrorText = "Pilih metode pembayaran!";

  static String emailErrorText = "Email tidak valid, cth : john@gmail.com";
  static String nameErrorText = "Nama tidak boleh kosong";
  static String phoneNumberErrorText = "No. Ponsel tidak boleh kosong";
  static String passwordErrorText = "Password tidak boleh kosong";


  static String newPassword = "Password Baru";
  static String resetPassword = "Reset Password";
  static String resetPasswordWarning =
      "Kata sandi baru Anda harus berbeda dari kata sandi yang digunakan sebelumnya.";
  static String resetPasswordSlogan =
      "Masukkan email yang terdaftar. Kami akan mengirimkan link verifikasi untuk mengatur ulang kata sandi.";

  static String sendResetPasswordLink = "Kirim Link Reset Password";
  static String resetPasswordAskForLogin = "Ingat password anda?";
  static String openWithEmailApp = "Buka Aplikasi Email";
  static String checkYourEmail = "Cek Email Anda";
  static String resetPasswordTutorial =
      "Instruksi dan link verifikasi untuk mengatur ulang password anda sudah dikirim.";
  static String search = "Cari program sedekah";
  static String zakatCalculatorWarning =
      "Khusus untuk harta yang telah tersimpan selama lebih dari 1 tahun (haul) dan mencapai batas tertentu (nisab)";
  static String modifyNameWarning = "Nama dapat dilihat oleh pengguna lainnya. Pastikan nama sudah benar.";
  static String modifyPhoneNumberWarning = "Pastikan nomor handphone anda aktif. Kami akan mengirimkan kode verifikasi ke nomor handphone anda";
  static String modifyPasswordWarning = "Jika anda ingin mengubah password, silahkan ubah pada form berikut.";
  static String verificationCodeSentMsg = "Kami sudah mengirimkan kode verifikasi via SMS ke nomor ";
  
  static String loadingErrorText = "Ups, Ada kesalahan, coba refresh halaman ini";

  static String lorem =
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Porttitor dictumst elementum, convallis tortor eleifend. Lihat vestibulum aliquet amet eu in.Morbi eget mattis et, vitae dolor massa pellentesque ridiculus. Suscipit ac lacinia ultrices elit risus. Lorem ullamcorper quis volutpat faucibus porttitor tincidunt.";
}
