class AssetPaths{
  static const _imPath = "assets/images";

  static const logo = "$_imPath/logo_1.png";
  static const logoWithName = "$_imPath/logo.png";
  static const noAvailableImage = "$_imPath/no_avail_image.png";
  static const intro_one = "$_imPath/intro_1.png";
  static const intro_two = "$_imPath/intro_2.png";
  static const intro_three = "$_imPath/intro_3.png";
  static const banner = "$_imPath/banner.png";
  static const noItems = "$_imPath/no_items.png";
  static const noItemSpeaker = "$_imPath/no_items_2.png";
  static const pageNotFound = "$_imPath/not_found.png";
  static const messageSent = "$_imPath/message_sent.png";
  static const messageEmpty = "$_imPath/message_empty.png";
  static const delivered = "$_imPath/delivered.png";
  // static const zakat = "$_imPath/zakat.png";
}