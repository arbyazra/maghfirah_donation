import 'package:flutter/material.dart';
import 'package:maghfirah_donation/modules/modules.dart';
import 'package:maghfirah_donation/shared/shared.dart';

class AppConfigProvider extends InheritedWidget {
  const AppConfigProvider({
    Key key,
    @required this.stacks,
    this.network,
    this.current,
    @required Widget child,
  })  : assert(stacks != null),
        assert(child != null),
        super(key: key, child: child);

  final Set<ScreenNavigationEvent> stacks;
  final ScreenNavigationEvent current;
  final Network network;
  static AppConfigProvider of(BuildContext context) {
    return context.dependOnInheritedWidgetOfExactType<AppConfigProvider>();
  }

  @override
  bool updateShouldNotify(AppConfigProvider old) {
    return stacks != old.stacks;
  }
}
