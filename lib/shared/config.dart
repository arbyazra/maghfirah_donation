class Config {
  static const String endpoint = "https://kampungmaghfirah.id";
  // static const String endpoint = "https://maghfirah.digyta.net";
  static const String apiEndpoint = "$endpoint/api";
}

String parseUrl(String route) => Config.apiEndpoint + route;
