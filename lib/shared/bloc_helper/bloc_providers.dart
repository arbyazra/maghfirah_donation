import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:maghfirah_donation/modules/modules.dart';
import '../shared.dart';

class BlocProviders {
  getProviders(Network network) {
    final authRepository = AuthRepository(network: network);
    final activityaRepository = HomeRepository(network: network);
    final donationRepository = DonationRepository(network: network);
    final zakatRepository = ZakatRepository(network: network);
    final profileRepository = ProfileRepository(network: network);
    // final myDonationRepository = MyDonationRepository(network: network);
    return [
      BlocProvider(
        create: (context) => ScreenNavigationBloc()..add(NavigateToHome()),
      ),
      BlocProvider(
          create: (context) =>
              AuthBloc(repository: authRepository)..add(AppStarted())),
      BlocProvider(
        create: (context) => DonationBloc(repository: donationRepository),
      ),
      BlocProvider(
        create: (context) => DonationFilterCubit(),
      ),
      BlocProvider(
        create: (context) => ZakatCalculatorCubit(),
      ),
      BlocProvider(
        create: (context) => ActivityBloc(repository: activityaRepository),
      ),
      BlocProvider(
        create: (context) => DonationDetailBloc(repository: donationRepository),
      ),
      BlocProvider(
        create: (context) => PaymentMethodsBloc(repository: donationRepository),
      ),
      BlocProvider(
        create: (context) => DonatingCubit(repository: donationRepository),
      ),
      BlocProvider(
        create: (context) => ZakatKursBloc(repository: zakatRepository),
      ),
      BlocProvider(
        create: (context) => SliderBloc(repository: activityaRepository),
      ),
      BlocProvider(
        create: (context) => ProfileBloc(repository: profileRepository),
      ),
      BlocProvider(
        create: (context) => UserDonationBloc(repository: profileRepository),
      ),
      BlocProvider(
        create: (context) =>
            UpdateProfileCubit(repository: ProfileRepository(network: network)),
      ),
    ];
  }
}
