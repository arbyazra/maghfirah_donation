import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:maghfirah_donation/components/images/images.dart';
import 'package:maghfirah_donation/modules/modules.dart';
import 'package:maghfirah_donation/shared/shared.dart';

import '../components.dart';

class DefaultCarousels extends StatefulWidget {
  final bool isTop;
  final Function(int index, CarouselPageChangedReason reason) onPageChanged;
  final double height;
  final bool isHeader;
  const DefaultCarousels({
    Key key,
    this.onPageChanged,
    this.height = 300,
    this.isTop = true,
    this.isHeader = true,
  }) : super(key: key);

  @override
  _DefaultCarouselsState createState() => _DefaultCarouselsState();
}

class _DefaultCarouselsState extends State<DefaultCarousels> {
  int currentIndex = 0;
  List<Widget> _buildItems(List<String> items) {
    return items
        .map(
          (e) => Padding(
            padding: EdgeInsets.only(
              right: !widget.isHeader ? 14 : 0,
              left: items.indexOf(e) == 0
                  ? !widget.isHeader
                      ? 14
                      : 0
                  : 0,
            ),
            child: ImageURLAtm(
              radius: !widget.isHeader ? 10 : 0,
              hasBorder: false,
              width: double.maxFinite,
              imageUrl: "${Config.endpoint}/storage/$e",
            ),
          ),
        )
        .toList();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: widget.height,
      child: BlocBuilder<SliderBloc, SliderState>(
        builder: (context, state) {
          if (state is SliderLoaded) {
            if (widget.isTop
                ? state.resultTop == null || state.resultTop == null
                : state.resultBottom == null || state.resultBottom == null) {
              return SizedBox();
            }

            var items = widget.isTop
                ? state.resultTop.data
                    .map((element) => element.pathSlider.toString())
                    .toList()
                : state.resultBottom.data
                    .map((element) => element.pathSlider.toString())
                    .toList();

            return Stack(
              children: [
                CarouselSlider(
                  items: _buildItems(items),
                  options: CarouselOptions(
                    height: widget.height,
                    viewportFraction: 1,
                    initialPage: 0,
                    enableInfiniteScroll: items.length > 1 ? true : false,
                    reverse: false,
                    autoPlay: items.length > 1 ? true : false,
                    autoPlayInterval: Duration(seconds: 3),
                    autoPlayAnimationDuration: Duration(milliseconds: 800),
                    autoPlayCurve: Curves.fastOutSlowIn,
                    onPageChanged: (i, r) {
                      currentIndex = i;
                      if (widget.onPageChanged != null)
                        widget.onPageChanged(i, r);
                      setState(() {});
                    },
                    scrollDirection: Axis.horizontal,
                  ),
                ),
                _buildCarouselsIndicator(items)
              ],
            );
          }
          return Center(child: CircularProgressIndicator());
        },
      ),
    );
  }

  Widget _buildCarouselsIndicator(List<String> items) {
    return Padding(
      padding: EdgeInsets.only(left:!widget.isHeader?14:0),
      child: Align(
        alignment: Alignment.bottomLeft,
        child: Padding(
          padding: const EdgeInsets.all(Dimens.dp10),
          child: Row(
              children: items
                  .map((e) => Container(
                        width: 8,
                        height: 8,
                        margin: EdgeInsets.only(right: Dimens.dp6),
                        decoration: BoxDecoration(
                          boxShadow: [
                            BoxShadow(
                                blurRadius: 5.0,
                                spreadRadius: 0.1,
                                color: Colors.black.withOpacity(0.3)),
                          ],
                          color: items.indexOf(e) == currentIndex
                              ? Colors.white
                              : Colors.white.withOpacity(0.5),
                          shape: BoxShape.circle,
                        ),
                      ))
                  .toList()),
        ),
      ),
    );
  }
}
