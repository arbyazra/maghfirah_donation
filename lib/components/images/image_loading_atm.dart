import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:maghfirah_donation/shared/shared.dart';
import 'package:shimmer/shimmer.dart';

class ImageURLAtm extends StatelessWidget {
  final String imageUrl;
  final File fileImg;
  final String customHeroTag;
  final bool useShimmer;
  final bool hasBorder;
  final bool isHuman;
  final BoxFit fit;
  final double height;
  final double radius;
  final double width;
  final Color bgColor;
  final BorderRadiusGeometry borderRadius;
  const ImageURLAtm({
    Key key,
    this.imageUrl,
    this.useShimmer = false,
    this.height = 100,
    this.radius = 2,
    this.width = 100,
    this.fit = BoxFit.cover,
    this.customHeroTag = "",
    this.hasBorder = true,
    this.borderRadius,
    this.isHuman = false,
    this.bgColor,
    this.fileImg,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      height: height,
      width: width,
      decoration: BoxDecoration(
        color:isHuman?null:  bgColor ?? AppColors.bgScaffold,
        borderRadius: borderRadius ?? BorderRadius.circular(radius),
        border: isHuman?null: !hasBorder
            ? null
            : Border.fromBorderSide(
                BorderSide(color: AppColors.greyDark.withOpacity(0.1))),
      ),
      child: ClipRRect(
        borderRadius: borderRadius ?? BorderRadius.circular(radius),
        child: imageUrl != null
            ? fileImg != null
                ? Image.file(
                    fileImg,
                    fit: fit,
                  )
                : CachedNetworkImage(
                    fit: fit,
                    imageUrl: imageUrl,
                    placeholder: (context, url) => Center(
                      child: useShimmer
                          ? Shimmer.fromColors(
                              baseColor: Colors.grey[200],
                              highlightColor: Colors.grey[400],
                              child: SizedBox(
                                height: height,
                                width: width,
                              ),
                            )
                          : CircularProgressIndicator(),
                    ),
                    errorWidget: (context, url, error) => isHuman
                        ? Icon(
                            Icons.account_circle,
                            size: 40,
                            color: Colors.grey[100],
                          )
                        : Image.asset(AssetPaths.noAvailableImage),
                  )
            : isHuman
                ? Icon(
                    Icons.account_circle,
                    size: 40,
                    color: Colors.grey[300],
                  )
                : Image.asset(AssetPaths.noAvailableImage),
      ),
    );
  }
}
