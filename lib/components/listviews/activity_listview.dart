import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:maghfirah_donation/components/components.dart';
import 'package:maghfirah_donation/modules/modules.dart';
import 'package:maghfirah_donation/shared/shared.dart';

class ActivityListView extends StatelessWidget {

  const ActivityListView({Key key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 260,
      margin: EdgeInsets.only(top: Dimens.dp20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding:
                const EdgeInsets.only(left: Dimens.dp18, bottom: Dimens.dp18),
            child: H1Atm(
              "Kegiatan kampung maghfirah",
              style: TextStylesTheme.sectionTitleText,
            ),
          ),
          Container(
            height: 220,
            child: BlocBuilder<ActivityBloc, ActivityState>(
              builder: (context, state) {
                if(state is ActivityError){
                  return Center(child: H3Atm("an error occured"));
                }
                if (state is ActivityLoaded) {
                  return ListView.separated(
                    // shrinkWrap: true,
                    physics: BouncingScrollPhysics(),
                    padding:
                        EdgeInsets.only(left: Dimens.dp18, right: Dimens.dp18),
                    scrollDirection: Axis.horizontal,
                    itemCount: state.result.data.length,
                    separatorBuilder: (context, index) => SizedBox(
                      width: Dimens.dp18,
                    ),
                    itemBuilder: (context, index) {
                      final activity = state.result.data[index];
                      return ActivityItemCard(
                        activity: activity,
                      );
                    },
                  );
                }
                return Center(child: CircularProgressIndicator());
              },
            ),
          ),
        ],
      ),
    );
  }
}
