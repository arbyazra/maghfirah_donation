import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:maghfirah_donation/modules/modules.dart';
import 'package:maghfirah_donation/shared/shared.dart';

import '../components.dart';

class DonationListView extends StatelessWidget {
  final List<Donation> donations;
  final hasTitle;
  const DonationListView({Key key, this.donations, this.hasTitle = true})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: hasTitle ? Dimens.dp20 : 0, bottom: 100),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          hasTitle
              ? Padding(
                  padding: const EdgeInsets.only(
                    left: Dimens.dp18,
                    bottom: Dimens.dp16,
                    right: Dimens.dp18,
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Expanded(
                        child: H1Atm(
                          "12 Amal kampung maghfirah",
                          style: TextStyle(
                              fontWeight: FontWeight.w700,
                              letterSpacing: 0.4,
                              color: Color(0xFF353535)),
                        ),
                      ),
                      GestureDetector(
                        onTap: () =>
                            Navigator.push(context, DonationPage().route()),
                        child: H3Atm(
                          "Lihat Semua",
                          style: TextStyle(
                              fontWeight: FontWeight.w700,
                              color: AppColors.primaryColor),
                        ),
                      )
                    ],
                  ),
                )
              : SizedBox(),
          BlocBuilder<DonationBloc, DonationState>(
            builder: (context, state) {
              if (state is DonationError) {
                return Center(
                  child: H3Atm(Strings.loadingErrorText),
                );
              }
              if (state is DonationLoaded) {
                if (state.result.data==null || state.result.data.isEmpty) {
                  return Padding(
                    padding: EdgeInsets.only(top: Dimens.height(context) * .25),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        SizedBox(
                          height: 120,
                          child: Image.asset(
                            AssetPaths.noItems,
                          ),
                        ),
                        SizedBox(
                          height: 25,
                        ),
                        H3Atm(
                          "Belum ada kegiatan",
                          style: TextStylesTheme.primaryText,
                        )
                      ],
                    ),
                  );
                }
                return _buildListView(state.result.data);
              }
              return SizedBox(
                height: 100,
                child: Center(
                  child: CircularProgressIndicator(),
                ),
              );
            },
          )
        ],
      ),
    );
  }

  Widget _buildListView(List<Donation> donations) {
    return ListView.separated(
      shrinkWrap: true,
      physics: NeverScrollableScrollPhysics(),
      padding: EdgeInsets.only(
          left: Dimens.dp18, right: Dimens.dp18, top: Dimens.dp8),
      itemCount: donations.length,
      separatorBuilder: (context, index) => SizedBox(
        height: Dimens.dp18,
      ),
      itemBuilder: (context, index) {
        final donation = donations[index];
        return DonationItemCard(
          donation: donation,
        );
      },
    );
  }
}
