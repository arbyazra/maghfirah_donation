import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:maghfirah_donation/modules/modules.dart';
import 'package:maghfirah_donation/shared/shared.dart';

import '../components.dart';

class MyDonationItemCard extends StatelessWidget {
  final MyDonation myDonation;
  const MyDonationItemCard({Key key, this.myDonation}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Ink(
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(Dimens.dp4),
        boxShadow: [
          BoxShadow(
              spreadRadius: 1.5,
              blurRadius: 5,
              color: Colors.black.withOpacity(0.06)),
        ],
      ),
      child: InkWell(
        borderRadius: BorderRadius.circular(Dimens.dp4),
        // onTap: onTap,
        child: Container(
            padding: EdgeInsets.symmetric(
                horizontal: Dimens.dp16, vertical: Dimens.dp16),
            child: Column(
              children: [
                _buildDonationTypeAndTime(),
                SizedBox(height: Dimens.dp6),
                DefaultDividerAtm(
                  color: AppColors.grey2,
                ),
                _buildDonationContent(),
                SizedBox(
                  height: Dimens.dp10,
                ),
                _buildDonationSummary(context),
              ],
            )),
      ),
    );
  }

  Widget _buildDonationTypeAndTime() {
    return Padding(
      padding: const EdgeInsets.only(bottom: Dimens.dp8),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              H3Atm(
                Utils.getDonationType(myDonation.fundType),
                style: TextStyle(fontWeight: FontWeight.w500),
              ),
              SizedBox(
                height: Dimens.dp4,
              ),
              H3Atm(
                  DateFormat("dd MMM yyyy", 'id').format(myDonation.createdAt)),
            ],
          ),
          DefaultBadge(
            height: 25,
            radius: 4,
            padding: EdgeInsets.symmetric(vertical: 4, horizontal: 12),
            color: AppColors.primary3Color,
            child: Center(
              child: H4Atm(
                Utils.getStatusOrder(myDonation.status),
                style: TextStyle(
                  fontWeight: FontWeight.w500,
                  color: Utils.getStatusColorOrder(myDonation.status),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget _buildDonationContent() {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: Dimens.dp12),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          ImageURLAtm(
            width: 74,
            height: 74,
            radius: 8,
            hasBorder: false,
            imageUrl: myDonation.project != null
                ? "${Config.endpoint}/storage/${myDonation.project.pathFeatured}"
                : null,
          ),
          SizedBox(
            width: Dimens.dp10,
          ),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                H2Atm(
                  myDonation.project != null
                      ? myDonation.project.title
                      : "project tidak ditemukan",
                  maxLine: 2,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                    fontWeight: FontWeight.w700,
                    height: 1.1,
                    letterSpacing: 0.8,
                    color: Color(0xFF353535),
                  ),
                ),
                SizedBox(
                  height: Dimens.dp10,
                ),
                // H2Atm(
                //   "${myDonation.} Sedekah",
                //   style: TextStyle(
                //     fontWeight: FontWeight.w300,
                //     color: AppColors.greyDark,
                //   ),
                // )
              ],
            ),
          )
        ],
      ),
    );
  }

  Widget _buildDonationSummary(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            H3Atm(
              "Total ${Utils.getDonationType(myDonation.fundType)}",
              style: TextStyle(
                fontWeight: FontWeight.w300,
              ),
            ),
            SizedBox(
              height: Dimens.dp6,
            ),
            H1Atm(
              NumberFormat.simpleCurrency(locale: 'id')
                  .format(int.parse(myDonation.nominal)),
              // activity.title,
              maxLine: 1,
              overflow: TextOverflow.ellipsis,
              style: TextStyle(
                fontWeight: FontWeight.w500,
              ),
            ),
          ],
        ),
        myDonation.status.toLowerCase() == "waiting"
            ? SizedBox()
            : myDonation.status.toLowerCase() == "paid"
                ? DefaultButtonMol(
                    height: 30,
                    onClick: () => Utils.addNewStack(context, NavigateToHome()),
                    whiteMode: false,
                    color: AppColors.primaryColor,
                    child: H4Atm(
                      "Sedekah Lagi",
                      style: TextStyle(
                          color: Colors.white, fontWeight: FontWeight.w500),
                    ),
                  )
                : DefaultButtonMol(
                    height: 30,
                    onClick: () {
                      Navigator.push(
                          context,
                          PaymentTutorialPage(
                            paymentId: myDonation.id.toString(),
                            isBank:
                                myDonation.paymentType.toLowerCase() == 'bank',
                          ).route());
                    },
                    whiteMode: false,
                    color: Colors.orange[800],
                    child: H4Atm(
                      "Bayar",
                      style: TextStyle(
                          color: Colors.white, fontWeight: FontWeight.w500),
                    ),
                  )
      ],
    );
  }
}
