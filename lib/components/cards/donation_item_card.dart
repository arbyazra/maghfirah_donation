import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:maghfirah_donation/modules/modules.dart';
import 'package:maghfirah_donation/shared/shared.dart';
import 'package:percent_indicator/percent_indicator.dart';

import '../components.dart';

class DonationItemCard extends StatelessWidget {
  final Donation donation;

  const DonationItemCard({Key key, this.donation}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Ink(
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(Dimens.dp10),
          boxShadow: [
            BoxShadow(
                blurRadius: 3.0,
                offset: Offset(0, 3),
                spreadRadius: 2.0,
                color: Colors.black.withOpacity(0.07)),
          ]),
      child: InkWell(
        borderRadius: BorderRadius.circular(Dimens.dp10),
        onTap: () => Navigator.push(
            context,
            DonationDetailPage(
              donation: donation,
            ).route()),
        child: Container(
          height: 220,
          width: double.maxFinite,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              ImageURLAtm(
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(Dimens.dp10),
                    topRight: Radius.circular(Dimens.dp10)),
                width: double.maxFinite,
                height: 110,
                imageUrl: donation.pathFeatured,
              ),
              Padding(
                padding: const EdgeInsets.symmetric(
                    horizontal: Dimens.dp14, vertical: Dimens.dp12),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    H2Atm(
                      donation.title,
                      overflow: TextOverflow.ellipsis,
                      maxLine: 1,
                      style: TextStyle(
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                    SizedBox(
                      height: Dimens.dp10,
                    ),
                    _buildTargetDonation(),
                    _buildDonorProgress(context),
                    _buildDonationDurationAndDonors(),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildTargetDonation() {
    return Row(
      children: [
        H3Atm(
          NumberFormat.simpleCurrency(locale: 'id')
              .format(int.parse(donation.donations)),
          // activity.title,
          style: TextStyle(
            fontWeight: FontWeight.w500,
          ),
        ),
        Expanded(
          child: H3Atm(
            " Terkumpul",
            // activity.title,
            style: TextStyle(
              fontWeight: FontWeight.w300,
            ),
          ),
        ),
      ],
    );
  }

  Widget _buildDonorProgress(BuildContext context) {
    return
        // donation.isUnlimited
        //     ? SizedBox(
        //         height: Dimens.dp8,
        //       )
        //     :
        Padding(
      padding: const EdgeInsets.only(
        top: 8,
        bottom: 12,
      ),
      child: LinearPercentIndicator(
        width: Dimens.width(context) - 64,
        padding: EdgeInsets.symmetric(horizontal: Dimens.dp4),
        lineHeight: 8.0,
        clipLinearGradient: true,
        percent:double.parse(donation.percentage) / 100,
        backgroundColor: AppColors.bgGrey,
        progressColor: (double.parse(donation.percentage) / 100) > 0.5
            ? AppColors.primaryColor
            : Colors.orange[400],
      ),
    );
  }

  Widget _buildDonationDurationAndDonors() {
    return Row(
      children: [
        donation.isUnlimited
            ? H2Atm(
                "Tidak Terbatas",
                style: TextStyle(
                    fontWeight: FontWeight.w300, color: AppColors.greyDark),
              )
            : RichText(
                text: TextSpan(
                  text: "${donation.dateCount} ",
                  style: TextStyle(
                      fontWeight: FontWeight.w700, color: Colors.black),
                  children: <TextSpan>[
                    TextSpan(
                      text: "Hari lagi",
                      style: TextStyle(
                          fontWeight: FontWeight.w300,
                          color: AppColors.greyDark),
                    )
                  ],
                ),
              ),
        Spacer(),
        RichText(
          text: TextSpan(
            text: "${donation.donaturs} ",
            style: TextStyle(fontWeight: FontWeight.w700, color: Colors.black),
            children: <TextSpan>[
              TextSpan(
                text: "Donatur",
                style: TextStyle(
                    fontWeight: FontWeight.w300, color: AppColors.greyDark),
              )
            ],
          ),
        ),
      ],
    );
  }
}
