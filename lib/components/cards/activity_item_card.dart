import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:maghfirah_donation/components/badges/default_badge.dart';
import 'package:maghfirah_donation/components/images/images.dart';
import 'package:maghfirah_donation/modules/home/pages/activity_detail_page.dart';
import 'package:maghfirah_donation/modules/modules.dart';
import 'package:maghfirah_donation/shared/shared.dart';

import '../components.dart';

class ActivityItemCard extends StatelessWidget {
  final Activity activity;

  const ActivityItemCard({Key key, @required this.activity}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Ink(
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(Dimens.dp10),
          boxShadow: [
            BoxShadow(
                blurRadius: 3.0,
                offset: Offset(0, 3),
                spreadRadius: 2.0,
                color: Colors.black.withOpacity(0.07)),
          ]),
      child: InkWell(
        borderRadius: BorderRadius.circular(Dimens.dp10),
        onTap: () => Navigator.push(
            context,
            ActivityDetailPage(
              activity: activity,
            ).route()),
        child: Container(
          width: Dimens.width(context) * .715,
          margin: EdgeInsets.only(bottom: Dimens.dp10),
          child: Column(
            children: [
              ImageURLAtm(
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(Dimens.dp10),
                    topRight: Radius.circular(Dimens.dp10)),
                width: double.maxFinite,
                height: 130,
                imageUrl: activity.path,
              ),
              Expanded(
                child: Container(
                  height: 50,
                  padding: EdgeInsets.symmetric(
                      horizontal: Dimens.dp14, vertical: Dimens.dp10),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      _buildBadge(),
                      _buildActivityInfo(),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildActivityInfo() {
    return Expanded(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Padding(
            padding: const EdgeInsets.only(left: 10.0),
            child: H2Atm(
              activity.title,
              // activity.title,
              overflow: TextOverflow.ellipsis,
              maxLine: 2,
              style: TextStyle(
                fontWeight: FontWeight.w500,
              ),
            ),
          ),
          SizedBox(height: Dimens.dp4),
          Padding(
            padding: const EdgeInsets.only(left: 6.0),
            child: Row(
              children: [
                Icon(
                  Icons.place,
                  color: AppColors.greyDark,
                  size: 16,
                ),
                Expanded(
                  child: H3Atm(
                    activity.place,
                    maxLine: 1,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                      fontWeight: FontWeight.w300,
                    ),
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }

  Widget _buildBadge() {
    return DefaultBadge(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          H3Atm(
            DateFormat("dd").format(activity.activityDate),
            // DateFormat("dd").format(DateTime.parse("2021-02-12")),
            style: TextStyle(
                fontWeight: FontWeight.w700,
                letterSpacing: 0.1,
                color: AppColors.primary2Color),
          ),
          SizedBox(
            height: 2,
          ),
          H3Atm(
            DateFormat("MMM","id").format(activity.activityDate),
            // DateFormat("MMM", "id").format(DateTime.parse("2021-02-12")),
            style: TextStyle(
                fontWeight: FontWeight.w400, color: AppColors.primary2Color),
          ),
        ],
      ),
    );
  }
}
