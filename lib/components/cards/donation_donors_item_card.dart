import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:jiffy/jiffy.dart';
import 'package:maghfirah_donation/modules/modules.dart';
import 'package:maghfirah_donation/shared/shared.dart';

import '../components.dart';

class DonationDonorsItemCard extends StatelessWidget {
  final Donatur donatur;

  const DonationDonorsItemCard({Key key, this.donatur}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    print(donatur.pathFoto);
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        ImageURLAtm(
          radius: 1000,
          height: 45,
          width: 45,
          imageUrl: donatur.pathFoto,
          isHuman: true,
        ),
        SizedBox(width: Dimens.dp12),
        Expanded(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              H2Atm(
                donatur.donatureName,
                style:
                    TextStyle(fontWeight: FontWeight.w500, letterSpacing: 0.2),
              ),
              SizedBox(
                height: Dimens.dp6,
              ),
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  H3Atm(Utils.getDonationType(donatur.fundType)),
                  SizedBox(width: Dimens.dp6),
                  H3Atm(
                    NumberFormat.simpleCurrency(locale: 'id').format(int.parse(donatur.nominal)),
                    style: TextStyle(
                        fontWeight: FontWeight.w500, letterSpacing: 0.2),
                  ),
                ],
              ),
              SizedBox(
                height: Dimens.dp6,
              ),
              H3Atm(
                Jiffy(donatur.createdAt).fromNow(),
                style: TextStyle(
                    fontWeight: FontWeight.w300,
                    letterSpacing: 0.2,
                    color: Colors.grey[500]),
              ),
              SizedBox(
                height: Dimens.dp10,
              ),
              H3Atm(
                donatur.specialMessage??"",
                maxLine: 2,
                overflow: TextOverflow.ellipsis,
                style: TextStyle(
                  fontWeight: FontWeight.w300,
                  letterSpacing: 0.2,
                ),
              ),
            ],
          ),
        )
      ],
    );
  }
}
