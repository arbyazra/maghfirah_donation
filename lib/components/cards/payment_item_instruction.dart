import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:maghfirah_donation/modules/modules.dart';
import 'package:maghfirah_donation/shared/shared.dart';

import '../components.dart';

class PaymentItemInstruction extends StatefulWidget {
  final Instruction instruction;
  final bool isFirstItem;
  const PaymentItemInstruction(
      {Key key, this.instruction, this.isFirstItem = false})
      : super(key: key);
  @override
  _PaymentItemInstructionState createState() => _PaymentItemInstructionState();
}

class _PaymentItemInstructionState extends State<PaymentItemInstruction> {
  var isExpanded;
  @override
  void initState() {
    super.initState();
    isExpanded = widget.isFirstItem;
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        ListTile(
          contentPadding: EdgeInsets.zero,
          onTap: () {
            setState(() {
              isExpanded = !isExpanded;
            });
          },
          leading: H2Atm(
            widget.instruction.title,
            style: TextStylesTheme.contentText,
          ),
          trailing: Icon(
              isExpanded ? Icons.keyboard_arrow_up : Icons.keyboard_arrow_down),
        ),
        AnimatedCrossFade(
          duration: Duration(milliseconds: 200),
          sizeCurve: Curves.easeInOutCirc,
          crossFadeState:
              isExpanded ? CrossFadeState.showSecond : CrossFadeState.showFirst,
          secondChild: _buildSteps(),
          firstChild: SizedBox(),
        ),
      ],
    );
  }

  Widget _buildSteps() {
    return ListView.builder(
      shrinkWrap: true,
      padding: EdgeInsets.only(bottom: 14),
      physics: NeverScrollableScrollPhysics(),
      itemCount: widget.instruction.steps.length,
      itemBuilder: (context, index) {
        var data = widget.instruction.steps[index];
        return Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.only(top: 10),
              child: H4Atm(
                (index + 1).toString() + ".",
              ),
            ),
            Expanded(
              child: Html(
                data: data,
              ),
            ),
          ],
        );
      },
    );
  }
}
