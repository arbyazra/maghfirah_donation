import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:maghfirah_donation/modules/modules.dart';
import 'package:maghfirah_donation/shared/shared.dart';

import '../components.dart';

class DonationNewsItemCard extends StatelessWidget {
  final News news;

  const DonationNewsItemCard({Key key, this.news}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return IntrinsicHeight(
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Column(
            children: [
              Container(
                width: 8,
                height: 8,
                decoration: BoxDecoration(
                  color: AppColors.primaryColor,
                  shape: BoxShape.circle,
                ),
              ),
              Expanded(
                child: Container(
                  color: Colors.grey,
                  width: 0.5,
                ),
              ),
            ],
          ),
          SizedBox(width: Dimens.dp10),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                H3Atm(DateFormat("dd MMMM yyyy",'id').format(news.createdAt)),
                SizedBox(
                  height: Dimens.dp2,
                ),
                H2Atm(
                  news.title,
                  style: TextStyle(
                    fontWeight: FontWeight.w500,
                    height: 1.1,
                    letterSpacing: 0.2,
                  ),
                ),
                SizedBox(
                  height: Dimens.dp10,
                ),
                H3Atm(
                    news.content,
                    style: TextStylesTheme.contentText),
                SizedBox(
                  height: Dimens.dp12,
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
