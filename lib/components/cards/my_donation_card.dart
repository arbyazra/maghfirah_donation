import 'package:flutter/material.dart';
import 'package:maghfirah_donation/shared/shared.dart';

import '../components.dart';

class MyDonationCard extends StatelessWidget {
  final IconData icon;
  final String title;
  final String value;
  final VoidCallback onTap;

  const MyDonationCard({Key key, this.icon, this.title, this.value, this.onTap})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(
        horizontal: Dimens.dp16,
      ),
      child: Ink(
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(Dimens.dp4),
            boxShadow: [
              BoxShadow(
                  spreadRadius: 1.5,
                  blurRadius: 5,
                  color: Colors.black.withOpacity(0.06)),
            ]),
        child: InkWell(
          borderRadius: BorderRadius.circular(Dimens.dp4),
          onTap: onTap,
          child: Container(
            padding: EdgeInsets.symmetric(
                horizontal: Dimens.dp16, vertical: Dimens.dp16),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                IconBadge(
                  icon: Icon(
                    icon,
                    size: 34,
                    color: AppColors.primaryColor,
                  ),
                ),
                SizedBox(
                  width: Dimens.dp18,
                ),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      H3Atm(
                        title,
                        style: TextStyle(fontWeight: FontWeight.w300),
                      ),
                      SizedBox(
                        height: Dimens.dp6,
                      ),
                      H1Atm(
                        value,
                        style: TextStyle(fontWeight: FontWeight.w700),
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
