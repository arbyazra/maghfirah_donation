import 'package:flutter/material.dart';
import 'package:maghfirah_donation/modules/modules.dart';
import 'package:maghfirah_donation/shared/shared.dart';

import '../components.dart';

class ProfileNameCard extends StatelessWidget {
  final ProfileResponse response;

  const ProfileNameCard({Key key, this.response}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(Dimens.dp16),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Container(
            width: 55,
            height: 55,
            child: ImageURLAtm(
              radius: 1000,
              imageUrl: "${Config.endpoint}/storage/${response.data.pathFoto}",
            ),
          ),
          SizedBox(width: Dimens.dp14),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                H2Atm(
                  response.data.name,
                  style: TextStyle(
                    fontWeight: FontWeight.w700,
                    letterSpacing: 0.6,
                  ),
                ),
                SizedBox(
                  height: Dimens.dp6,
                ),
                response.data.level.toLowerCase() != "user"
                    ? SizedBox()
                    : H3Atm(
                        "Donatur",
                        style: TextStyle(
                            fontWeight: FontWeight.w300,
                            color: AppColors.greyDark),
                      )
              ],
            ),
          )
        ],
      ),
    );
  }
}
